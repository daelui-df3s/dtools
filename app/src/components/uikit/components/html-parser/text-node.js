/**
 * @author Rid King
 * @since 2017-07-15
 * @version 2.0.0
 * @description 文本节点类
 */

import Node from './node.js'

class TextNode extends Node{
    /**
     * @description 构造函数
     * @param text <String> // 文本
    */
    constructor (text) {
        super()
        this.text = text
    }
}

export default TextNode