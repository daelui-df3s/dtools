/*
 * @desc 对话框
 * */
define('dialoger', ['jquery', 'toastr'], function($, toastr) {
	var reg = new RegExp("\\[([^\\[\\]]*?)\\]", 'igm'),
		alrStr = '<div class="modal fade modal-dialoger">' +
			'<div class="modal-dialog modal-sm">' +
			'<div class="modal-content">' +
			'<div class="modal-header">' +
			'<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
			'<h5 class="modal-title"><i class="fa fa-exclamation-circle"></i> [Title]</h5>' +
			'</div>' +
			'<div class="modal-body small">' +
			'<p>[Message]</p>' +
			'</div>' +
			'<div class="modal-footer" >' +
			'<button type="button" class="btn btn-sm btn-base ok" data-dismiss="modal">[BtnOk]</button>' +
			'<button type="button" class="btn btn-sm btn-default cancel" data-dismiss="modal">[BtnCancel]</button>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

	//关闭时恢复 modal html 原样，供下次调用时 replace 用
	//var _init = function () {
	//	alr.on("hidden.bs.modal", function (e) {
	//		$(this).html(ahtml);
	//	});
	//}();

	/* html 复原不在 _init() 里面做了，重复调用时会有问题，直接在 _alert/_confirm 里面做 */


	var _alert = function (options) {
			var alr = $(alrStr);	// 复原
			alr.find('.cancel').hide();
			dialog(alr, options);
			if (options.callback && options.callback instanceof Function) {
				alr.find('.ok').click(function () { options.callback(true) });
			}
			return alr;
		},

		_confirm = function (options) {
			var alr = $(alrStr); // 复原
			alr.find('.cancel').show();
			dialog(alr, options);
			if (options.callback && options.callback instanceof Function) {
				alr.find('.ok').click(function () { options.callback(true) });
				alr.find('.cancel').click(function () { options.callback(false) });
			}
			return alr;
		},

		_dialog = function (options) {
			var opts = {
				backdrop : true,
				width : $(window).width() * 0.8,
				height : $(window).height() * 0.8,
				btnok: "确定",
				btncl: "取消"
			};
			options = $.extend(opts, options);
			options.height = options.height - 120 < 50 ? 50 : options.height - 120;
			var alr = $(alrStr); // 复原
			if(opts.btnok){
				alr.find('.ok').show();
			}else alr.find('.ok').hide();
			if(opts.btncl){
				alr.find('.cancel').show();
			}else alr.find('.cancel').hide();
			dialog(alr, options);
			if (options.callback && options.callback instanceof Function) {
				alr.find('.ok').click(function () { options.callback(true) });
				alr.find('.cancel').click(function () { options.callback(false) });
			}
			return alr;
		};

	var dialog = function (alr, options) {
			var ops = {
				msg: "提示内容",
				title: "操作提示",
				btnok: "确定",
				btncl: "取消",
				backdrop: true,
				backhide : false
			};

			$.extend(ops, options);

			var html = alr.html().replace(reg, function (node, key) {
				return {
					Title: ops.title,
					Message: ops.msg,
					BtnOk: ops.btnok,
					BtnCancel: ops.btncl
				}[key];
			});

			alr.html(html);

			//额外按钮
			if($.isArray(options.btns)){
				$.each(options.btns, function(i, item){
					var $btn = $('<button type="button" class="btn btn-sm btn-bas ' + (item.cls || '') + '" data-close-dia="' + (item.close ? 'true' : '') + '">' + item.text + '</button>');
					$btn.click(function(e){
						if(typeof item.handler) item.handler.call(this, e, alr);
						if(!!$(this).attr('data-close-dia')) $(this).parents('.dialoger').modal('hide');
					});
					alr.find('.modal-footer').append($btn);
				});
			}
			//按钮事件
			if(typeof ops.okHandler == 'function'){
				alr.find('.ok').removeAttr('data-dismiss').click(function(){
					ops.okHandler.call(this, alr);
				});
			}
			if(typeof ops.clHandler == 'function'){
				alr.find('.cancel').removeAttr('data-dismiss').click(function(){
					ops.clHandler.call(this, alr);
				});
			}
			console.log(options)
			//无按钮
			if(!ops.btnok && !ops.btncl && (!$.isArray(ops.btns) || (ops.btns || []).length < 1)){
				alr.find('.modal-footer').hide();
			}

			//长度与宽度限制
			if(ops.width){
				alr.find('.modal-dialog').removeClass('modal-sm').css('width', ops.width);
			}
			if(ops.height){
				alr.find('.modal-body').css('height', ops.height);
			}

			if(ops.dialog){
				alr.find('.modal-body').empty().append('<div class="modal-load" style="width:100%;height:100%;overflow:auto"></div>');
				alr.find('.modal-load').append($(ops.dialog).addClass('popup-dialog').show());
				if(typeof ops.completeFunc == 'function'){
					ops.completeFunc({dialog : alr})
				}
				//关闭时删除
				alr.on('hidden.bs.modal', function (e) {
					alr.find('.popup-dialog').hide().appendTo('body');
					setTimeout(function(){
						alr.remove();
					}, 200);
				});
			}

			//弹出元素
			else if(ops.popup){
				var $modal = $('<div class="modal fade"></div>'),
					$popup= $(ops.popup).addClass('modal-dialog');
				var $win = $(ops.window || window),
					width = $win.width(),
					height = $win.height(),
					defaults = {
						width : width * 0.95,
						height : height * 0.95
					};
				ops = $.extend(defaults, ops);
				var $popup = $(ops.popup),
					popWidth = $popup.width(),
					popHeight = $popup.height(),
					style;

				if(popWidth < 20 || popWidth > defaults.width){
					popWidth = defaults.width;
				}
				if(popHeight < 20 || popHeight > defaults.height){
					popWidth = defaults.height;
				}
				style = {
					width : popWidth,
					height : popHeight,
					left : (width - popWidth) / 2,
					top : (height - popHeight) / 2,
					position : 'fixed',
					'z-index' : 1050
				};
				$popup.css(style);
				$modal.append($popup.show()).modal(ops);
				if(ops.backhide){
					$popup.on('click', function(){
						return false;
					});
					$modal.on('click', function(){
						$modal.modal('hide');
						setTimeout(function(){
							$popup.hide().appendTo('body');
							$modal.remove();
						}, 200);
					});
				}
				$popup.one('[data-dismiss]').click(function(){
					$modal.modal('hide');
					setTimeout(function(){
						$popup.hide().appendTo('body');
						$modal.remove();
					}, 200);
				});
				return;
			}

			//加载iframe
			else if(ops.frame){
				alr.find('.modal-body').empty().append('<iframe src="' + ops.url + '" frameborder="no" scrolling="auto" style="width:100%;height:100%"></iframe>');
				frameLoad(alr.find('iframe').get(0), function(){
					if(typeof ops.completeFunc == 'function'){
						ops.completeFunc({dialog : alr})
					}
				});
			}

			//远程加载load
			else if(ops.load){
				alr.find('.modal-body').empty().append('<div class="modal-load" style="width:100%;height:100%;overflow:auto"></div>');
				removeBind();
			}

			//显示
			alr.modal({
				backdrop: ops.backdrop ? 'static' : false,
				remote : options.remote
			});

			if(ops.load){
				alr.find('.modal-load').load(ops.url, function () {
					if (typeof ops.completeFunc == 'function') {
						ops.completeFunc({dialog: alr});
					}
				});
				removeBind();
			}

			if(ops.backhide){
				alr.on('click', function(){
					alr.modal('hide');
				});
				alr.on('click', '.modal-dialog', function(){
					return false;
				});
			}

			//移除绑定
			function removeBind(){
				//关闭时删除
				alr.on('hidden.bs.modal', function (e) {
					setTimeout(function(){
						alr.remove();
					}, 200);
				});
			}

		},

		//框架监测
		frameLoad = function(frame, fn){
			if(!frame){
				fn();
				return;
			}
			if(frame.readyState == "complete" || frame.readyState == 1){
				fn();
			}else{
				if (frame.attachEvent){
					frame.attachEvent("onload", function(){
						fn();
					});
				} else {
					frame.onload = function(){
						fn();
					};
				}
			}
		};

	return {
		//警告框
		alert : _alert,
		//选择框
		confirm : _confirm,
		//对话框
		dialog : _dialog,
		//隐藏
		hide : function($el){
			if(typeof $el == 'object' && $el){
				if($el.modal) $el.modal('hide');
			}
		},
		//普通信息展示
		info : function(options){
			toastr.info(options.msg || '成功', options.title || '提示', options);
		},
		//成功信息展示
		success : function(options){
			toastr.success(options.msg || '成功', options.title || '提示', options);
		},
		//警告信息展示
		warning : function(options){
			toastr.warning(options.msg || '警告', options.title || '提示', options);
		},
		//错误信息展示
		error : function(options){
			toastr.error(options.msg || '错误', options.title || '提示', options);
		}
	}
});