/**
 * @description 图片工具
 */

define(['jquery', 'qrcoder'], function(){
    var imager = {
        //从 canvas 提取图片 image
        convertCanvasToImage : function (canvas) {
            //新Image对象，可以理解为DOM
            var image = new Image();
            // canvas.toDataURL 返回的是一串Base64编码的URL，当然,浏览器自己肯定支持
            // 指定格式 PNG
            image.src = canvas.toDataURL("image/png");
            return image;
        }
    };

    return imager;
});