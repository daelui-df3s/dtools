/**
 * @description 应用加载器
 * */

import $pig from '@daelui/pigjs'

const isProd = /production|single/i.test(process.env.NODE_ENV)
// 是否运行态
const ap = (window.application || {}).dtools || {}
ap.mode = ap[isProd ? 'prod' : 'dev']
// 初始化模块配置
const loader = ap.mode.loader || {}
const domain = ap.mode.domain || location.origin
const rootUrl = loader.rootUrl || location.origin
export default $pig.init({
  path: loader.path || rootUrl + '@daelui/pigjs/latest/dist/system.min.js',
  registry: loader.registry || location.origin + '/components.json',
  rootUrl: rootUrl,
  rootUrls: loader.rootUrls,
  versions: [{name: 'vue', version: '2.*.*'}]
}).then(function(){
  $pig.import(domain + '/df3s/dtools/app/static/css/chunk-vendors.css')
  $pig.import(domain + '/df3s/dtools/app/static/css/main.css')
  $pig.import(domain + '/df3s/dtools/app/static/js/chunk-vendors.js')
  return $pig.import(domain + '/df3s/dtools/app/static/js/main.js')
})
