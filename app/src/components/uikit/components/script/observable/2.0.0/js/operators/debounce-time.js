/**
 * @author Rid King
 * @since 2017-06-02
 * @version 2.0.0
 * @description 延时操作符
 */
'user strict';

import Operator from '../operator.js'

/**
 * @description 只有在特定的一段时间经过后并且没有发出另一个源值，才从源 Observable 中发出一个值。
 * @param executor <Executor> // 执行者
 * [@param time <int> 时间毫秒数，默认500毫秒]
*/
class DebounceTimeOperator extends Operator{
    /**
     * @description 获取操作名称
     * @return <String>
    */
    static getName () {
        return 'debounceTime'
    }

    /**
     * @description 管道方法
    */
    process () {
        super.process.apply(this, arguments)

        // 时间过虑
        let time = parseInt(this._args[0])
        time = isNaN(time) ? 500 : time
        time = time < 0 ? 0 : time

        // 获取操作者
        let executor = this._executor

        // 是否取消，决定是否执行下一操作
        let isCancel = false
        // 消费参数
        let args = arguments

        // 重新设置下一步操作
        let next = this.destination.next
        let error = this.destination.error
        this.destination.next = (function () {
            // 清除定时器
            if (typeof this.timer !== 'undefined') {
                clearTimeout(this.timer)
            }

            // 创建定时器
            this.timer = setTimeout(() => {
                // 已取消不执行
                if (isCancel) {
                    return false
                }
                // 删除计时器
                delete this.timer

                // 执行下一步
                try {
                    typeof next === 'function' && next.apply(this.destination, arguments)
                }
                catch (e) {
                    typeof error === 'function' && error.apply(this.destination, arguments)
                }
            }, time)
        }).bind(this)
    }
}

export default DebounceTimeOperator