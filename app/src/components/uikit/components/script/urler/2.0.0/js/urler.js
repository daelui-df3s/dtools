/**
 * 统一资源定位
 * */

export default {
	stringify (obj, sep, eq) {
    sep = sep || '&';
    eq = eq || '=';
    let str = "";
    for (var k in obj) {
        str += k + eq + unescape(obj[k]) + sep
    }
    return str.slice(0, -1)
  },

  parse (str) {
    var obj = new Object(); 
    strs = str.split("&");
    for (var i = 0; i < strs.length; i++) {
        let index = strs[i].indexOf("=")
        obj[strs[i].slice(0, index)] = unescape(strs[i].slice(index + 1));
    }
    return obj
  }
}