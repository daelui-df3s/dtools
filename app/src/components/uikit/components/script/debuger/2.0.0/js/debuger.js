/**
 * @author Rid King
 * @since 2017-05-04
 * @version 2.0.0
 * @description 调试器
 */

class Debuger {
    static warn (value) {
        console.warn(value)
    }

    warn (value) {
        console.log(value)
    }
}

// 定义默认调试器
const debuger = new Debuger

export default Debuger
export {
    debuger
}