define(function(){
    var histery = {
        //当前URL和history.state加入到history中,并用新的state和URL替换当前,不会造成页面刷新
        pushState : function(state, title, url){
            if(state == undefined) state = {};
            if(!title) title = _title;
            if(!url || typeof url != 'string') return;

            changeTitle(title);

            hashId++;
            hashList[hashId] = {
                hashId : hashId,
                target : win,
                state : state,
                title : title,
                url : url
            }
            if(bState){
                history.pushState(state, title, url);
                return hashId;
            } else {
                //处理不兼容情况
                setTimeout(function(){
                    loc.href = url;
                }, 10);
            }
        },

        //用新的state和URL替换当前,不会造成页面刷新
        replaceState : function(state, title, url){
            if(state == undefined) state = {};
            if(!title) title = _title;
            if(!url || typeof url != 'string') return;

            changeTitle(title);

            if(bState){
                history.replaceState(state, title, url);
            } else {
                //处理不兼容情况
                setTimeout(function(){
                    loc.replace(url);
                }, 10);
            }
        },

        //返回
        back : function(){
            win.history.back();
        },

        //hash改变事件
        onhashchange : function(func){
            func = typeof func == 'function' ? func : function(){};
            /*if(this.bState()){
                win.addEventListener('popstate', function(){
                    func();
                }, false);
            }else */if(this.bState()){
	            win.addEventListener('hashchange', function(){
		            func();
	            }, false);
            }else{
                //模拟变化发生的触发逻辑
                setInterval(function(){
                    func();
                }, 50);
            }
        },

        //是否支持pushState
        bState : function(){
            return bState;
        }
    };

    var bState = window.history.pushState ? true : false,
	    bHash = window.history.pushState ? true : false,
        hashList = [],//hash数组
        hashId = -1,
        win = window,
        doc = document,
        loc = window.location,
        _title = doc.title;

    //触发
    function trigger(hash, callback){
        var length = hashList.length,
            i = length - 1;

        //现代浏览器则直接触发
        if(bState){
            callback(hashList[hashId]);
            return;
        }
        //非现代浏览器，需要先通过hash比对，通过则触发
        for(i; i >= 0; i--) {
            if(hashList[i].path == hash) {
                callback(hashList[i]);
            }
        }
    }

    //改变标题
    function changeTitle(title){
        doc.title = title;
    }

    return histery;
});