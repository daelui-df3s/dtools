/**
 * 浏览器
 */

var browser = {
  versions: function () {
    var u = navigator.userAgent.toLowerCase(), app = navigator.appVersion,
      isPad = false, isAndroidPad = false, isIpad = false,
      isMobile = false, isPc = false;

    if (u.indexOf('')){
      if (u.indexOf('android') > -1) {
        if (u.indexOf('mobile') == -1) {
          isAndroidPad = true;
        }
      }
    }

    if (u.indexOf('ipad') > -1) {
      isIpad = true;
    }

    if (isAndroidPad || isIpad) {
      isPad = true;
    } else if ((u.indexOf('mobile') > -1 && !isPad ) || (u.indexOf('android') > -1 && !isAndroidPad) || (u.indexOf('phone') > -1)) {
      isMobile = true;
    } else {
      isPc = true;
    }

    return {   //移动终端浏览器版本信息
      trident: u.indexOf('Trident') > -1, //IE内核
      presto: u.indexOf('Presto') > -1, //opera内核
      webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
      gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1, //火狐内核
      pc: isPc,
      mobile: !!u.match(/(iPhone|iPad|iPod|ios|Android|Linux|AppleWebKit.*Mobile.*)/i), //是否为移动终端
      //mobile: !!u.match(/(iPhone|iPad|iPod|ios|Android|Linux|AppleWebKit.*Mobile.*)/i) || (!!u.match(/AppleWebKit/) && u.indexOf('QIHU') && u.indexOf('QIHU') > -1 && u.indexOf('Chrome') < 0), //是否为移动终端
      android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
      ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
      iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
      iPad: u.indexOf('iPad') > -1, //是否iPad
      iPod: u.indexOf('iPod') > -1, //是否iPod
      pad: isPad, // 是否pad
      webApp: u.indexOf('Safari') === -1 //是否web应该程序，没有头部与底部
    };
  }(),

  language: (navigator.browserLanguage || navigator.language).toLowerCase(),

  //是否是微信
  isWechat : function(){
    return browser.versions.mobile && ua.match(/MicroMessenger/i) === 'micromessenger';
  },

  //是否是安卓浏览器
  isAndroidBrowser : function(){
    return browser.versions.android && !browser.versions.webApp;
  },

  //是否是IOS浏览器
  isIOSBrowser : function(){
    return browser.versions.ios && !browser.versions.webApp;
  },

  //是否是安卓APP
  isAndroidApp : function(){
    return browser.versions.android && browser.versions.webApp;
  },

  //是否是IOS的APP
  isIOSApp : function(){
    return browser.versions.ios && browser.versions.webApp;
  },

  // 是否支持ES5
  isSupportES5: function () {
    return !!Object.freeze
  },

  // 是否支持ES6
  isSupportES6: function () {
    return !!Object.assign
  }
}, ua = navigator.userAgent.toLowerCase();

export default browser