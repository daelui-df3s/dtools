/**
 * 进程的有关信息
 * */
define(function () {
    return {
        env: {
            NODE_ENV: 'production' // 环境模式，development：开发模式；production：产品模式
        }
    }
});