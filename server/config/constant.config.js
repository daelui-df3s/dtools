/*
 * @description 环境配置
 * @author: Rid King 
 * @since 2019-11-21 22:59:22 
 */

module.exports = {
  // 主机名
  hostname: '0.0.0.0',
  netPort: 3501,
  port: 3502,
  httpsPort: 3503,
  isProxyAble: true,
  proxyPath: '/dtools/proxy',
  isDynamicOrigin: true,
  appPath: '/app',
  appDir: __dirname + '/../../app',
  tesseractWorkerPath: __dirname + '/../static/tesseract.js/worker/worker.js',
  ocrImagePath: __dirname + '/../static/ocrimage',
  tessdata: __dirname + '/../static/tessdata'
}