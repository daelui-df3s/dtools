/*
 * 视图
 * */
define(['classer', 'objecter'], function(classer, objecter) {
	var View = classer.extend({
		// 属性
		_props: {},

		// 事件
		_events: {
			// 视图对象创建前
			beforeCreate: null,
			// 视图对象创建后
			created: null,
			// 视图对象挂载前
			beforeMount: null,
			// 视图对象挂载后
			mounted: null,
			// 视图对象更新前
			beforeUpdate: null,
			// 视图对象更新后
			updated: null,
			// 视图对象销毁前
			beforeDestroy: null,
			// 视图对象销毁后
			destroyed: null
		},

		// 默认配置
		defaults : {},

		/**
		 * @desc 初始化
		 * */
		init: function (obj) {
			// 数据融合
			this._attr = objecter.extend(true, {}, this.defaults, data);

		},

		/**
		 * @desc 渲染视图
		 * @param url <String> 访问地址
		 * */
		render: function () {

		},

		/**
		 * @desc 更新
		 * @param url <String> 访问地址
		 * */
		update: function () {

		},

		/**
		 * @desc 移除视图
		 * */
		destroy: function () {

		}
	});

	return View;
});