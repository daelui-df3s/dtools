/**
 * @description 表格
*/

define(function () {
  return {
    /**
     * @function 导出为表格
     * @param data {Array} // 表格列表数据
     * @param header {String} // 表格头，示例：`姓名,电话,邮箱`
    */
    toExport: function (data, header) {
      //列标题，逗号隔开，每一个逗号就是隔开一个单元格
      let str = header + `\n`;
      //增加\t为了不让表格显示科学计数法或者其他格式
      for(let i = 0 ; i < data.length ; i++ ){
        for(let item in data[i]){
            str+=`${data[i][item] + '\t'},`;     
        }
        str+='\n';
      }

      //encodeURIComponent解决中文乱码
      let uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(str);
      //通过创建a标签实现
      var link = document.createElement("a");
      link.href = uri;
      //对下载的文件命名
      link.download =  "json数据表.csv";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
})