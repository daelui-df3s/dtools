/**
 * Object ES5 extend
 * */

/*
 * 创建对象的原型对象
 */
Object.create = function (o) {
	if (arguments.length > 1) {
		throw new Error('Object.create implementation only accepts the first parameter.')
	}
	function F() {}
	F.prototype = o
	return new F()
}

/*
 * 返回一个所有元素为字符串的数组，其元素来自于从给定的对象上面可直接枚举的属性
 */
Object.keys = function(o) {
	if (o !== Object(o)) {
		throw new TypeError('Object.keys called on a non-object')
	}
	var k=[], p
	for (p in o) {
		if (Object.prototype.hasOwnProperty.call(o,p)) {
			k.push(p)
		}
	}
	return k
}