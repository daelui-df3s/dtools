/*
 *编辑器
 **/
define(['kindeditor', 'tool', 'applications'], function(_, tool, applications){
	var editor = {
		build : function(options){
            var me = this, $el =  tool.get$el(options.el), temp;
            if($el.length == 1){
            	var id = $el.attr('id') || me.getId();
            	$el.attr('id', $el.attr('id') || id);
            	options.id = id;
            	temp = me._build(options);
                return temp;
            }else if($el.length > 1){
                var arr = [];
                $el.each(function(){
                    var $el = $(this),
                    	opts = $.extend({}, options, {el : this}),
                    	id = $el.attr('id') || me.getId();
                    $el.attr('id', id);
                    opts.id = id;
            		temp = me._build(opts);
                    arr.push(temp);
                });
                return arr;
            }
            return options;
		},

		//创建编辑器
		_build : function(options){
            options = $.extend({
                themeType : 'simple',
                resizeType : 1,
                upload_url : 'name',
                filePostName : 'file',
                uploadJson : applications.UPLOADURL,
                fileManagerJson : applications.FILEURL,
                allowFileManager : false,
                afterCreate : function(){this.sync();},
                afterBlur : function(){this.sync();}
            }, options);
	        var editor = KindEditor.create('#' + options.id, options);
	        return editor;
		},

		//获取ID
		getId : function(){
			return 'editor_' + tool.rand(4);
		}

	};

	return editor;
});