/*
 * @desc 基类
 * */
define(function() {
    var classer = {
        //继承
        extend : function(prop){
            return Classer.extend(prop);
        },

        //获取类
        getClass : function(){
            return Classer;
        }
    };

    //继承属性并生成新的类
    var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/,
        Classer = function(){};
    Classer.prototype = {
        //复制属性到自身
        copyProps : function(opts){
            for(var i in opts){
                this[i] = opts[i];
            }
            return this;
        },
        //返回$元素
        get$el : function(){
            if(!this.$el){
                this.$el = $('body');
            }
            return this.$el;
        }
    };

    Classer.extend = function(prop) {
        var father = this, _super = father.prototype;
        initializing = true;
        var prototype = new father();
        initializing = false;
        //多继承
        if (prop['extends']) {
            var extendArr = prop['extends'];
            if(extendArr){
                extendArr = Object.prototype.toString.call(extendArr) == '[object Array]'? extendArr : [extendArr];
                prop['extends'] = extendArr;
                for(var m = 0, temp; m < extendArr.length; m++){
                    if(typeof extendArr[m] != 'function') break;
                    initializing = true;
                    temp = new extendArr[m]();
                    for(var sub in temp){
                        prototype[sub] = typeof prop[sub] == "function" &&
                            typeof temp[sub] == "function" && fnTest.test(prop[sub]) ?
                            (function(sub, fn, _super){
                                return function() {
                                    var tmp = this._super;
                                    this._super = _super[sub];
                                    var ret = fn.apply(this, arguments);
                                    this._super = tmp;
                                    return ret;
                                };
                            })(sub, prop[sub], temp) : temp[sub];
                    }
                    initializing = false;
                }
            }
        }
        for (var key in prop) {
            prototype[key] = typeof prop[key] == "function" &&
                typeof _super[key] == "function" && fnTest.test(prop[key]) ?
                (function(key, fn, _super){
                    return function() {
                        var tmp = this._super;
                        this._super = _super[key];
                        var ret = fn.apply(this, arguments);
                        this._super = tmp;
                        return ret;
                    };
                })(key, prop[key], _super) : prop[key];
        }
        function Class() {
            if ( !initializing && this.init ) this.init.apply(this, arguments);
        }
        Class.prototype = prototype;
        Class.prototype.constructor = Class;
        Class.prototype._superClass = _super;
        //静态方法继承
        for(key in father){
            if(typeof father[key] == "function" && key != 'extend') Class[key] = father[key];
        }
        Class.extend = arguments.callee;
        return Class;
    };

    //静态属性添加
    Classer.extendStatic = function(key, value){
        var me = this;
        if(key){
            if(typeof key != 'object' && key != null){
                me[key] = value;
            }else{
                for(var i in key){
                    me[i] = key[i];
                }
            }
        }
        return this;
    };

    return classer;
});