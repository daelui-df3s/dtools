/**
 * @description 路由配置文件
*/

// 仅本地增、删、改
let permission = {
  // hosts: 'localhost',
  rules: ['admin']
}

/* 自定义控制器 */
const router = {
  routes: [
    {
      path: '/dtools/inter', // 接口
      controllerName: 'inter',
      tableName: 'inter',
      children: [
        {
          path: 'list/all',
          method: 'get',
          action: 'queryAll',
          excute: {
            fields: [],
            operators: [
              {field: 'id', operator: 'EQUAL'},
              {field: 'did', operator: 'EQUAL'},
              {field: 'name', operator: 'LIKE'},
              {field: 'href', operator: 'LIKE'},
              {field: 'data', operator: 'LIKE'},
              {field: 'createTime', operator: 'BETWEEN'},
              {field: 'updateTime', operator: 'BETWEEN'}
            ]
          }
        },
        {
          path: 'item',
          method: 'get',
          action: 'query'
        },
        {
          path: 'item',
          method: 'put',
          action: 'add',
          permission
        },
        {
          path: 'item',
          method: 'post',
          action: 'update',
          permission
        },
        {
          path: 'item',
          method: 'delete',
          action: 'deleteAll',
          permission
        }
      ]
    },
    {
      path: '/dtools/rsa', // 接口
      controller: require('../modules/rsa/controller.js'),
      children: [
        {
          path: 'genrsa',
          method: 'post',
          action: 'genrsa',
          isCheckToken: false
        }
      ]
    },
    // {
    //   path: '/dtools/ocr', // 接口
    //   controller: require('../modules/ocr/controller.js'),
    //   children: [
    //     {
    //       path: 'resolve',
    //       method: 'post',
    //       action: 'resolveText',
    //       isCheckToken: false
    //     }
    //   ]
    // }
  ]
}

module.exports = router