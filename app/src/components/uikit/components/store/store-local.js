/**
 * @description 内存存储
*/

import Store from './store'

class StoreMemory extends Store {

}

// 默认实例
const storeMemory = new StoreMemory
export default StoreMemory
export {
  storeMemory
}