/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 监视者类
 */

class Watcher {
    /**
     * @function 构造方法
     */
    constructor () {
        this.init.apply(this, arguments)
    }

    /**
     * @function 初始化
     * @param target {target} // 被监听者
     * @param key {String} // 被监听者
     * @param action {Function} // 监听处理动作方法
    */
    init (target, key, action) {

    }

    /**
     * @function 数据流入
    */
    input () {

    }

    /**
     * @function 数据输出
    */
    output () {

    }
}

