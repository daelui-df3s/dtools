/**
 * Object ES6 extend
 * */

/**
 * 拷贝源对象自身的并且可枚举的属性到目标对象
 * @return {Object} arg1 复制的对象1
 * @return {Object} arg2 复制的对象2
 */
Object.assign = function(deep, arg1, arg2){
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	if ( typeof target === "boolean" ) {
		deep = target;
		target = arguments[1] || {};
		i = 2;
	}

	if ( typeof target !== "object" && !this.isFunction(target) ) {
		target = {};
	}

	if ( length === i ) {
		target = this;
		--i;
	}

	for ( ; i < length; i++ ) {
		options = arguments[ i ];
		if ( options !== null && this.isObject(options) && !this.isDOM(options) && !this.isElementNode(options)) {
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];
				if ( target === copy ) {
					continue;
				}
				if ( deep && copy && ( this.isPlainObject(copy) || (copyIsArray = this.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && this.isArray(src) ? src : [];
					} else {
						clone = src && this.isPlainObject(src) ? src : {};
					}
					target[ name ] = this.extend( deep, clone, copy );
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}
	return target;
}