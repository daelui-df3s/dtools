/**
 * @description 拖拽指令
*/

import Vue from 'vue'
import './v-drag.less'

class DragerDirect {
  //初始化类方法
  constructor (options) {
    //拖拽鼠标样式
    this.cursor = 'move'
    //边界限制
    this.boundaryLimit = false
    this.$el = options.$el
    this.$dragEl = options.$dragEl
    this.options = options
    this.initDrag()
  }
  
  //初始化拖拽
  initDrag () {
      var me = this,
          dragStatus = false,
          oldStyle = null,
          oldPos = null;
      this.get$el().on({
        mouseover : function(){
          // me.setCursor(me.cursor);
        },
          mousedown : function(e){
              //防止右键影响
              if(e.button == 2) return !1;
              try{
                  if(window.event){//IE
                      me.get$el().get(0).setCapture();
                  }else if(window.captureEvents){//FF
                      window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP|Event.STARTSELECT);
                  }
              }catch(e){}
              dragStatus = true;
              me.downCall(e);
              me.setCursor(me.cursor);
              oldPos = me.getMousePosition(e);
              oldStyle = me.coordinate();
              return false;
          }
      });
      $(document).on({
          mousemove : function(e){
              if(dragStatus){
                  me.dragCall(oldStyle, oldPos, me.getMousePosition(e), e);
                  return false;
              }
          },
          mouseup : function(e){
              if(dragStatus){
                  dragStatus = false;
                  me.setCursor();
                  me.upCall(oldStyle, oldPos, me.getMousePosition(e), e);
                  try{
                      if(window.event){
                          me.get$el().get(0).releaseCapture();
                      }else if(window.releaseEvents){
                          window.releaseEvents(Event.MOUSEMOVE|Event.MOUSEUP|Event.STARTSELECT);
                      }
                  }catch(e){}
              }
          }
      });
  }
  
  //坐标获取与设置
  coordinate (coord) {
      var $dragEl = this.get$dragEl();
      if(coord){
        $dragEl.css(coord);
      }else return {
          left : parseInt($dragEl.css('left')) || 0,
          top : parseInt($dragEl.css('top')) || 0,
          width : parseInt($dragEl.css('width')) || 0,
          height : parseInt($dragEl.css('height')) || 0
      };
  }
  
  //按下时调用
  downCall (e) {
    this.$dragEl.addClass('drag-ing')
    if (this.options.downCall) {
      this.options.downCall(this.options, e)
    }
  }

  //放开时调用
  upCall (oldStyle, oldPos, mouPos, e) {
    this.$dragEl.removeClass('drag-ing')
    if (this.options.upCall) {
      this.options.upCall(this.options, oldStyle, oldPos, mouPos, e)
    }
  }
  
  /*
   * @desc拖拽过程需要执行的操作
   * @param Object oldStyle
   * @param Object oldPos
   * @param Object newPos
   * @param Event e
   */
  dragCall (oldStyle, oldPos, newPos, e) {
      var position = this.getNewPosition.apply(this, arguments);
      this.coordinate(position);
  }
  
  //获取鼠标位置坐标
  getMousePosition (e) {
      var posx = 0, posy = 0;
      if (!e) var e = window.event;
      if (e.pageX || e.pageY) {
          posx = e.pageX;
          posy = e.pageY;
      }else if (e.clientX || e.clientY) {
          posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
          posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
      return {x : posx, y : posy};
  }

  //获取新的坐标
  getNewPosition (oldStyle, oldPos, newPos, e) {
      var $el = this.get$el(),
          spanX = newPos.x - oldPos.x,
          spanY = newPos.y - oldPos.y,
          style = this.coordinate(),
          elW = style.width,
          elH = style.height,
          newElemTop = oldStyle.top + spanY,
          newElemLeft = oldStyle.left + spanX, range;
      if(this.boundaryLimit){
          range = this.getRange();
          newElemTop = newElemTop > range.h - elH ? range.h - elH : newElemTop;
          newElemLeft = newElemLeft > range.w - elW ? range.w - elW : newElemLeft;
          newElemLeft < 0 && (newElemLeft = 0);
          newElemTop < 0 && (newElemTop = 0);
      }
      return {
          top : newElemTop,
          left : newElemLeft
      };
  }
  
  //获取拖拽范围
  getRange () {
      var $win = $(window);
      return {w : parseFloat($win.width()), h : parseFloat($win.height())};
  }
  
  setCursor (cursor) {
      var $el = this.get$el();
      if(!this.selfCursor){
          this.selfCursor = $el.css('cursor') || 'default';
      }
      if(!cursor) cursor = this.selfCursor;
      $el.css('cursor', cursor);
  }
  
  //获取元素
  get$el () {
      return this.$el;
  }

  //获取拖拽元素
  get$dragEl () {
    return this.$dragEl;
  }
}

// 拖拽绑定指令
Vue.directive('drag', {
  inserted (el, binding) {
    let value = binding.value
    // 按下时回调
    if (value.bindCall) {
      let options = value.bindCall(el, value)
      // 添加拖拽
      new DragerDirect(Object.assign(value, options))
    }
  }
})