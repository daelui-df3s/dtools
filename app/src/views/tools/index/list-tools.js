/*
 * @description 工具列表
 * @author: Rid King 
 * @since 2019-12-22 23:43:07 
 */

export default [
  {
    text: '格式化',
    children: [
      {text: 'JSON格式化', icon: 'iconJson', href: 'json', input: true, output: true},
      {text: 'JS/HTML格式化', icon: 'iconoutline-java-script', href: 'js'},
      {text: 'CSS格式化', icon: 'iconcss', href: 'css'}
    ]
  },
  {
    text: '服务',
    children: [
      {text: 'HTTP测试', icon: 'iconceshi', href: 'proxy', input: true, output: true},
      {text: 'WebSocket测试', icon: 'iconceshi', href: 'websocket', input: false, output: false},
      {text: 'STOMP测试', icon: 'iconceshi', href: 'stomp', input: false, output: false}
    ]
  },
  {
    text: '文本处理转换',
    children: [
      {text: '大小写转换', icon: 'icondaxiaoxie', href: 'uplower', input: true, output: true},
      {text: '字符统计', icon: 'icontongji', href: 'count', input: true},
      {text: '字符替换', icon: 'icontihuan', href: 'replace', input: true, output: true},
      {text: '文本比对', icon: 'iconhechabidui', href: 'compare', input: false, output: false},
      {text: '日期转换', icon: 'iconriqi', href: 'date', input: true, output: true},
      {text: '转化文件', icon: 'iconwenjian-zhuanhuafangan', href: 'tofile', input: true, output: true},
      {text: '图标示例', icon: 'icontubiao-09', href: 'iconfont', input: true, output: true},
      {text: '随机数', icon: 'iconsuijishushengcheng', href: 'random', input: false, output: false}
    ]
  },
  {
    text: '编译',
    children: [
      {text: 'ES6转ES5', icon: 'iconesjiazai', href: 'es6-es5', input: true, output: true},
      {text: 'Less', icon: 'iconless', href: 'less', input: true, output: true},
      {text: '模板编译', icon: 'iconbianyi', href: 'tpl', input: true, output: true},
      {text: '控制台', icon: 'iconkongzhitai', href: 'console', input: true, output: true}
    ]
  },
  {
    text: '加密与解密',
    children: [
      {text: 'MD5', icon: 'iconMD', href: 'md5', input: true, output: true},
      {text: 'RSA', icon: 'iconjiami', href: 'rsa', input: true, output: true},
      {text: 'HASH', icon: 'icona-hsmjiamifuwu', href: 'hash', input: true, output: true},
      {text: 'Base64', icon: 'iconbase', href: 'base64', input: true, output: true},
      {text: '16进制转换', icon: 'iconurl', href: 'urlhex', input: true, output: true},
      {text: 'URL转码', icon: 'iconzhuanma', href: 'encode', input: true, output: true}
    ]
  },
  {
    text: '图片',
    children: [
      {text: '雪碧图', icon: 'iconxuebi', href: 'sprite'},
      {text: '流程图', icon: 'iconliuchengtu', href: 'flow'},
      {text: '二维码', icon: 'iconerweima', href: 'qrcode'},
      {text: '文本识别', icon: 'iconwenbenshibie', href: 'ocr'},
      {text: '下载', icon: 'iconceshi', href: 'image-download'},
      {text: 'SVG预览', icon: 'iconsvg', href: 'svg'}
    ]
  },
  {
    text: '颜色',
    children: [
      {text: '色值转换', icon: 'iconyanse', href: 'rgb'}
    ]
  },
  {
    text: '单位转换',
    children: [
      {text: '长度转换', icon: 'iconchangdu', href: 'length', input: false, output: false},
      {text: '面积转换', icon: 'iconchangdu', href: 'area', input: false, output: false},
      {text: '重量转换', icon: 'iconchangdu', href: 'weight', input: false, output: false},
      {text: '时间转换', icon: 'iconchangdu', href: 'time', input: false, output: false},
    ]
  },
  {
    text: '文档集',
    children: [
      {text: '符号大全', icon: 'iconfuhao', href: 'symbol'},
      {text: 'HTML符号', icon: 'iconhtml', href: 'contrast'},
      {text: '配色集合', icon: 'iconyanse1', href: 'colours'},
      {text: '常用正则', icon: 'iconzhengze', href: 'regexp'},
      {text: '按键KeyCode', icon: 'iconanjian', href: 'keycode'},
      {text: 'HTTP状态码', icon: 'iconHTTPzhuangtai-01', href: 'httpcode'},
      {text: 'Content-Type', icon: 'iconcontentType', href: 'contenttype'},
      {text: 'ASCII对照表', icon: 'iconasciidoc', href: 'ascii'},
      {text: 'TCP/UDP端口', icon: 'iconduankou', href: 'port'},
      {text: 'Base64编码', icon: 'iconbase', href: 'base64code'},
      {text: '语言代码', icon: 'iconhtml', href: 'lang'}
    ]
  }
]