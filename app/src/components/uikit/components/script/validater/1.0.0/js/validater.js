/*
 * @desc 验证
 * */
define('validater', ['jquery', 'jqValidateExt', 'dialoger', 'tool', 'cruder'], function($, _1, dialoger, tool, cruder) {
    var validater = {
            validate : function(opts){
                $(opts.el).each(function(){
                    var $this = $(this), validator;
                    opts = $.extend({}, {
                        invalidHandler: function (e, element) {
                            var errors = validator.numberOfInvalids();
                            if (errors && opts.alertShow) {
                                var error = element.errorList[0], msg = $(error.element).attr(opts.validName) + ':' + error.message;
                                dialoger.error({msg : msg});
                            }
                        }
                    }, defaults, tool.parseOptions(this), opts);

                    //配置检测
                    if(opts.blurValid !== false){
                        $.extend(opts, {
                            //生成错误信息元素
                            errorPlacement : function ($errorLabel, $element) {
                                var $container;
                                if(opts.errorParent) $container = $element.parents(opts.errorParent);
                                //所有错误指定容器
                                else if(opts.errorContainer){
                                    $container = $(opts.errorContainer).empty();
                                }
                                //单个错误指定容器
                                if($container && opts.selfErrorContainer){
                                    $container = $container.find(opts.selfErrorContainer).empty();
                                }
                                $element.get(0).errorContainer = $container;
                                $errorLabel.get(0).errorContainer = $container;
                                $container.addClass(opts.invalidClass).append($errorLabel);
                            },
                            //显示错误信息
                            showErrors : function(errorMap, errorList) {
                                if( this.errorList.length ) {
                                    this.toShow = this.toShow.add( this.containers );
                                }
                                if (this.settings.success) {
                                    for ( var i = 0; this.successList[i]; i++ ) {
                                        this.showLabel( this.successList[i] );
                                    }
                                }
                                for ( var i = 0; this.errorList[i]; i++ ) {
                                    var error = this.errorList[i];
                                    this.settings.highlight && this.settings.highlight.call( this, error.element, this.settings.errorClass, this.settings.validClass );
                                    this.showLabel( error.element, error.message );
                                    var $container = error.element.errorContainer;
                                    if($container){
                                        $container.addClass(opts.invalidClass);
                                        if(opts.errorShowAble) $container.show();
                                    }
                                }
                                if (this.settings.unhighlight) {
                                    for ( var i = 0, elements = this.validElements(); elements[i]; i++ ) {
                                        this.settings.unhighlight.call( this, elements[i], this.settings.errorClass, this.settings.validClass );
                                    }
                                }
                                this.toHide = this.toHide.not( this.toShow );
                                this.hideErrors();
                                this.addWrapper( this.toShow ).show();
                            },
                            //成功操作
                            success : function($label) {
                                var $container = $label.get(0).errorContainer
                                if($container){
                                    $container.removeClass(opts.invalidClass);
                                    if(opts.errorShowAble) $container.hide();
                                }
                            },
                            //自定义高亮
                            highlight: function(element, errorClass, validClass) {
                                var errorContainer = element.errorContainer;
                                if(errorContainer) errorContainer.removeClass(validClass);
                            },
                            unhighlight: function(element, errorClass, validClass) {
                                var errorContainer = element.errorContainer;
                                if(errorContainer) errorContainer.addClass(validClass);
                            },
                            //失去焦点检测
                            onfocusout : function(element){
                                $(element).valid();
                            }
                        });
                    }

                    validator = $this.on('submit',function (e) {
                        if (!$(this).valid()) {
                            return false;
                        } else {
                            if(typeof opts.onSubmit == 'function'){
                                var result = opts.onSubmit();
                            }else if(opts.submit === false || opts.onSubmit === false){
                                result = false;
                            }else if($.isFunction(opts.callback)){
                                result = opts.callback();
                            }
                            if(result === false){
                                preventDefault(e);
                                return false;
                            }

                            if(opts.ajax == false){
                                return true;
                            }
                            cruder.ajaxForm({
                                form : $this
                            });
                            return false;
                        }
                    }).validate(opts);

                    function preventDefault(e){
                        e = e || window.event; // Event对象
                        if (e.preventDefault) { // 标准浏览器
                            e.preventDefault();
                        } else { // IE浏览器
                            window.event.returnValue = false;
                        }
                    }
                });
            },

            //检测验证
            isValid : function(el){
                return $(el).valid();
            }
        },

    //默认配置
    defaults = {
        //显示名键
        validName : 'data-valid-name',
        //以ajax发送表单
        ajax : true,
        //失去焦点时检测
        blurValidate : true,
        //错误信息父级容器
        errorParent : '.form-grep',
        //统一的错误信息父级容器
        errorContainer : null,
        //单个错误信息父级容器
        selfErrorContainer : null,
        //控制错误显示与隐藏
        errorShowAble : false,
        //验证通过类名
        validClass : 'has-success',
        //错误类名
        invalidClass : 'has-error',
        //生成的错误元素类型
        errorClass : 'help-block',
        //以弹出形式显示错误
        alertShow : false
    };

    return validater;

});