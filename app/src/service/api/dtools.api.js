/**
* @description 小工具合集(※自动化生成,勿手动更改※)
*/

export default {
	"proxy": {
		"url": "{proxy}",
		"desc": "代理请求数据服务",
		"method": "post"
	},
	"queryInterList": {
		"url": "{host}/dtools/inter/list/all",
		"desc": "查询接口列表服务",
		"method": "get"
	},
	"insertInter": {
		"url": "{host}/dtools/inter/item",
		"desc": "添加接口",
		"method": "put"
	},
	"updateInter": {
		"url": "{host}/dtools/inter/item",
		"desc": "更新接口",
		"method": "post"
	},
	"deleteInter": {
		"url": "{host}/dtools/inter/item",
		"desc": "删除接口",
		"method": "delete"
	},
	"genrsa": {
		"url": "{host}/dtools/rsa/genrsa",
		"desc": "生成RSA公私钥",
		"method": "post"
	},
	"queryOcrText": {
		"url": "{host}/dtools/ocr/resolve",
		"desc": "ocr识别文本",
		"method": "post"
	}
}
