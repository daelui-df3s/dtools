/**
 * 条码扫描
 */
define(['jquery'], function ($) {
	var barcoder = {
		scan : function(options){
			options = options || {};
			var scaner = new plus.barcode.Barcode(options.id || 'barcode');
			this.scaner = scaner;
			scaner.onmarked = function (type, result) {
				typeof options.success === 'function' && options.success(type, result);
			};
			scaner.start();
		},

		//关闭
		close : function(){
			if(this.scaner){
				this.scaner.close();
			}
		}
	}

	return barcoder;
});