/**
 * @description 服务端配置文件
*/

let constant = require('./constant.config')
let db = require('./db.config')
let auth = require('./auth.config')

module.exports = {
  // 配置变量
  constant: constant,
  // 数据库配置
  db: db,
  // 权限
  auth: auth
}