﻿/**
 * @description JS打包
*/

import './base.js'
import style_html from './htmlformat.js'
import js_beautify from './jsformat.js'

const jspacker = {
    doJsBeautify(text, tabsize) {
        var js_source = String(text || '').replace(/^\s+/, '')
        var tabchar = ' '
        if (tabsize === 1) {
            tabchar = '\t'
        }
        if (js_source && js_source.charAt(0) === '<') {
            return style_html(js_source, tabsize, tabchar, 80)
        } else {
            return js_beautify(js_source, tabsize, tabchar)
        }
    },
    packJs(text, base64) {
        var input = text
        var packer = new Packer
        if (base64) {
            var output = packer.pack(input, 1, 0)
        } else {
            var output = packer.pack(input, 0, 0)
        }
        return output
    }
}

export default jspacker
