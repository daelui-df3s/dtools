module.exports = function(grunt) {

    //package.json相对地址
    grunt.packdir = '../../../../../../../';

	grunt.tasks = ['lesslint', 'sasslint', 'less', 'sass'];

    //任务1
    grunt.bombFiles = ['./shining/1.1.0'];

    //引入公用gruntfile.js
    require(grunt.packdir + 'gruntfile-bomb.js')(grunt);

};