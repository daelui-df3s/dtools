/**
 * @description fetch服务
 * @author Rid King
 * @since 2018-07-06
 */

import objecter from '../../../objecter/2.0.0/js/objecter'
import urler from '../../../urler/2.0.0/js/urler'
import Service from '../../../service/2.0.0/js/service'

/**
 * @class fetch服务类
*/
class FetchService extends Service {
  /**
   * @desc 发送请求
   * @param options <Object> 配置
   * @return <Promise>
   * */
  _send (options) {
    let me = this
    // 复制配置
    options = objecter.clone(options)
    // 是否超时
    let isTimeout = false
    // 请求参数
    let params =  ''

    // 如果是GET请求，拼接url
    if (/get/i.test(options.method)) {
      if (options.data) {
        options.url += '?' + urler.stringify(options.data)
      }
      options.data = ''
    } else {
      // 数据为数组格式时，请求设置为json
      if (Array.isArray(options.data)) {
        options.headers['Content-Type'] = 'application/json;charset=UTF-8'
      }
      else if (objecter.isObject(options.data)) {
        // 检查数据中是否含有对象
        let hasObject = false
        for (let key in options.data) {
          if (objecter.isObject(options.data[key])) {
            hasObject = true
            break
          }
        }
        // 数据中含有对象
        if (hasObject) {
          options.headers['Content-Type'] = 'application/json;charset=UTF-8'
        }
      }

      // 参数设置
      if (options.data) {
        if (/json/.test(options.headers['Content-Type'])) {
          params = JSON.stringify(options.data)
        } else {
          params = urler.stringify(options.data)
        }
      }
      options.body = params
    }
    // 生成Request对象
    let request = new Request(options.url, options)

    // 超时处理
    if (options.timeout > 50) {
      let timer = setTimeout(function () {
        isTimeout = true
      }, options.timeout)
    }

    return fetch(request).then((res) => {
      // 超时
      if (isTimeout) {
        return Promise.reject(res)
      }

      // 失败
      if(res.status !== 200){
        return Promise.reject(res)
      }

      // 结束回调
      if (typeof options.complete === 'function') {
        options.complete({xhr, status, options, responseText})
      }

      // 正常
      return Promise.resolve(res)
    }, (err) => {
      // 超时检测
      if (isTimeout) {
        return Promise.reject(err)
      }

      // 失败
      return Promise.reject(err)
    })
    // 自定义处理
    .then(res => {
      return requester.interceptors.response._resolve(res)
    }, err => {
      return requester.interceptors.response._reject(err)
    })
  }

  /**
   * @function 默认配置
   * @return {Object}
  */
  getDefaults () {
    /**
     * 默认配置
     * {
     *  url : <String>, 访问地址
     *  method : <String>, 访问类型，GET, POST, PUT, DELETE, HEAD
     *  data : <Object>, 数据
     *  body : <ArrayBuffer, ArrayBufferView(Uint8Array and friends), Blob/File, String, URLSearchParams, FormData>, 传输的数据
     *  dataType : <String>, 返回值类型
     *  headers : <Object>, 关联的Header对象
     *  referrer : <String>,
     *  mode : <String>, 请求的模式，主要用于跨域设置，cors, no-cors, same-origin
     *  credentials : <String>, 是否发送Cookie omit, same-origin
     *  redirect : <String>, 收到重定向请求之后的操作，follow, error, manual
     *  integrity : <String>, 完整性校验
     *  cache : <String>, 缓存模式(default, reload, no-cache)
     *  timeout: <Int> // 超时时间，默认-1永不过期
     * }
    */
    return {
      url: '',
      method: 'POST',
      data: '',
      mode: 'cors',
      redirect: 'follow',
      credentials: 'include',
      timeout: -1, // 默认-1永不过期
      dataType: 'object',
      // 请求头对象
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        post: {},
        get: {},
        put: {},
        delete: {}
      }
    }
  }
}

export default new FetchService
export {
  FetchService
}