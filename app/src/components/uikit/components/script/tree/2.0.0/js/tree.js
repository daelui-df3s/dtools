/**
 * @description 树形
 * @sinc 2019-02-17
 * @author Rid King
*/

class Tree {
  /**
   * @functin 生成树形文本
   * @param {Object} data 树形数据
   * @return {String}
   * @demo
    ├─css //css目录
    │  │─modules // 模块css目录
    │  │  ├─index
    │  │  ├─user
    │  │  └─product
    │  └─core.css // 核心样式文件
    ├─fonts  // 字体图标目录
    ├─images // 图片资源目录
    │─js // 脚本目录
    │  └─modules // 各模块组件
    │─core.js // 基础核心库
    └─lib.js // 库文件
  */
  static toTreeText (data) {

  }

  /**
   * @function 转换为数组
   * @param {Array/Object} data 树数据
   * @param {Object} [options] 配置项
   * @return {Array}
  */
  static toArray (data, options) {
    if (!Array.isArray(data)) {
      data = [data]
    }
    options = options || {}
    options.level = options.level || 0
    // 子键
    options.childKey = options.childKey || 'children'
    let childKey = options.childKey
    let list = []
    data.forEach(item => {
      if (item) {
        item.$_level = options.level
        list.push(item)
        // 子级
        if (Array.isArray(item[childKey])){
          list = list.concat(Tree.toArray(
            item[childKey], Object.assign({}, options, {level: options.level + 1})
          ))
        }
        delete item[childKey]
      }
    })

    return list
  }
}

export default Tree