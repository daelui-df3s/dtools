export default {
  // 文章列表
  articleList: state => {
    return state.articleList.filter(todo => todo.done)
  },

  // 接口状态
  getInterStatus: (state, getters) => (id, isHtml) => {
    let status = '-'
    state.interStatusList.forEach(item => {
      if (id === item.id) {
        if (isHtml) {
          status = `<span class="${item.cls} ico-pointer">${item.value}</span>`
        }
        else {
          status = item.value
        }
      }
    })

    return status
  }
}
