/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 条件判断
 * demo: <div dog-else-if="isAble">Test</div>
 */

import {DIRECTIVE_MODE} from './directive.js'

export default {
    name: 'dog-else-if',
    scope: {
        /**
         * @property 指令编译模式
         * @description 分为编译指令与运行时指令
         * */
        mode: DIRECTIVE_MODE.COMPILE,
        /**
         * @property 指令排序
         * @description 值越小优先级越高
         * */
        order: 1.04,
        /**
         * @function 解析参数
         * @param value {String} // 指令值
         * @param code {String} // 编译码
        */
        resolve (value, code) {
            code = 'else if(' + value + '){return ' + code + '}'

            return code
        },
        /**
         * @function 编译方法
         * @param b {Boolean} // 条件
         * @param fn {Function} // 处理方法
        */
        compile (b, fn) {
           let code = []
           // 结果为正则添加该元素
            if (b) {
                code.push(fn())
            }

          return code
        }
   }
}