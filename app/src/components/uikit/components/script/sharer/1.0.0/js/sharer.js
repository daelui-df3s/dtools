/**
 * @description 分享链接
 */
define(['jquery', 'qrcoder'], function(){
    var sharer = {
        share : function(type, options){
            if(typeof type != 'string') return;
            var cat = this.cats[type];
            if(cat){
                this._share(cat, options);
            }
        },

        _share : function(cat, options){
            var info = this.getInfo(),
                url = cat.build($.extend(info, options));
            if(url != false){
                //重新打开一个新的窗体
                window.open(url);
            }
        },

        //分享类型
        cats : {
            qq : {
                url : 'http://connect.qq.com/widget/shareqq/index.html',
                build : function(options){
                    return this.url + '?url=' + options.host_url + '&title=' + options.title + '&pics=' + options.pic;
                }
            },

            zone : {
                url : 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey',
                build : function(options){
                    return this.url + '?url=' + options.host_url + '&title=' + options.title + '&pics=' + options.pic;
                }
            },

            wechat : {
                url : '',
                build : function(options){
                    require(['qrcoder'], function(qrcoder){
                        qrcoder.show({
                            text : options.url
                        });
                    });
                    return false;
                }
            },

            py : {
                url : '',
                build : function(options){
                    require(['qrcoder'], function(qrcoder){
                        qrcoder.show({
                            text : options.url
                        });
                    });
                    return false;
                }
            },

            txweibo : {
                url : 'http://share.v.t.qq.com/index.php',
                build : function(options){
                    return this.url + "?url=" + options.host_url + "&title=" + options.title + "&pic=" + options.pic;
                }
            },

            txpy : {
                url : 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?to=pengyou',
                build : function(options){
                    return this.url + "url=" + options.host_url + "&title=" + options.title + "&pic=" + options.pic;
                }
            },

            sina : {
                url : 'http://service.weibo.com/share/share.php',
                build : function(options){
                    return this.url + "?url=" + options.host_url + "&title=" + options.title + "&pic=" + options.pic;
                }
            },

            ren : {
                url : 'http://widget.renren.com/dialog/share',
                build : function(options){
                    return this.url + '?resourceUrl=' + options.host_url + '&srcUrl=' + options.host_url + '&title=' + options.title + '&pic=' + options.pic;
                }
            },

            dou : {
                url : 'http://shuo.douban.com/!service/share',
                build : function(options){
                    return this.url + '?href=' + options.host_url + '&name=' + options.title + '&image=' + options.pic;
                }
            },

            bd : {
                url : 'http://i.baidu.com/store',
                build : function(options){
                    return this.url + '?url=' + options.host_url + '&t=' + options.title;
                }
            },

            yx : {
                url : '',
                build : function(options){
                    require(['qrcoder'], function(qrcoder){
                        qrcoder.show({
                            text : options.url
                        });
                    });
                    return false;
                }
            },

            ms : {
                url : '',
                build : function(options){
                    return false;
                }
            }
        },

        //获取网页信息
        getInfo : function(){
            var host_url = document.location; //host_url获取当前的路径
            var title = $("title").text();
            var pic = $(".logo img").attr("src");
            return {
                host_url : host_url,
                title : title,
                pic : pic,
                desc : title
            };
        },

        //初始邦定
        initShare : function(){
            var me = this;
            $('[data-share]').click(function(){
                var name = $(this).attr('data-share');
                me.share(name);
            });
        }
    };

    return sharer;
});

/*
 新浪微博：
 http://service.weibo.com/share/share.php?url=

 count=表示是否显示当前页面被分享数量(1显示)(可选，允许为空)
 &url=将页面地址转成短域名，并显示在内容文字后面。(可选，允许为空)
 &appkey=用于发布微博的来源显示，为空则分享的内容来源会显示来自互联网。(可选，允许为空)
 &title=分享时所示的文字内容，为空则自动抓取分享页面的title值(可选，允许为空)
 &pic=自定义图片地址，作为微博配图(可选，允许为空)
 &ralateUid=转发时会@相关的微博账号(可选，允许为空)
 &language=语言设置(zh_cn|zh_tw)(可选)

 腾讯微博：
 http://share.v.t.qq.com/index.php?c=share&a=index

 &title=默认的文本内容或RICH化转播时的消息体标题，RICH化时最多15个全角字符的长度
 &url=转播页的url
 &pic=需要转播的图片url，多张以|连接
 &appkey=填写正确的appkey，转播后将显示该key的来源
 &line1=消息体第一行的文字，最多15个全角字符的长度
 &line2=消息体第二行的文字，最多15个全角字符的长度
 &line3=消息体第三行的文字，最多15个全角字符的长度

 API文档：http://wiki.open.t.qq.com/index.php/一键转播rich化参数详解

 腾讯朋友
 http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?to=pengyou

 url
 title
 desc
 summary

 人人网：
 http://widget.renren.com/dialog/share?

 resourceUrl=分享的资源Url
 &srcUrl=分享的资源来源Url,默认为header中的Referer,如果分享失败可以调整此值为resourceUrl试试
 &pic=分享的主题图片Url
 &title=分享的标题
 &description=分享的详细描述

 API文档：http://dev.renren.com/website/?widget=rrshare&content=use

 QQ好友
 http://connect.qq.com/widget/shareqq/index.html?

 url
 desc
 pics
 site

 QQ空间：
 http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?

 url=分享的网址
 &desc=默认分享理由(可选)
 &summary=分享摘要(可选)
 &title=分享标题(可选)
 &site=分享来源 如：腾讯网(可选)
 &pics=分享图片的路径(可选)

 API文档：http://connect.qq.com/intro/share/

 豆瓣：
 http://shuo.douban.com/!service/share?

 image=分享图片
 &href=分享网址
 &name=分享标题
 &text=分享内容

 API文档：http://open.weixin.qq.com/document/api/timeline/?lang=zh_CN

 百度个人中心
 http://i.baidu.com/store
 url
 t

 * */

