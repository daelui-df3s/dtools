import '@daelui/roosterui/dist/pc/css/boost.min.css'
import '@/components/style/animate/3.5.2/css/animate.min.css'
import '@/components/vue/transition/transition.css'
import './assets/iconfont/iconfont.css'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import './directives/index'
import messager from './components/messager/messager.js'
import eventBus from '@/components/vue/components/event-bus.js'
import config from '@/micros/config'
import store from './store'

Vue.config.productionTip = false
Vue.use(ElementUI)
const win = typeof self !== 'undefined' ? window : global

// 是否运行态
const isProd = /production|single/i.test(process.env.NODE_ENV)
const isDev = /dev/i.test(process.env.NODE_ENV)
const isSingle = /single/i.test(process.env.NODE_ENV)
const ap = (window.application || {}).dtools || {}
ap.mode = ap[isProd ? 'prod' : 'dev']
const mode = ap.mode
// 初始化模块配置
const loader = mode.loader || {}
const rootUrl = loader.rootUrl || location.origin
win.MonacoWorkerBaseUrl = isDev ? '/static/' : isSingle ? '/app/static/' : rootUrl + '/monaco-editor/'

// 路径处理
let microPath = ''
if (window.__POWERED_BY_QIANKUN__) {
  microPath = config.microPath
}
Vue.prototype.getMicroPath = function (path) {
  return path.replace('#', '#' + microPath).replace(/\/+/, '/')
}
Vue.prototype.getMicroRoute = function (path) {
  return path.replace(/^\//, '/' + microPath + '/').replace(/\/+/, '/')
}

// jquery
import $ from './components/jquery/1.10.2/jquery.min'
window.jQuery = window.$ = $

// 请求服务绑定事件，验证登录
import requester from './service/components/requester'
requester.$on('onAfterRequest', function (result) {
  if (result && String(result.status) === '401') {
    // 跳转页面
    // dialoger.confirm({
    //   msg: '无操作权限，是否跳转到登录页面?'
    // }).then(result => {
    //   if (result) {
    //     router.push('/login')
    //   }
    // })
    // 弹出窗口
    eventBus.$emit('onShowLogin')
  } else {
    if (result && String(result.status) === '0') {
      messager.error('请求服务失败')
    }
  }
})

/* 原始启动代码 */
if (!window.__POWERED_BY_QIANKUN__) {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#micro-app')
}

// debugger加载
;(function(){
  let win = typeof self !== 'undefined' ? window: global
  let href = (win.location || {}).href
  let b = /debugger=true/i.test(href)
  if (!b) return
  if (win.$pig) {
    win.$pig.load('eruda').then(eruda => {
      eruda.init()
    })
  } else {
    var script = document.createElement('script')
    let src = location.origin + '/pool/prod/packages/eruda/1.2.6/eruda.min.js'
    script.src = src
    script.async = true
    document.getElementsByTagName('head')[0].appendChild(script)
    script.onload = function () {
      eruda.init()
    }
  }
})();

export default {
  Vue, router, store, App
}

export {
  Vue, router, store, App
}