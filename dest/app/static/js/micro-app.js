/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["dtools"] = factory();
	else
		root["dtools"] = factory();
})(self, function() {
return /******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/loader.js":
/*!***********************!*\
  !*** ./src/loader.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _daelui_pigjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @daelui/pigjs */ \"./node_modules/@daelui/pigjs/dist/index.js\");\n/* harmony import */ var _daelui_pigjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_daelui_pigjs__WEBPACK_IMPORTED_MODULE_0__);\n/**\r\n * @description 应用加载器\r\n * */\n\n\nconst isProd = /production|single/i.test(\"single\");\n// 是否运行态\nconst ap = (window.application || {}).dtools || {};\nap.mode = ap[isProd ? 'prod' : 'dev'];\n// 初始化模块配置\nconst loader = ap.mode.loader || {};\nconst domain = ap.mode.domain || location.origin;\nconst rootUrl = loader.rootUrl || location.origin;\n/* harmony default export */ __webpack_exports__[\"default\"] = (_daelui_pigjs__WEBPACK_IMPORTED_MODULE_0___default().init({\n  path: loader.path || rootUrl + '@daelui/pigjs/latest/dist/system.min.js',\n  registry: loader.registry || location.origin + '/components.json',\n  rootUrl: rootUrl,\n  rootUrls: loader.rootUrls,\n  versions: [{\n    name: 'vue',\n    version: '2.*.*'\n  }]\n}).then(function () {\n  _daelui_pigjs__WEBPACK_IMPORTED_MODULE_0___default()[\"import\"](domain + '/df3s/dtools/app/static/css/chunk-vendors.css');\n  _daelui_pigjs__WEBPACK_IMPORTED_MODULE_0___default()[\"import\"](domain + '/df3s/dtools/app/static/css/main.css');\n  _daelui_pigjs__WEBPACK_IMPORTED_MODULE_0___default()[\"import\"](domain + '/df3s/dtools/app/static/js/chunk-vendors.js');\n  return _daelui_pigjs__WEBPACK_IMPORTED_MODULE_0___default()[\"import\"](domain + '/df3s/dtools/app/static/js/main.js');\n}));\n\n//# sourceURL=webpack://dtools/./src/loader.js?");

/***/ }),

/***/ "./src/micro-app.js":
/*!**************************!*\
  !*** ./src/micro-app.js ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   bootstrap: function() { return /* binding */ bootstrap; },\n/* harmony export */   mount: function() { return /* binding */ mount; },\n/* harmony export */   unmount: function() { return /* binding */ unmount; }\n/* harmony export */ });\n/* harmony import */ var _micros_public_path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/micros/public-path */ \"./src/micros/public-path.js\");\n/* harmony import */ var _micros_public_path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_micros_public_path__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _micros_micros_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/micros/micros-actions */ \"./src/micros/micros-actions.js\");\n/* harmony import */ var _micros_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/micros/config */ \"./src/micros/config.js\");\n/* harmony import */ var _loader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loader */ \"./src/loader.js\");\n\n\n\n\n\n// 用于保存vue实例\nlet instance = null;\n\n/** *  * 渲染函数 * 两种情况：主应用生命周期钩子中运行 / 微应用单独启动时运行 */\nfunction render(props) {\n  _loader__WEBPACK_IMPORTED_MODULE_3__[\"default\"].then(mod => {\n    const {\n      Vue,\n      router,\n      store,\n      App\n    } = mod;\n    console.log('子应用render的参数', props);\n    // 新增判断，如果是独立运行不执行onGlobalStateChange\n    if (window.__POWERED_BY_QIANKUN__) {\n      if (props) {\n        // 注入 actions 实例\n        _micros_micros_actions__WEBPACK_IMPORTED_MODULE_1__[\"default\"].setActions(props);\n      }\n\n      // 挂载主应用传入路由实例 用于子应用跳转主应用\n      Vue.prototype.$microRouter = props.router;\n      props.onGlobalStateChange((state, prevState) => {\n        // state: 变更后的状态; prev 变更前的状态\n        console.log('通信状态发生改变：', state, prevState);\n        store.commit('setToken', state.globalToken);\n      }, true);\n    }\n\n    // router不再是同一个实例，而是每次mount的时候都会新获取一个实例\n    // 路由守卫\n    // router.beforeEach((to, from, next) => {\n    //   if (to.path !== (microPath + '/login')) {\n    //     if (store.state.token) {\n    //       console.log('已经登录 token=', store.state.token)\n    //       if (window.__POWERED_BY_QIANKUN__ && !to.path.includes('micro-app')) {\n    //         next(microPath + to.path)\n    //       } else {\n    //         next()\n    //       }\n    //     } else {\n    //       console.log('子应用 - 未登录 请登录')\n    //       next(microPath + '/login')\n    //     }\n    //   } else {\n    //     next()\n    //   }\n    // })\n    instance = new Vue({\n      router,\n      store,\n      render: h => h(App)\n    }).$mount('#micro-app');\n  }).catch(e => {\n    console.log(e);\n  });\n}\n\n/** \r\n*  \r\n* bootstrap 只会在微应用初始化的时候调用一次，\r\n  下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发 bootstrap。 \r\n* 通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等。 \r\n*/\nasync function bootstrap() {\n  console.log('VueMicroApp bootstraped');\n}\n\n/** \r\n*  \r\n* 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法 \r\n*/\nasync function mount(props) {\n  console.log('VueMicroApp mount', props);\n  render(props);\n}\n/** \r\n*  \r\n* 应用每次 切出/卸载 会调用的方法，通常在这里我们会卸载微应用的应用实例 \r\n*/\nasync function unmount() {\n  console.log('VueMicroApp unmount');\n  instance && instance.$destroy();\n  instance = null;\n}\n\n// 独立运行时，直接挂载应用\nif (!window.__POWERED_BY_QIANKUN__) {\n  render();\n}\n\n//# sourceURL=webpack://dtools/./src/micro-app.js?");

/***/ }),

/***/ "./src/micros/micros-actions.js":
/*!**************************************!*\
  !*** ./src/micros/micros-actions.js ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }\nfunction _toPropertyKey(t) { var i = _toPrimitive(t, \"string\"); return \"symbol\" == typeof i ? i : i + \"\"; }\nfunction _toPrimitive(t, r) { if (\"object\" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || \"default\"); if (\"object\" != typeof i) return i; throw new TypeError(\"@@toPrimitive must return a primitive value.\"); } return (\"string\" === r ? String : Number)(t); }\nfunction emptyAction() {\n  // 警告：提示当前使用的是空 Action\n  console.warn(\"Current execute action is empty!\");\n}\nclass Actions {\n  constructor() {\n    // 默认值为空 Action\n    _defineProperty(this, \"actions\", {\n      onGlobalStateChange: emptyAction,\n      setGlobalState: emptyAction\n    });\n  }\n  // 设置 actions\n  setActions(actions) {\n    this.actions = actions;\n  }\n  // 映射\n  onGlobalStateChange() {\n    return this.actions.onGlobalStateChange(...arguments);\n  }\n  // 映射\n  setGlobalState() {\n    return this.actions.setGlobalState(...arguments);\n  }\n}\nconst MicroActions = new Actions();\n/* harmony default export */ __webpack_exports__[\"default\"] = (MicroActions);\n\n//# sourceURL=webpack://dtools/./src/micros/micros-actions.js?");

/***/ }),

/***/ "./src/micros/public-path.js":
/*!***********************************!*\
  !*** ./src/micros/public-path.js ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __unused_webpack_exports, __webpack_require__) {

eval("// 新增：动态设置 webpack publicPath，防止资源加载出错\nif (window.__POWERED_BY_QIANKUN__) {\n  // __POWERED_BY_QIANKUN__ 使用qiankun初始化的属性\n  __webpack_require__.p = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__;\n}\n\n//# sourceURL=webpack://dtools/./src/micros/public-path.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/amd options */
/******/ 	!function() {
/******/ 		__webpack_require__.amdO = {};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	!function() {
/******/ 		// The chunk loading function for additional chunks
/******/ 		// Since all referenced chunks are already included
/******/ 		// in this file, this function is empty here.
/******/ 		__webpack_require__.e = function() { return Promise.resolve(); };
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	!function() {
/******/ 		__webpack_require__.nmd = function(module) {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		__webpack_require__.p = "";
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		__webpack_require__.b = document.baseURI || self.location.href;
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"micro-app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkdtools"] = self["webpackChunkdtools"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["chunk-vendors","chunk-common"], function() { return __webpack_require__("./src/micro-app.js"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});