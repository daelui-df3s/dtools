/**
 * @description json对象
 */
define( function (objecter) {
	var jsonAble = false;
    //判断浏览器是否支持
    if(typeof window.JSON !== 'object'){
	    jsonAble = true;
    }

    var jsoner = {
	    /**
	     * @desc json对象转化为字符串
	     * @param data <Object> 转化的对象
	     * */
      stringify: function(data){
		    if (!objecter.isObject(data)) {
			    data = String(data);
		    }
		    else if (jsonAble) {
			    data = JSON.stringify(data);
		    } else {
			    data = toString(data);
		    }

		    return data;

		    /**
		     * @desc 对象转化为字符串
		     * @param data <Object> 转化的对象
		     * */
        function toString (data) {
          var arr = [], str = '';
          if (objecter.isArray(data)) {
            for (var i = 0; i < data.length; i++)
              arr.push(toString(data[i]));
            str = '[' + arr.join(',') + ']';
          }
          else if (objecter.isDate(data)) {
            str = 'new Date(' + data.getTime() + ')';
          }
          else if (objecter.isRegexp(data) || objecter.isFunction(data)) {
            str = data.toString();
          }
          else if (objecter.isPlainObject(data)) {
            for (var i in data) {
              data[i] = typeof (data[i]) == 'string' ? '"' + data[i] + '"' : (typeof (data[i]) === 'object' ? toString(data[i]) : data[i]);
              arr.push(i + ':' + data[i]);
            }
            str = '{' + arr.join(',') + '}';
          }

          return str;
        }
      },

      /**
	     * @desc 字符串转化为json对象
	     * @param data <String> json字符串
	     * */
      parse: function (data) {
        data = (objecter.isString(data) ? data : '{}').replace(/[\n\r\f\t\v]/g, ' ');
        if (jsonAble) {
          data = JSON.parse(data);
        } else if (/^\s*[\[\{].*[\}\]]\s*$/.test(text)) {
          data = toJSON(data);
        }

		    return data;

		    /**
		     * @desc 字符串转化为json对象
		     * @param data <String> json字符串
		     * */
	      function toJSON (text) {
          if(typeof text === 'string' && /^\s*[\[\{].*[\}\]]\s*$/.test(text)){
            try{
              text = a(text) || b(text);
            }catch(e){
              text = {};
            }
          }

          return text;

          // 方式一
          function a(str){
            var json = (new Function("return " + str))();
            return json;
          }

        // 方式二
          function b(str){
            var json = eval('(' + str + ')');
            return json;
          }
        }
      }
    }

    return jsoner
})