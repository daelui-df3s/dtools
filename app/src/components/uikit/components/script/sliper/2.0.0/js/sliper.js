/*
 * 滑块
 * */
define(['classer', 'jquery', 'eventer', 'objecter'], function(classer, $, eventer, objecter){
	var Sliper = classer.extend({
		init : function(options){
			this.setOptions(objecter.extend({}, Sliper.defaults, options));
		},

		//前进
		forward : function(options){
			options.current.animate = options.current.animate || 'pushRightToCenter';
			options.replace.animate = options.replace.animate || 'pushCenterToLeft';
			this.replace(options);
		},

		//后退
		back : function(options){
			options.current.animate = options.current.animate || 'pushLeftToCenter';
			options.replace.animate = options.replace.animate || 'pushCenterToRight';
			this.replace(options);
		},

		//侧栏
		sidebar : function(options){
			options = objecter.extend(true, {
				replace : {
					animateIn : 'slip-move-main-center-to-right',
					animateOut : 'slip-move-main-right-to-center'
				},
				current : {
					animateIn : 'slip-move-left-to-center',
					animateOut : 'slip-move-center-to-left'
				}
			}, options);

			return this.move(options);
		},

		//弹出
		popup : function(options){
			options = objecter.extend(true, {
				current : {
					animateIn : 'coverBottomToCenter',
					animateOut : 'coverCenterToBottom'
				}
			}, options);

			return this.move(options);
		},

		//操作表
		action : function(options){
			options = objecter.extend(true, {
				current : {
					cls : 'slip-action',
					animateIn : 'moveBottomToRoot',
					animateOut : 'moveRootToBottom'
				}
			}, options);

			return this.move(options);
		},

		//滑块替换
		replace : function(options){
			var opts = this.getOptions(),
				$current = $(options.current.el),
				$replace = (options.replace && options.replace.el) ? $(options.replace.el) : null;

			//判断效果元素是否相同
			if(!$replace || ($current.get(0) == $replace.get(0))){
				$replace = null;
			}

			//额外类名
			if(options.current.cls){
				$current.addClass(options.current.cls);
			}
			if($replace && options.replace.cls) {
				$replace.addClass(options.replace.cls);
			}

			//初始定位样式
			$current.addClass(opts.position);

			//添加作用类名
			$current.addClass(opts.active);
			if($replace){
				$replace.addClass(opts.active);
			}

			//添加基本类名
			$current.addClass(opts.base);
			if($replace) {
				$replace.addClass(opts.base);
			}

			//添加效果类名
			var currentAnimate = opts.animates[options.current.animate],
				replaceAnimate = opts.animates[options.replace.animate];
			//指定键值类名不存在
			if(!currentAnimate){
				currentAnimate = options.current.animate || '';
			}
			if(!replaceAnimate){
				replaceAnimate = options.replace.animate || '';
			}

			setTimeout(function(){
				$current.addClass(currentAnimate);
				if($replace) {
					$replace.addClass(replaceAnimate);
				}

				//删除所有效果类名
				setTimeout(function(){
					$current.removeClass(currentAnimate + ' ' + opts.base + ' ' + opts.position);//$current.removeClass(currentAnimate + ' ' + opts.base + ' ' + opts.active);
					if($replace) {
						$replace.removeClass(replaceAnimate + ' ' + opts.active);
					}
				}, 420);
			}, 30);
		},

		//滑块挪动
		move : function(options){
			var opts = this.getOptions(),
				$current = $(options.current.el),
				$replace = (options.replace && options.replace.el) ? $(options.replace.el) : null;

			options.replace = options.replace || {};

			//判断效果元素是否相同
			if(!$replace || ($current.get(0) == $replace.get(0))){
				$replace = null;
			}

			//额外类名
			if(options.current.cls){
				$current.addClass(options.current.cls);
			}
			if($replace && options.replace.cls) {
				$replace.addClass(options.replace.cls);
			}

			//初始定位样式
			$current.addClass(opts.position);

			//添加作用类名
			$current.addClass(opts.active);
			if($replace) {
				$replace.addClass(opts.active);
			}

			//添加基本类名
			$current.addClass(opts.base);
			if($replace) {
				$replace.addClass(opts.base);
			}

			//添加效果类名
			var currentAnimateIn = opts.animates[options.current.animateIn],
				currentAnimateOut = opts.animates[options.current.animateOut],
				replaceAnimateIn = opts.animates[options.replace.animateIn],
				replaceAnimateOut = opts.animates[options.replace.animateOut];
			//指定键值类名不存在
			if(!currentAnimateIn){
				currentAnimateIn = options.current.animateIn || '';
			}
			if(!currentAnimateOut){
				currentAnimateOut = options.current.animateOut || '';
			}
			if(!replaceAnimateIn){
				replaceAnimateIn = options.replace.animateIn || '';
			}
			if(!replaceAnimateOut){
				replaceAnimateOut = options.replace.animateOut || '';
			}

			var $overlay;
			setTimeout(function(){
				$current.addClass(currentAnimateIn);
				if($replace) {
					$replace.addClass(replaceAnimateIn);
					$overlay = $replace.find('.slip-overlay');
					if($overlay.length < 1){
						$overlay = $('<div class="slip-overlay"></div>').appendTo($replace);
						$overlay.on(eventer.events.up, function(){
							mover.back();
						});
					}
				}else{
					$overlay = $current.find('.slip-overlay');
					if($overlay.length < 1){
						$overlay = $('<div class="slip-overlay active"></div>').insertAfter($current);
						$overlay.on(eventer.events.up, function(){
							mover.back();
						});
					}
				}
			}, 30);

			var mover = {
				back : function(){
					$current.removeClass(currentAnimateIn).addClass(currentAnimateOut);
					if($replace) {
						$replace.removeClass(replaceAnimateIn).addClass(replaceAnimateOut);
					}

					//删除所有效果类名
					setTimeout(function(){
						$current.removeClass(currentAnimateOut + ' ' + opts.active);
						if($replace) {
							$replace.removeClass(replaceAnimateOut + ' ' + opts.base);
						}
						$overlay.remove();
					}, 420);
				}
			};

			return mover;
		}
	});

	Sliper.defaults = {
		//滑块基本类名
		base : 'slip',
		//初始定位
		position : 'slip-position',
		//作用类名
		active : 'slip-active',
		//效果
		animates : {
			pushLeftToCenter : 'slip-push-left-to-center',
			pushCenterToLeft : 'slip-push-center-to-left',
			pushRightToCenter : 'slip-push-right-to-center',
			pushCenterToRight : 'slip-push-center-to-right',
			pushTopToCenter : 'slip-push-top-to-center',
			pushCenterToTop : 'slip-push-center-to-top',
			pushBottomToCenter : 'slip-push-bottom-to-center',
			pushCenterToBottom : 'slip-push-center-to-bottom',
			coverLeftToCenter : 'slip-cover-left-to-center',
			coverCenterToLeft : 'slip-cover-center-to-left',
			coverRightToCenter : 'slip-cover-right-to-center',
			coverCenterToRight : 'slip-cover-center-to-right',
			coverTopToCenter : 'slip-cover-top-to-center',
			coverCenterToTop : 'slip-cover-center-to-top',
			coverBottomToCenter : 'slip-cover-bottom-to-center',
			coverCenterToBottom : 'slip-cover-center-to-bottom',
			moveLeftToCenter : 'slip-move-left-to-center',
			moveCenterToLeft : 'slip-move-center-to-left',
			moveMainCenterToRight : 'slip-move-main-center-to-right',
			moveMainRightToCenter : 'slip-move-main-right-to-center',
			moveBottomToRoot : 'slip-move-bottom-to-root',
			moveRootToBottom : 'slip-move-root-to-bottom'
		}
	};

	return Sliper;
});