/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 组件类
 */

import Model from '../../../../../../components/script/model/2.0.0/js/model.js'
import objecter from '../../../../../../component/script/objecter/2.0.0/js/objecter.js'
import debuger from '../../../../../../components/script/debuger/2.0.0/js/debuger.js'
import Viewer from './viewer.js'

class Component extends Model {
    // /**
    //  * @description 构造方法
    //  * */
    // constructor () {

    // }

 



    /**
     * @description 组件继承
     * @param scope <Function/Object> 组件构造函数或对象
    */
    static extend (scope) {
        return {}
    }
}

export default Component