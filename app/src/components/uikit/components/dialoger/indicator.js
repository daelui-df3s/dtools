/**
 * @description 预加载指示器
*/

import objecter from '../../../core/extend/objecter.js'
import {Dialoger} from './dialoger'

class IndicatorDialoger extends Dialoger {
  /**
   * @function 获取默认配置对象
   * @return {Object}
  */
  getDefaults () {
    return {
      //指示器模板
      tpl: '<div class="modal modal-popup-indicator {{dialogerCls}}">' +
              '<span class="indicator indicator-snow"></span>' +
            '</div>',
      backdrop: true,
      overlayZIndexAuto: true
    }
  }
}

// 默认实例
const indicator = new IndicatorDialoger
export default IndicatorDialoger
export {
  indicator
}