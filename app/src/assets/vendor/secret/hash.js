import CryptoJS from './crypto-js.min.js'
import messager from '@/components/messager/messager.js'

// hash加密
export const hash = function (type, str, params) {
  params = params || {}
  let value = ''

  switch (type) {
    case 'sha1':
      value = CryptoJS.SHA1(str)
      break
    case 'sha224':
      value = CryptoJS.SHA224(str)
      break
    case 'sha256':
      value = CryptoJS.SHA256(str)
      break
    case 'sha384':
      value = CryptoJS.SHA384(str)
      break
    case 'sha512':
      value = CryptoJS.SHA512(str)
      break
    case 'md5':
      value = CryptoJS.MD5(str)
      break
    case 'hmacsha1':
      value = CryptoJS.HmacSHA1(str, params.key)
      break
    case 'hmacsha224':
      value = CryptoJS.HmacSHA224(str, params.key)
      break
    case 'hmacsha256':
      value = CryptoJS.HmacSHA256(str, params.key)
      break
    case 'hmacsha384':
      value = CryptoJS.HmacSHA384(str, params.key)
      break
    case 'hmacsha512':
      value = CryptoJS.HmacSHA512(str, params.key)
      break
    case 'hmacmd5':
      value = CryptoJS.HmacMD5(str, params.key)
      break
    case 'pbkdf2':
      let iterations = parseInt(params.iterations)
      let keySize = parseInt(params.length)
      if (isNaN(iterations)) {
        messager.error('迭代次数必须为数字')
        return ''
      }
      if (isNaN(keySize)) {
        messager.error('Key长度必须为数字')
        return ''
      }
      var salt = CryptoJS.enc.Utf8.parse(params.salt)
      var hash_str_pbkdf2 = CryptoJS.PBKDF2(
        str, salt,
        {
          keySize: keySize,
          iterations: iterations
        }
      )
      value = hash_str_pbkdf2
      break
  }

  return value
}
