/**
 * @description 管道类
 * @version 2.0.0
 * @since 2017-04-29
 * @author Rid King
 */

class Piper {
    /**
     * @function 开始,
    */
    start () {

    }

    /**
     * @function 结束
    */
    complete () {

    }

    then () {

    }

    reject () {
        
    }

    finally () {

    }
}