/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 视图模型
 * demo: dog-model="userEntity"
 */

import {DIRECTIVE_MODE} from './directive.js'

export default {
  name: 'dog-model',
  scope: {
    /**
     * @property 指令编译模式
     * @description 分为编译指令与运行时指令
     * */
    mode: DIRECTIVE_MODE.RUN,
    /**
     * @property 指令排序
     * @description 值越小优先级越高
     * */
    order: 1.02,
    /**
     * @function 编译方法
     * @param vel {VirtualElement} // 虚拟元素对象
     * @param direct {Object} // 指令属性集
     * @param viewer {Viewer} // 视图对象
    */
    compile (vel, direct, viewer) {
      // 设置元素的默认值
      vel.setValue(direct.value)
      // 元素监视模型
      vel.watchModel(direct.express, viewer)

      ///value: firstName,
      // express: "firstName"
      // console.log(vel)
      // console.log(direct)
      // console.log(viewer)
    }
  }
}