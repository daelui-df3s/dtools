/**
 * @description 滑行器
 */
define(['jquery', 'toucher', 'tool'], function($, toucher, tool){
	var slider = {
		//创建
		build : function(options){
			var $el =  tool.get$el(options.el), temp;
			if($el.length === 1){
				temp = new Slider(options);
				return temp;
			}else if($el.length > 1){
				var arr = [];
				$el.each(function(){
					var opts = $.extend({}, options, {el : this});
					temp = new Slider(opts);
					arr.push(temp);
				});
				return arr;
			}
			return options;
		},

		//获取类
		getClass : function(){
			return Slider;
		}
	};

	//滑行器
	function Slider(){
		this.init.apply(this, arguments);
	}

	Slider.prototype = {
		init : function(options){
			var me = this, $slider, $handler, $items = [];

			//配置
			this.options = $.extend({}, Slider.defaults, options);
			$slider = $(this.options.el);
			if(this.options.handlerEl){
				$handler = $(this.options.handlerEl);
			}else if(this.options.handlerCls){
				$handler = $slider.find('.' + this.options.handlerCls);
			}
			this.$slider = $slider;
			this.$handler = $handler;

			//配置项设定了宽度
			if(!isNaN(parseInt(this.options.width))){
				$slider.width(parseInt(this.options.width));
			}

			//遍历子项
			$slider.find('.' + this.options.itemCls).each(function(){
				$items.push($(this).css({position : 'relative', 'z-index' : 1, width : me.getClientWidth() + 'px'}).data('data-left', 0));
			});

			if($items.length < 2){
				return;
			}else if($items.length === 2){
				$items.push($items[0].clone());
				$items.push($items[1].clone());
				$items[0].parent().append($items[2]).append($items[3]);
			}
			if(this.$handler){
				$.each($items, function(){
					$handler.append('<' + me.options.itemHandlerTag + ' class="' + me.options.itemHandlerCls + '"></' + me.options.itemHandlerTag + '>');
				});
			}

			this.$items = $items;
			this.prevNum = $items.length - 1;
			this.nextNum = 1;
			this.curNum = 0;
			this.oldCurNum = 0;

			//初始化事件
			this.initEvent();

			//默认选中第一个
			this.active(0);
		},

		//获取宽度
		getClientWidth : function(){
			var width = parseFloat(this.options.width);
			if(!width){
				width = this.$slider.width();
			}

			return width;
		},

		//获取高度
		getClientHeight : function(){
			return parseFloat($(window).height());
		},

		//初始化事件
		initEvent : function(){
			var me = this;
			//触摸控制
			toucher.build({
				el : this.$slider,
				ontouchstart : function(){
					clearTimeout(me.intval);
					this._slider = true;
				},

				ontouchmove : function(e, start_x, start_y, end_x, end_y){
					var levelNum = end_x - start_x,
						verticalNum = end_y - start_y,
						relate;

					if(this._slider === true){
						this._slider = Math.abs(levelNum) >= Math.abs(verticalNum) ? 1 : -1;
					}

					if(this._slider === 1){
						if(levelNum < 0) relate = me.nextNum;
						else relate = me.prevNum;
						me.$items[me.curNum].css('left', me.$items[me.curNum].data('data-left') + levelNum + 'px');
						me.$items[relate].css('left', me.$items[relate].data('data-left') + levelNum + 'px');
						e.preventDefault();
					}
				},

				ontouchend : function(e, start_x, start_y, end_x, end_y){
					if(this._slider === 1) me.slide(start_x, start_y, end_x, end_y);
				},

				ontouchcancel : function(e, start_x, start_y, end_x, end_y){
					//判断是否是纵向滑动
					if(typeof end_x === 'undefined' || end_x === start_x){

					}else{
						var levelNum = end_x - start_x;
						if(levelNum > 0){
							end_x = 1000;
						}else{
							end_x = -1000;
						}
						me.slide(start_x, start_y, end_x, end_y);
					}
				}
			});
		},

		//选中
		active : function(n, result){
			var me = this, cls = this.options.activeCls,
				tag = this.options.itemHandlerTag;
			clearTimeout(this.intval);
			if(this.$handler){
				this.$handler.find(tag + '.' + cls).removeClass(cls);
				this.$handler.find(tag + ':eq(' + n + ')').addClass(cls);
			}
			me.curNum = n;
			this._move(result);
			this.intval = setTimeout(function(){
				me.oldCurNum = me.curNum;
				me.roll(n + 1, 1);
			}, this.options.intval);
		},

		//滚动控制
		roll : function (n, result){
			var len = this.$items.length;
			clearTimeout(this.intval);
			if(n > len - 1) n = 0;
			else if(n < 0) n = len - 1;
			this.active(n, result);
		},

		//滑行
		slide : function(start_x, start_y, end_x, end_y){
			var len = this.$items.length;
			if(typeof end_x === 'undefined' || end_x === start_x){
				//destroy();
				//return false;
			}else{
				var levelNum = end_x - start_x,
					halfWidth = this.getClientWidth() / 4,
					result, curLeft, prevLeft, nextLeft;
				if(halfWidth < Math.abs(levelNum)){
					//向左滑
					if(levelNum < (0 - halfWidth)) result = 1;
					//向右滑
					else if(levelNum > halfWidth) result = -1;
					this.oldCurNum = this.curNum;
					this.curNum += result;
				}else{
					if(levelNum < 0){
						this.oldCurNum = this.curNum + 1;
						this.oldCurNum = this.oldCurNum > len - 1 ? 0 : this.oldCurNum;
						result = -1;
					}else if(levelNum > 0){
						this.oldCurNum = this.curNum - 1;
						this.oldCurNum = this.oldCurNum < 0 ? len - 1 : this.oldCurNum;
						result = 1;
					}
				}
				this.roll(this.curNum, result);
			}
		},

		//移动
		_move : function (result){
			var me = this, curLeft, prevLeft, nextLeft,
				len = this.$items.length,
				timeout = this.options.timeout;
			me.curNum = me.curNum < 0 ? len - 1 : me.curNum;
			me.curNum = me.curNum > len - 1 ? 0 : me.curNum;
			var curLeft = -1 * me.curNum * me.getClientWidth();
			me.$items[me.curNum].animate({left : curLeft + 'px'}, timeout, function(){callback();}).data('data-left', curLeft);
			if(result === 1){
				prevLeft = -1 * (me.oldCurNum + 1) * me.getClientWidth();
				me.$items[me.oldCurNum].animate({left : prevLeft + 'px'}, timeout).data('data-left', prevLeft);
			}else if(result === -1){
				nextLeft = -1 * (me.oldCurNum - 1) * me.getClientWidth();
				me.$items[me.oldCurNum].animate({left : nextLeft + 'px'}, timeout).data('data-left', nextLeft);
			}

			//回调
			function callback(){
				me.prevNum = me.curNum - 1;
				me.prevNum = me.prevNum < 0 ? me.$items.length - 1 : me.prevNum;
				me.nextNum = me.curNum + 1;
				me.nextNum = me.nextNum > me.$items.length - 1 ? 0 : me.nextNum;
				prevLeft = -1 * (me.prevNum + 1) * me.getClientWidth();
				nextLeft = -1 * (me.nextNum - 1) * me.getClientWidth();
				me.$items[me.prevNum].css({left : prevLeft + 'px'}, 'fast').data('data-left', prevLeft);
				me.$items[me.nextNum].css({left : nextLeft + 'px'}, 'fast').data('data-left', nextLeft);
			}
		}

	};

	//默认配置
	Slider.defaults = {
		intval : 5000,
		timeout : 300,
		itemCls : 'item-slider',
		handlerEl : null,
		handlerCls : 'slider-handler',
		itemHandlerTag : 'li',
		itemHandlerCls : 'item-handler',
		activeCls : 'active'
	};

	return slider;
});