/**
* @description 小工具合集(※自动化生成,勿手动更改※)
*/

import requester from '../components/requester'
import resolver from '../components/resolver'
import API from '../api/dtools.api'

// api地址解析
const api = resolver.solveAPI(API)

export default {
  /**
   * @function 代理请求数据服务
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  proxy (...params) {
    return requester.post(api.proxy.url, ...params)
  },
  /**
   * @function 查询接口列表
   * @param {Object} params 参数对象
   * @return {Promise}
  */
  queryInterList (...params) {
    return requester.get(api.queryInterList.url, ...params)
  },
  insertInter (...params) {
    return requester.put(api.insertInter.url, ...params)
  },
  updateInter (...params) {
    return requester.post(api.updateInter.url, ...params)
  },
  deleteInter (...params) {
    return requester.delete(api.deleteInter.url, ...params)
  },
  genrsa (...params) {
    return requester.post(api.genrsa.url, ...params)
  },
  queryOcrText (...params) {
    return requester.post(api.queryOcrText.url, ...params)
  }
}

export {
  api
}
