const path = require('path')
//const webpack = require('webpack')
const  CopyPlugin = require('copy-webpack-plugin')

module.exports = {
  // 定义（告诉webpack）入口文件
  entry: {
    app: './main.js'
  },
  target: 'node',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, '../dist/server'), // 定义输出文件将存放的文件夹名称，这里需要绝对路径，因此开头引入path,利用path方法
    // filename: '[name].js'                    // 输出文件名称定义，这样写默认是main.js
    filename: 'main.js', //也可以定为index.js
    libraryTarget: 'commonjs'
  },
  // 排除文件
  externals: {
    'CONFIG': './config/app.config'
  },
  resolve: {
    extensions: ['.js']
  },
  node: {
    global: true,
    __filename: true,
    __dirname: true
  },
  module: {
    // rules中的每一项是一个规则
    rules:[
      {
        test: /\.js$/, // 值一个正则，符合这些正则的资源会用一个loade来处理
        use: {
          loader: 'babel-loader', // 使用bable-loader来处理
          options: { // 指定参数
            // presets: ['es2015','stage-0']
          }
        },
        exclude: ['/node_module/', '/config'] // 排除在外
      }
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: 'vendor',
          chunks: "initial",
          minChunks: 2
        }
      }
    },
    // minimize: {}
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, './app.js'),
          to: path.resolve(__dirname, "../dist/server/app.js")
        },
        {
          from: path.resolve(__dirname, './db'),
          to: path.resolve(__dirname, "../dist/server/db")
        },
        {
          from: path.resolve(__dirname, './config'),
          to: path.resolve(__dirname, "../dist/server/config")
        }
      ]
    })
  ]
}