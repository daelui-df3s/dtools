/**
 * 浏览器
 */
define(function () {
	var browser = {
		versions: (function () {
			var u = navigator.userAgent.toLowerCase(),
				isPad = false, isAndroidPad = false, isIpad = false, isMobile = false, isPc = false;

			if (u.indexOf('')){
				if (u.indexOf('android') > -1) {
					if (u.indexOf('mobile') == -1) {
						isAndroidPad = true;
					}
				}
			}

			if (u.indexOf('ipad') > -1) {
				isIpad = true;
			}

			if (isAndroidPad || isIpad) {
				isPad = true;
			} else if ((u.indexOf('mobile') > -1 && !isPad ) || (u.indexOf('android') > -1 && !isAndroidPad) || (u.indexOf('phone') > -1)) {
				isMobile = true;
			} else {
				isPc = true;
			}

			return {
				pc: isPc,
				mobile: isMobile,
				android: u.indexOf('android') > -1 || u.indexOf('Linux') > -1,
				ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
				iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
				iPad: u.indexOf('iPad') > -1, //是否iPad
				iPod: u.indexOf('iPod') > -1, //是否iPod
				pad: isPad, // 是否pad
				wx: u.toLowerCase().indexOf('micromessenger') > -1,
			};
		})(),

		os : function(){
			var ua = navigator.userAgent;
			if(ua.match(/android/ig)){
				return 'android';
			}else if(ua.match(/iphone/ig)){
				if(ua.match(/os (9|[1-9]\d)/ig)){
					return 'iphone_ios9';
				}else{
					return 'iphone';
				}
			}
		}
	};

	return browser;
});