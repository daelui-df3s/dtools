/**
 * 调试
 */
define(function () {
    var debuger = {
        //记录
        log : function(msg, code, data){
            console.log([msg, code, data]);
        },

        error : function(msg, code, data){
            console.error([msg, code, data]);
        }
    };

    return debuger;

});