/**
 * 钟
 * */
define(function () {
	var bell = {
		/**
		 * @desc 睡眠
		 * @param milliSeconds <Number> 睡眠毫秒数
		 * */
		sleep : function (milliSeconds) {
			var startTime = new Date().getTime();
			while (new Date().getTime() < startTime + milliSeconds);

			/*
			 return new Promise((reslove, reject) => {
				 setTimeout(function(){
				    reslove();
				 }, milliSeconds)
			 })
			 */
		},

		/**
		 * @desc 周期执行
		 * @param cycleTime <Int> 周期毫秒数
		 * @param caller <Function> 周期执行函数
		 * @param maxTime <Int> 最大时间毫秒数
		 * @param finalCaller <Function> 总计回调
		 * */
		cycleCall: function (cycleTime, caller, maxTime, finalCaller) {
			cycleTime = parseInt(cycleTime)
			cycleTime = isNaN(cycleTime) ? 1000 : cycleTime
			caller = typeof caller === 'function' ? caller : function () {}

			// 启动执行
			loop(cycleTime, caller);

			// 周期函数
			var count = 0
			fucntion loop (time, fn) {
				setTimeout(function () {
					count += time
					// 总计时回调
					if(maxTimecount >= maxTime) {
						typeof finalCaller === 'function' && finalCaller(count, cycleTime)
						return false
					}
					// 单次回调
					fn(count)
					// 周期调用
					loop(time, fn)
				}, time)
			}
		},

		/**
		 * @desc 布尔监测执行
		 * @param func1 <Function> 条件函数通过返回bool值决定是否执行函数2
		 * @param func2 <Function> 执行函数
		 * @param max <Int> 最大检测次数
		 * */
		boolCall : function(fun1, fun2, max){
			max = max || 200;
			var n = 0;
			function loopCall(){
				if(n > max) return;
				var b = fun1();
				if(b === true){
					fun2();
				}else{
					setTimeout(function(){
						loopCall();
					}, 30);
					n += 1;
				}
			}

			loopCall();
		},

		/**
		 * @desc 计数执行
		 * @param callback <Function> 执行函数
		 * @param el <Selector> 选择器
		 * @param maxNum <Int> 最大检测次数
		 * @param event <String> 绑定的事件名称
		 * @param maxTime <Int> 最大保存时间毫秒数
		 * */
		numCall : function(callback, el, maxNum, event, maxTime){
			var n = 0, timeout;
			event = event || 'click';
			maxNum = maxNum || 5;
			maxTime = maxTime || 400;
			el = el || 'body';
			$(el).on(event, function(){
				clearTimeout(timeout);
				n += 1;
				if(n >= maxNum){
					n = 0;
					callback();
				}
				timeout = setTimeout(function(){
					n = 0;
				}, maxTime);
			});
		}
	};
});