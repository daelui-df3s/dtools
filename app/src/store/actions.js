import * as types from './mutation-types'

export default {
  // 评论
  comment ({commit}, {data}) {
    return Api.comment.add({data}).then(res => {
      return Promise.resolve(res)
    }).catch((error) => {
      return Promise.reject(error)
    })
  }
}

/*
//请求数据
export function getLoginUser({commit}) {
  api.user.getUser().then(res => {
    let user = res.data.user
    commit(types.INIT_USER_INFO, {
      user
    })
  })
}

//直接commit操作
export function setLang({ commit }, lang) {
  commit(types.UPDATE_LANG, lang)
}

//直接commit操作
export function getUser({commit}, callback = () => {}) {
  api.user.getUser().then(res => {
    let user = res.data
    commit(types.INIT_TIMETIME, {
      user
    })
    callback()
  })
}

export function updateUser({commit}, { mid, type }) {
  commit(types.UPDATE_TIMETIME, {
    mid,
    type
  })
}
*/
