/**
 * Object extend
 * */

/*
 * 遍历
 */
Object.forEach = function (obj, func) {
	if (Array.isArray(obj)) {
		obj.forEach(func)
	} else if (typeof obj === 'string') {
		var n = 0
		for (var item in obj) {
			var b = func(obj[item], n, obj)
			n += 1
			if (b === false) {
				break
			}
		}
	}  else if (typeof obj === 'object' && obj !== null) {
		for (var property in obj) {
			if (obj.hasOwnProperty(property)) {
				var b = func(obj[property], property, obj);
				if (b === false) {
					break
				}
			}
		}
	}
}