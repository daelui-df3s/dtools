import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const state = {
  /*
  * 系统配置
  * */
  // 语言
  lang: 'zh',

  /*
   * 公用配置
   * */
  // 显示加载状态
  showPreloader: false,

  /*
   * 模块数据
   * */
  // 首页文章列表
  indexArticleList: null,

  // 文章列表
  articleList: null,
  // 文章
  article: {},

  // 管理员
  master: null,
  // 用户列表
  userList: null,
  // 用户
  user: null,

  // 模块列表
  modules: [],
  // 正在编辑的模块
  editingModule: null,
  // 正在编辑的接口
  editingInter: null,
  // 模块类型列表
  moduleTypeList: [
    {id: 1, value: '服务模块', tip: '服务模块：根据配置生成api与service'},
    {id: 2, value: '开发辅助模块', tip: '开发辅助模块：仅提供测试服务'}
  ],
  // 接口状态列表
  interStatusList: [
    {id: 1, value: '新建', cls: 'text-gray'},
    {id: -2, value: '标记', cls: 'text-sign', tag: 'sign'},
    {id: 2, value: '开发中', cls: 'text-cyan'},
    {id: 3, value: '联调中', cls: 'text-info'},
    {id: 4, value: '使用中', cls: 'text-success'},
    {id: 5, value: '不赞成', cls: 'text-warn'},
    {id: -1, value: '已废弃', cls: 'text-remove', tag: 'remove'}
  ],
  // 服务列表
  serveList: [
    // 示例
    // {
    //   serve: 'dev',
    //   name: '开发环境',
    //   rules: [
    //     {rule: '{host}', url: 'http://127.0.0.1'},
    //     {rule: '{server}', url: 'http://127.0.0.1'}
    //   ],
    //   proxy: {
    //     host: 'http://127.0.0.1',
    //     port: 3117
    //   }
    // }
  ]
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
