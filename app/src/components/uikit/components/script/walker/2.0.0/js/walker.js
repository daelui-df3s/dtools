/*
 * 载入器
 * */
define(['classer', 'jquery', 'ajaxer', 'sliper', 'objecter'], function(classer, $, ajaxer, Sliper, objecter){
	var walker = classer.extend({
			init: function (options) {
				this.setOptions(objecter.extend({}, walker.defaults, options));
				//链路数组
				this._chain = [];
				//滑块数组
				this._slips = [];
			},

			//显示
			load : function(options){
				options = objecter.extend(true, {}, this.getOptions(), options);
				options.current = options.current || {};
				options.replace = options.replace || {};
				var me = this, $wrapper = $(options.wrapperEl),
					active = options.active,
					current = options.current,
					replace = options.replace,
					$active = options.replace.$el || $wrapper.children('.' + active.active),
					$el = current.$el || this.getMark$el(current.el);
				replace.$el = replace.$el || $active;
				replace = this.mark(replace);

				//远程加载
				if(!$el && !/^\s*(#|\.).+/.test(current.el)){
					//以ajax模式加载
					ajaxer.get({
						url : current.el,
						dataType : 'text',
						success : function(html){
							var $page = $(html);
							$page.addClass(options.base);
							if(current.frame){
								$page.find('iframe').attr('src', current.frame);
							}
							$wrapper.append($page);
							current.$el = $page;
							current = me.mark(current);
							me._change({
								current : current,
								replace : replace,
								type : options.type || 'forward'
							});
						}
					});
				}
				//本地加载
				else{
					current.$el = $el || $(current.el);
					current = this.mark(current);
					this._change({
						current : current,
						replace : replace,
						type : options.type || 'forward'
					});
				}
			},

			//前进
			active : function(options){
				options = objecter.extend(true, {}, this.getOptions(), options);
				var $wrapper = $(options.wrapperEl),
					active = options.active,
					$active = options.$active || $wrapper.children('.' + active.active),
					$el = options.$el || this.getMark$el(options.el) || $(options.el);
				$wrapper.children('.' + options.page).addClass(active.base).removeClass(active.active);
				if($el.length < 1){
					ajaxer.get({
						url : options.el,
						dataType : 'text',
						success : function(html){
							var $page = $(html);
							$page.removeClass(active.base);
							$active.addClass(active.base).removeClass(active.active);
							$page.addClass(active.active).removeClass(active.base);
							$wrapper.append($page);
						}
					});
				}else{
					$active.addClass(active.base).removeClass(active.active);
					$el.addClass(active.active).removeClass(active.base);
				}
			},

			//前进
			forward : function(options){
				options = options || {};
				this.load({
					current : options,
					type : 'forward'
				});
			},

			//后退
			back : function(){
				var chain = this._chain,
					len = chain.length;
				if(len > 1){
					var options = {
						current : chain[len - 2],
						replace : chain[len - 1],
						type : 'back'
					};
					this.load(options);
				}
			},

			//链路控制
			_change : function(options){
				var current = options.current,
					replace = options.replace,
					type = options.type,
					chain = this._chain,
					len = chain.length, _type;
				if(len === 0){
					chain[0] = replace;
					chain[1] = current;
					_type = 'forward';
				}else if(len === 1){
					if(chain[0].name === replace.name){
						chain[1] = current;
					}
					_type = 'forward';
				}else{
					var arr = [], max = len - 1, isAdd = false;
					for(var i = 0, m = -1, n = -1; i < len; i++){
						if(chain[i].name === current.name){
							m = i;
						}
						if(chain[i].name === replace.name){
							n = i;
						}
					}
					if(m !== -1 && n !== -1){
						if(m > n){
							_type = 'forward';
						}else{
							_type = 'back';
						}
						max = m;
					}else if(n !== -1){
						max = n;
						isAdd = true;
					}else{
						max = m;
					}

					for(var i = 0; i <= max; i++){
						arr[i] = chain[i];
					}
					if(isAdd){
						arr[arr.length] = current;
					}
					chain = arr;
				}

				//滑动
				if(type){
					var $replace$el = replace.$el;
					if(replace.name === current.name){
						$replace$el = null;
					}else{
						type = _type || type;
					}
					sliper[type]({
						current : {
							el : current.$el
						},
						replace : {
							el : $replace$el
						}
					});
				}

				this._chain = chain;
			},

			//标记
			mark : function(options){
				var name = undefined,
					markName = options.name,
					$el = options.$el,
					b1 = false, slip = undefined;

				//属性添加
				if($el && $el.length > 0){
					name = $el.attr('data-slip');
					b1 = true;
					if(!name){
						name = markName || 'slip-' + String(Math.random()).replace('.', '');
						$el.attr('data-slip', name);
					}
					options.name = name;
				}

				//数组中检测是否在在
				for(var i = 0; i < this._slips.length; i++){
					if(this._slips[i].name === name){
						slip = this._slips[i];
						break;
					}
				}

				//添加进数组
				if(b1 && !slip){
					if(!options.el){
						var el = $el.attr('id');
						el = el || 'slip_' + String(Math.random()).replace('.', '');
						$el.attr('id', el);
						options.el = '#' + el;
					}
					slip = options;
					this._slips.push(slip);
				}
				slip = slip || options;

				return slip;
			},

			//获取标记元素
			getMark$el : function(name){
				var slips = this._slips, len = slips.length, $el;
				for(var i = 0; i < len; i++){
					if(slips[i].el === name){
						$el = slips[i].$el;
						break;
					}
				}

				return $el;
			}
		}),

		// 滑块器
		sliper = new Sliper();

	// 默认配置
	walker.defaults = {
		//页面容器
		wrapperEl : '.wrapper',
		//页面
		page : 'page',
		//基本类名
		base : sliper.getOptions('base'),
		//选中类名
		active : sliper.getOptions('active')
	};

	return new walker;
});