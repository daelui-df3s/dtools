/**
 * @description 枚举
 * @since 2019-02-16
 * @author Rid King
*/

/**
 * @class 枚举类
*/
class DEnum extends Array {
  /**
   * @function 构造方法
   * @param {Object} obj 初始化对象
  */
  constructor (obj) {
    super(obj)
    this.shift()
    // 值存储
    // this._map = new Map()
    if (!obj) {
      return false
    }
    // 存储数据
    for(let key in obj) {
      if (obj.hasOwnProperty(key)) {
        this.push(obj[key])
        this[key] = obj[key]
      }
    }
  }

  // /**
  //  * @function 获取值
  //  * @return {any}
  // */
  // get (key) {
  //   return this._map.get(key)
  // }

  // /**
  //  * @function 设置键值
  //  * @param {String} key 键名
  //  * @param {String} value 值
  // */
  // set (key, value) {
  //   this._map.set(key, value)
  //   this[key] = value
  // }
}

export default DEnum