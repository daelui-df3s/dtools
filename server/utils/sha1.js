const crypto = require('crypto')

function sha1 (str) {
  const hash = crypto.createHash('sha1')
  hash.update(str)
  const res = hash.digest('hex')
  return res
}

module.exports = sha1