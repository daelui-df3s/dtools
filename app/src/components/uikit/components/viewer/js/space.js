/**
 * @author Rid King
 * @since 2017-05-21
 * @version 2.0.0
 * @description 视图空间类
 */

import Model from '../../../../../../components/script/model/2.0.0/js/model.js'

class Space extends Model {
    /**
     * @description 构造方法
     * @param name <Object> // 空间名
     * @param dependencies <Array> // 依赖数组或空间、组件、指令
    */
    constructor (name, dependencies) {
        super(params)
        // 添加依赖
        this.depends(dependencies)
        // 组件列表
        this.componentList = []
        // 指令列表
        this.directiveList = []
    }

    /**
     * @description 添加依赖
     * @param params <Array/Object> // 依赖数组或空间、组件、指令
    */
    depends (params) {

    }

    /**
     * @description 定义组件
     * @param name <String> 组件名称
     * @param scope <Function/Object> 组件构造函数或对象
    */
    component (name, scope) {
        // 生成控制对象
        scope = this.packScope(scope)
        this.componentList.push({
            name: name,
            scope: scope
        })
    }

    /**
     * @description 定义指令
     * @param name <String> 指令名称
     * @param scope <Function/Object> 指令构造函数或对象
    */
    directive (name, scope) {
        // 生成控制对象
        scope = this.packScope(scope)
        this.directiveList.push({
            name: name,
            scope: scope
        })
        // 元素
        // 属性
        // 事件
        // 值
    }

    /**
     * @description 包装控制域对象
     * @param scope <Function/Object> 控制域构造函数或对象
    */
    static packScope (scope) {
        // 构造函数
        if (objecter.isFunction(scope)) {
            let Fn = function () {}
            let constructor = scope
            Fn.prototype.constructor = function () {
                let result = constructor.call(this)
                if (objecter.isPlainObject(result)) {
                    for (var prop in result) {
                        this[prop] = result[prop]
                    }
                }
            }

            // 生成控制域对象
            scope = new Fn()
        }
        // 非对象
        else if (!scope) {
            scope = {}
        }

        return scope
    }
}

export default Space