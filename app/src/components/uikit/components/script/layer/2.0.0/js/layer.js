/**
 * @description 层级
*/

const layer = {
  // 层级数值
  zIndex: 20,

  /**
   * @function 获取层级
   * @return {Number}
  */
  getZIndex () {
    return this.zIndex
  },

  /**
   * @function 层级增加
   * @return {Number}
  */
  increase () {
    this.zIndex += 5
    return this.zIndex
  }
}

export default layer