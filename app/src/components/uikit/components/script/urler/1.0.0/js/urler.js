/**
 * 统一资源定位
 * */
define(function () {
	var url = {
		/**
		 * @desc json对象转化为参数字符串,即a=1&b=2形式
		 * @param obj <Object> 被转化的对象
		 * @param options <Object> 被转化的对象
		 * {
		 *  key : <String>, 参数键名
		 *  replacer : <String>, 访问地址
		 *  levMax : <Int>, 最大层级
		 *  levNow : <Int> 当前层级
		 * }
		 * @param replacer <Function> 编码字符串的函数
		 * @param level <Number> 控制数据等级,避免无限嵌套，默认5层
		 * @return <String>
		 * */
		formatRequestParams: function (param, options) {
			var paramStr = ''
			options = options || {}
			let replacer = options.replacer || function (value) {
				return encodeURIComponent(value)
			}
			var levMax = options.levMax || 5,
				levNow = options.levNow || 1
			if (levNow > levMax) {
				paramStr += '&' + options.key + '=' + replacer(String(param))
			}
			else if (!(typeof param === 'object')) {
				paramStr += '&' + options.key + '=' + replacer(String(param))
			} else {
				param.each(function (item, i) {
					var k = (options.key === null || options.key === undefined) ?
							i : options.key + (param instanceof Array ? '[' + i + ']' : '.' + i),
						lev = levNow + 1
					paramStr += '&' + this.formatRequestParams(item, {
						key: k,
						replacer: replacer,
						levMax: levMax,
						levNow: lev
					})
				})
			}

			return paramStr.substr(1)
		},

		/**
		 * @desc 解析参数
		 * @param url <String> 链接地址
		 * */
		parseParams: function (url) {
			var query = {}
			var urlToParse = url || window.location.href;

			if (typeof urlToParse === 'string' && urlToParse.length) {
				urlToParse = urlToParse.indexOf('?') > -1 ? urlToParse.replace(/\S*\?/, '') : '';
				var params = urlToParse.split('&').filter(function (paramsPart) {
						return paramsPart !== '';
					});
				params.forEach(function (item) {
					var param = item.replace(/#\S+/g, '').split('=');
					query[decodeURIComponent(param[0])] = typeof param[1] === 'undefined' ? undefined : decodeURIComponent(param[1]) || '';
				})
			}

			return query;
		}
	};

	return url;
});