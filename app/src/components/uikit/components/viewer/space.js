/**
 * @author Rid King
 * @since 2017-05-21
 * @version 2.0.0
 * @description 视图域
 */

class Space {
    constructor (name) {
        this.name = name
        // 视图组
        this._viewers = []
        // 指令组
        this._directives = []
    }

    /**
     * @description 定义视图
    */
    viewer () {
        let viewer = null
        // 查找是否已经含有相同名称指令
        this._viewers.map((item) => {
            if (item.name === name) {
                return scope
            }
            return item
        })

        return scope
    }

    /**
     * @description 定义指令
    */
    directive (name, scope) {
        let directive = null
        // 查找是否已经含有相同名称指令
        this._directives.map((item) => {
            if (item.name === name) {
                return scope
            }
            return item
        })

        return scope
    }
}

export default Space