/**
 * @description 事件集合
*/

import browser from '../../../browser/2.0.0/js/browser'

// 移动端事件
const mode_mobile = {
  down : 'touchstart',
  move : 'touchmove',
  up : 'touchend',
  click : 'touchend',
  over : 'touchstart',
  out : 'touchend',
  cancel : 'touchcancel'
}

// PC端事件
const mode_pc = {
  down : 'mousedown',
  move : 'mousemove',
  up : 'mouseup',
  click : 'click',
  over : 'mouseover',
  out : 'mouseout',
  cancel : 'mouseout'
}

const eventMode = browser.versions.mobile ? mode_mobile : mode_pc

export default eventMode
export {
  mode_mobile, mode_pc
}