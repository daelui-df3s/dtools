/**
 * @description 数据解析服务
 * @author Rid King
 * @since 2018-07-06
 */

const parser = {
  /**
   * @function 数据对象转化为字符串
   * @param {Object} data // 数据对象
   * @return {String}
  */
  stringify (data) {
    let result = data
    // 空数据
    if (data === undefined || data === null) {
      result = ''
    }
    // 数据对象
    else if (objecter.isObject(data)) {
      result = JSON.stringify(data)
    }

    return result
  },

  /**
   * @function 数据解析
   * @param {String} data // 数据字符串
   * @param {String} type // 数据类型,允许的值:json/object/array/
   * @param {Boolean} isForce // 是否强制转换
   * @return {String/Object}
  */
  parse (data, type, isForce) {
    let result = data
    type = String(type).toLowerCase().trim()
    // 是否转换成功
    let isSuccess = true
    // json格式化
    if (
      type === 'json' ||
      type === 'object' ||
      type === 'array'
    ) {
      if (typeof data === 'string' && /^\s*\t*(\[|\{)/.test(data)) {
        try {
          result = JSON.parse(data)
        } catch (e) {
          isSuccess = false
          result = data
        }
      }

      // 强制转换
      if (isForce === true || isForce === 1) {
        if (type === 'json' || type === 'object') {
          result = typeof result === 'object' && result ? result : {}
        }
        else if (type === 'array') {
          result = Array.isArray(result) ? result : []
        }
      }
    }

    return result
  }
}

export default parser