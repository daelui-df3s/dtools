/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 指令集合
 */

import forIn from './for-in.js'
import each from './each.js'
import model from './model.js'
import show from './show.js'
import _if from './if.js'
import _else from './else.js'
import elseIf from './else-if.js'

export default [
    forIn,
    each,
    model,
    show,
    _if,
    _else,
    elseIf
]