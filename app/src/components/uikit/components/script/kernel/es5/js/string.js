/**
 * String ES5 extend
 * */

// 清除两边的空格
String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, '');
};