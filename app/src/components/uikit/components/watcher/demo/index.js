function Vue(options) {
    var vm = this;
    this._el = options.el;
    var data = this._data = options.data;

    var keys = Object.keys(data);
    var i = keys.length;
    while(i--) {
        proxy(this, keys[i]);
    }
    observe(data);

    var elem = document.getElementById(this._el);
    elem.innerHTML = vm.message;
    
    new Watcher(this, 'message', function() {
        elem.innerHTML = vm.message;
    });

}
function proxy(vm, key) {
    Object.defineProperty(vm, key, {
        configurable: true,
        enumerable: true,
        get: function proxyGetter() {
            return vm._data[key];
        },
        set: function proxySetter(val) {
            vm._data[key] = val;
        }
    });
}

var app = new Vue({
    el: 'app',
    data: {
        message: 'aaaaaaaaaaaaa'
    }
});