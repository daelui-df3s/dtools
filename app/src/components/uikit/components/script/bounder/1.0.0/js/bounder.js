/*
 * @desc 弹性器
 * */
define(['classer', 'jquery', 'objecter'], function(classer, $, objecter) {
	var bounder = {
		build : function(options){
			new Bounder(options);
		}
	};
	//弹性效果类
	var Bounder = classer.extend({
		init : function(options){
			options = objecter.extend({}, Bounder.defaults, options);
			this.setOptions(options);
			options = this.getOptions();

			//滚动条元素
			this.$scrollEl = $(this.getOptions('scrollEl'));
			this.$topBoundEl = $(this.getOptions('topBoundEl'));
			this.$bottomBoundEl = $(this.getOptions('bottomBoundEl'));

			//绑定事件
			this.setTopBound();
			this.setBottomBound();
		},

		//设置顶部弹性
		setTopBound : function(){
			var me = this, options = this.getOptions();
			this.$scrollEl.on('scroll', function(e){
				if(me.isScrollTop(this)){
					objecter.isFunction(options.onTopBounded) && options.onTopBounded.call(me, e);
					objecter.isFunction(options.onBounded) && options.onBounded.call(me, e);
				}
			});
		},

		//设置底部弹性
		setBottomBound : function(){
			var me = this, options = this.getOptions();
			this.$scrollEl.on('scroll', function(e){
				if(me.isScrollBottom(this), options.docEl){
					objecter.isFunction(options.onBottomBounded) && options.onBottomBounded.call(me, e);
					objecter.isFunction(options.onBounded) && options.onBounded.call(me, e);
				}
			});
		},

		//是否滚动到顶部
		isScrollTop : function(scrollEl, docEl){
			var b = false, options = this.getOptions();
			if(scrollEl == window || scrollEl == document || typeof scrollEl == 'undefined'){
				scrollEl = window;
				docEl = docEl || document;
			}else{
				scrollEl = scrollEl;
				docEl = docEl || scrollEl;
			}

			var scrollTop = $(scrollEl).scrollTop();
			if(scrollTop <= options.topMin){
				b = true;
			}

			return b;
		},

		//是否滚动到底部
		isScrollBottom : function(scrollEl, docEl){
			var b = false, options = this.getOptions();
			if(scrollEl == window || scrollEl == document || typeof scrollEl == 'undefined'){
				scrollEl = window;
				docEl = docEl || document;
			}else{
				scrollEl = scrollEl;
				docEl = docEl || scrollEl;
			}

			var scrollTop = $(scrollEl).scrollTop(),
				windowHeight = $(scrollEl).height(),
				scrollHeight = $(docEl).height();
			if(scrollTop + windowHeight + options.bottomMin >= scrollHeight){
				b = true;
			}

			return b;
		}
	});

	//默认配置
	Bounder.defaults = {
		//滚动条元素
		scrollEl : window,
		//文档元素
		docEl : document,

		//距离顶部最小值
		topMin : 0,
		//距离底部最小值
		bottomMin : 50,

		//顶部弹性元素
		topBoundEl : null,
		//顶部效果完成回调
		onTopBounded : null,

		//底部弹性元素
		bottomBoundEl : null,
		//底部效果完成回调
		onBottomBounded : null,

		//效果完成回调
		onBounded : null
	};

	return bounder;
});