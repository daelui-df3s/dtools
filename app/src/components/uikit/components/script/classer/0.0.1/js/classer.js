/*
 * @desc 基类
 * */
define(['objecter'], function(objecter) {
    var classer = {
        //继承
        extend : function(prop){
            return Classer.extend(prop);
        },

        //获取类
        getClass : function(){
            return Classer;
        }
    };

    //继承属性并生成新的类
    var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/,
        Classer = function(){};

    Classer.extend = extend;

    // 继承
	function extend (prop) {
		var father = this, _super = father.prototype;
		initializing = true;
		var prototype = new father();
		initializing = false;
		for (var key in prop) {
			prototype[key] = typeof prop[key] == "function" &&
			typeof _super[key] == "function" && fnTest.test(prop[key]) ?
				(function(key, fn, _super){
					return function() {
						var tmp = this._super;
						this._super = _super[key];
						var ret = fn.apply(this, arguments);
						this._super = tmp;
						return ret;
					};
				})(key, prop[key], _super) : prop[key];
		}
		function Class() {
			if ( !initializing && this.init ) this.init.apply(this, arguments);
		}
		Class.prototype = prototype;
		Class.prototype.constructor = Class;
		Class.prototype._superClass = _super;

		//设置配置值
		Class.prototype.setOptions = setOptions;

		//获取配置值
		Class.prototype.getOptions = getOptions;

		//复制属性到自身
		Class.prototype.copyProps = copyProps;

		//静态方法继承
		for(key in father){
			if(typeof father[key] == "function" && key != 'extend') Class[key] = father[key];
		}
		Class.extend = arguments.callee;
		return Class;
	}

    // 设置配置
	function setOptions (options) {
		this.options = objecter.extend({}, this.options, options);
	}

	// 获取配置
	function getOptions (options) {
		return typeof key == 'string' ? this.options[key] : this.options;
	}

	// 复制属性
	function copyProps (opts){
		for(var i in opts){
			this[i] = opts[i];
		}
		return this;
	}

    return classer;
});