/*
 * @desc 拖拽
 * */
define(['jquery', 'classer'], function($, classer) {
    var drager = {
        //添加拖拽
        build : function(options){
            return new Drager(options);
        },

        //获取类
        getClass : function(){
            return Drager;
        }
    };

    var Drager = classer.extend({
        /****属性****/
        //拖拽鼠标样式
        cursor: 'move',
        
        //边界限制
        boundaryLimit : false,
        
        //初始化类方法
        init : function(opts){
            var me = this;
            this.copyProps(opts);
            this.initDrag();
        },
        
        //初始化拖拽
        initDrag : function(){
            var me = this,
                dragStatus = false,
                oldStyle = null,
                oldPos = null;
            this.get$el().on({
	            mouseover : function(){
		            me.setCursor(me.cursor);
	            },
                mousedown : function(e){
                    //防止右键影响
                    if(e.button == 2) return !1;
                    try{
                        if(window.event){//IE
                            me.get$el().get(0).setCapture();
                        }else if(window.captureEvents){//FF
                            window.captureEvents(Event.MOUSEMOVE|Event.MOUSEUP|Event.STARTSELECT);
                        }
                    }catch(e){}
                    dragStatus = true;
	                me.setCursor(me.cursor);
                    oldPos = me.getMousePosition(e);
                    oldStyle = me.coordinate();
                    me.downCall(e);
                    return false;
                }
            });
            $(document).on({
                mousemove : function(e){
                    if(dragStatus){
                        me.dragCall(oldStyle, oldPos, me.getMousePosition(e), e);
                        return false;
                    }
                },
                mouseup : function(e){
                    if(dragStatus){
                        dragStatus = false;
                        me.setCursor();
                        me.upCall(oldStyle, oldPos, me.getMousePosition(e), e);
                        try{
                            if(window.event){
                                me.get$el().get(0).releaseCapture();
                            }else if(window.releaseEvents){
                                window.releaseEvents(Event.MOUSEMOVE|Event.MOUSEUP|Event.STARTSELECT);
                            }
                        }catch(e){}
                    }
                }
            });
        },
        
        //坐标获取与设置
        coordinate : function(coord){
            var $dragEl = this.get$dragEl();
            if(coord){
	            $dragEl.css(coord);
            }else return {
                left : parseInt($dragEl.css('left')) || 0,
                top : parseInt($dragEl.css('top')) || 0,
                width : parseInt($dragEl.css('width')) || 0,
                height : parseInt($dragEl.css('height')) || 0
            };
        },
        
        //按下时调用
        downCall : function(e){},
        
        //放开时调用
        upCall : function(e){},
        
        /*
         * @desc拖拽过程需要执行的操作
         * @param Object oldStyle
         * @param Object oldPos
         * @param Object newPos
         * @param Event e
         */
        dragCall : function(oldStyle, oldPos, newPos, e){
            var position = this.getNewPosition.apply(this, arguments);
            this.coordinate(position);
        },
        
        //获取鼠标位置坐标
        getMousePosition : function(e){
            var posx = 0, posy = 0;
            if (!e) var e = window.event;
            if (e.pageX || e.pageY) {
                posx = e.pageX;
                posy = e.pageY;
            }else if (e.clientX || e.clientY) {
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            return {x : posx, y : posy};
        },
        
        //获取新的坐标
        getNewPosition : function(oldStyle, oldPos, newPos, e){
            var $el = this.get$el(),
                spanX = newPos.x - oldPos.x,
                spanY = newPos.y - oldPos.y,
                style = this.coordinate(),
                elW = style.width,
                elH = style.height,
                newElemTop = oldStyle.top + spanY,
                newElemLeft = oldStyle.left + spanX, range;
            if(this.boundaryLimit){
                range = this.getRange();
                newElemTop = newElemTop > range.h - elH ? range.h - elH : newElemTop;
                newElemLeft = newElemLeft > range.w - elW ? range.w - elW : newElemLeft;
                newElemLeft < 0 && (newElemLeft = 0);
                newElemTop < 0 && (newElemTop = 0);
            }
            return {
                top : newElemTop,
                left : newElemLeft
            };
        },
        
        //获取拖拽范围
        getRange : function(){
            var $win = $(window);
            return {w : parseFloat($win.width()), h : parseFloat($win.height())};
        },
        
        setCursor : function(cursor){
            var $el = this.get$el();
            if(!this.selfCursor){
                this.selfCursor = $el.css('cursor') || 'default';
            }
            if(!cursor) cursor = this.selfCursor;
            $el.css('cursor', cursor);
        },
        
        //获取元素
        get$el : function(){
            return this.$el;
        },

	    //获取拖拽元素
	    get$dragEl : function(){
		    return this.$dragEl;
	    }
    });

    return drager;

});