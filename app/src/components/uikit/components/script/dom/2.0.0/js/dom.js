/*
 * Dom操作类
 * */
define(['classer'], function(classer){
	var Dom = classer.extend({
		/**
		 * @desc 查找元素
		 * @param <Dom/String> selector 选择器
		 * @param <Dom/String> parentNode 父元素
		 * @return <Array>
		 * */
		find : function(selector, parentNode){
			var results = [];
			if(selector && typeof selector == 'string' ){
				selector = selector.split(',');
				for(var i = 0; i < selector.length; i++){
					results = results.concat(_query(selector[i], parentNode));
				}
			}else if(objecter.isDOM(selector)){
				results.push(selector);
			}else if(objecter.isArray(selector)){
				for(var i = 0; i < selector.length; i++){
					results = results.concat(this.find(selector[i], parentNode));
				}
			}
			results = _unique(results);

			return results;
		},
		//查找父元素
		parent : function(selector, parentSelector, loop, bFisrt){
			var nodes = this.find(selector),
				parentNodes = [],
				parents = (objecter.isDOM(parentSelector) || typeof parentSelector == 'string' || objecter.isArray(parentSelector)) ? this.find(parentSelector) : null,
				results = [];

			_parent(nodes);
			if(loop === true){
				while(parentNodes.length > 0){
					_parent(parentNodes);
				}
			}

			function _parent(_nodes) {
				parentNodes = [];
				if(bFisrt === true && results.length > 0) return;
				for (var i = 0, b = false, node; i < _nodes.length; i++) {
					b = false;
					nodeParent = _nodes[i].parentNode;
					if(nodeParent != null) parentNodes.push(nodeParent);
					if (parents != null) {
						for (var j = 0; j < parents.length; j++) {
							if (parents[j] == nodeParent && nodeParent != null) {
								b = true;
								break;
							}
						}
					} else {
						if (nodeParent != null) b = true;
					}

					if (b) results.push(nodeParent);

					if(bFisrt === true && b) break;
				}
			}
			results = _unique(results);

			return results;
		},

		//查找多个层级元素
		parents : function(selector, parentSelector){
			var results = this.parent(selector, parentSelector, true);

			return results;
		},

		//查找符合条件父元素则停止
		closest : function(selector, parSelector){
			var nodes = this.find(selector), results = [];
			for(var i = 0; i < nodes.length; i++){
				results = results.concat(this.parent(nodes[i], parSelector, true, true));
			}
			results = _unique(results);

			return results;
		},

		//查找子元素
		children : function(selector, childSelector){
			var nodes = this.contents(selector), results = [];
			for(var i = 0; i < nodes.length; i++){
				if(objecter.isElementNode(nodes[i])) results.push(nodes[i]);
			}

			return results;
		},

		//查找子元素包括文本节点
		contents : function(selector, childSelector){
			var nodes = this.find(selector), results = [];
			for(var i = 0; i < nodes.length; i++){
				var childNodes = nodes[i].childNodes || [];
				for(var j = 0; j < childNodes.length; j++){
					results.push(childNodes[j]);
				}
			}
			results = _unique(results);

			return results;
		},

		//查找除自已外的同辈元素
		siblings : function(selector, sibSelector){

		},
		//查找之前元素
		prev : function(selector, prevSelector){

		},

		//查找之前所有同辈元素
		prevAll : function(selector, prevSelector){

		},

		//查找后面元素
		next : function(selector, nextSelector){

		},

		//查找后面所有同辈元素
		nextAll : function(selector, nextSelector){

		}
	});

	return Dom;
});