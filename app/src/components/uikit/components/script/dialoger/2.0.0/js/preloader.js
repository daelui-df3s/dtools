/**
 * @description 预加载器
*/

import objecter from '../../../objecter/2.0.0/js/objecter'
import {Dialoger} from './dialoger'

class PreloaderDialoger extends Dialoger {
  /**
   * @function 获取默认配置对象
   * @return {Object}
  */
  getDefaults () {
    return {
      //加载模板
      tpl: '<div class="modal modal-popup {{dialogerCls}}">' +
                '<div class="modal-head">' +
                  '<div class="modal-title">{{title}}</div>' +
                  '<div class="modal-tools">' +
                    '<a class="modal-close">×</a>' +
                  '</div>' +
                '</div>' +
                '<div class="modal-body">' +
                  '<div class="modal-text"><span class="indicator"></span></div>' +
                '</div>' +
              '</div>',
      title : '加载中',
      backdropHideAble : true
    }
  }
}

// 默认实例
const preloader = new PreloaderDialoger
export default PreloaderDialoger
export {
  preloader
}