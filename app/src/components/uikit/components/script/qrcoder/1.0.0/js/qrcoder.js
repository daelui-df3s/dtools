/**
 * @description 分享二编码生成
 */
define(['jquery', 'qrcode', 'jqqrcode'], function($){
	var qrcoder = {
		//显示
		show : function(options){
			options = $.extend({}, defaults, options);
            if(options.el){
                $el = $(options.el);
                if($el.length > 0){
                    $el.each(function(){
                        $(this).qrcode(options);
                    });
                    return;
                }
            }
			var id = 'qrcode_' + Math.ceil(10000 * Math.random()),
				$div;
			options.id = id;
			
			$div = $(this.getTpl(options)).appendTo('body');
			$div.find('.closer').click(function(){
				$div.remove();
			});

            //直接根据图片链接显示
            if(options.imgUrl){
                $('#' + id).append('<img src="' + options.imgUrl + '" width="' + options.width + '" height="' + options.height + '" />');
            }else{
                $('#' + id).qrcode(options);
                //判断是否以图片显示
                if(options.render == 'canvas'){
                    this.showImage($('#' + id).get(0));
                }
            }

			$div.show();
		},

        //以图片展示
        showImage : function(el){
            require(['imager'], function(imager){
                //获取网页中的canvas对象
                var canvas = el.getElementsByTagName('canvas')[0];
                //将转换后的img标签插入到html中
                var img = imager.convertCanvasToImage(canvas);
                $(el).after(img).hide();//imagQrDiv表示你要插入的容器id
                $(el).parent().find('img').on({
                    touchstart : function(){return false;}
                });
            });
        },

		//获取模板
		getTpl : function(options){
			var tpl = '<div style="position:fixed;left:50%;top:50%;margin:-140px 0 0 -118px;padding:10px 15px;border:1px solid #ccc;background:#fff;z-index:11001;">' +
						'<span class="closer" style="float:right;font-size:18px;color:#999;cursor:pointer">×</span>' +
						'<strong style="display:block;font-size:14px;color:#333;line-height:25px;">' + options.title + '</strong>' +
                        '<div style="margin:5px auto 0;width:' + options.width + 'px;height:' + options.height + 'px;">' +
						    '<div id="' + options.id + '" style="width:' + options.width + 'px;height:' + options.height + 'px;"></div>' +
                        '</div>' +
						'<div style="padding-top:10px;line-height:20px;color:#666;">' + options.desc + '</div>' +
					'</div>';
			return tpl;
		}
	},

	//默认配置
	defaults = {
		render : ('ontouchstart' in window) ? 'canvas' : 'div',
        title : '分享到微信朋友圈',
        desc : '打开微信，点击底部的“发现”，<br/>使用“扫一扫”即可将网页分享至朋友圈。',
        el : '',//展示二维码元素
		text	: "_blank",
		width		: 128,
		height		: 128,
		background      : "#ffffff",
		foreground      : "#000000",
		src : null//中部小图地址
	};

	return qrcoder;

});