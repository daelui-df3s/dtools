/**
 * 设备
 */
define(['jquery', 'classer', 'browser'], function ($, classer, browser) {
    var devicer = {}, deviceClass;

    //是否是微信
    if(browser.isWechat()){
        deviceClass = WechatBrowserDevicer;
    }
    else if(browser.isAndroidBrowser()){
        deviceClass = AndroidBrowerDevicer;
    }
    else if(browser.isIOSBrowser()){
        deviceClass = IOSBrowerDevicer;
    }
    else if(browser.isAndroidApp()){
        deviceClass = IOSAppDevicer;
    }
    else if(browser.isIOSApp()){
        deviceClass = IOSAppDevicer;
    }

    devicer = deviceClass && new divceClass();

    /*设备*/
    var Devicer = classer.extend({
        //获取配置
        getConfig : function(){
            return {};
        },

        //获取服务端地址
        getServerUrl : function(){
            return '';
        },

        //是否有网络
        hasNetwork:function(){},

        /**
         * 获取网络状态
         */
        getNetwork : function(){},

        //设备加载完成
        ready : function(fn){},

        //返回控制
        handlerBack : function(fn){},

        /**
         * 页面跳转
         * @param <Object> options
         * {
         *  url : String 跳转地址
         *  title : String //页面标题
         *  data : Object //传递的数据
         * }
         */
        redirect : function(options){
            location.href = options.url;
        },

        //改变导航栏状态
        routeRedirect : function(options){
            options = $.extend({
                title : $('title').text(),
                url : location.href
            }, options);
            if(history && history.replaceState){
                history.replaceState(null, options.title, options.url);
            }
        }
    }),

    /*应用浏览器*/
    AppDevicer = Devicer.extend({
        //是否有网络
        hasNetwork:function(){
            var network = this.getNetwork();
            return network && network != navigator.connection.NONE && network != navigator.connection.UNKNOWN;
        },

        /**
         * 获取网络状态
         * @returns {*} Connection[UNKNOWN、ETHERNET、WIFI、CELL_2G、CELL_3G、CELL_4G、CELL、NONE]
         */
        getNetwork : function(){
            if(navigator.network){
                return navigator.connection.type;
            }
            require(['debuger'], function(debuger){
                debuger.error('检测网络状态失败');
            });
            return null;
        },

        //设备加载完成
        ready : function(fn){
            document.addEventListener("deviceready", function(){
                typeof fn == 'function' && fn();
            }, false);
        },

        //返回控制
        handlerBack : function(fn){
            var me = this;
            if(fn == undefined){
                document.addEventListener("backbutton", function(){
                    if(me.clickback == 1){
                        require(['dialoger'], function(dialoger){
                            dialoger.confirm({
                                msg : "确定退出当前应用吗?",
                                callback : function(flag){
                                    if(flag) navigator.app.exitApp();
                                }
                            });
                            me.clickback = 0;
                        });
                    }else{
                        //三秒钟内连续点击返回键，也是退出。
                        me.clickback = 1;
                        setTimeout(function(){
                            me.clickback = 0;
                        }, 3000);
                    }
                }, false);
            }else{
                document.addEventListener("backbutton", function(){
                    fn();
                }, false);
            }
        }
    }),

    /*安卓应用*/
    AndroidAppDevicer = AppDevicer.extend({

    }),

    /*IOS应用*/
    IOSAppDevicer = AppDevicer.extend({

    }),

    /*普通浏览器*/
    BrowerDevicer = Device.extend({
        //设备加载完成
        ready : function(fn){
            $(function(){
                typeof fn == 'function' && fn();
            });
        },

        //返回控制
        handlerBack : function(fn){
            var me = this;
            window.addEventListener('beforeunload', function(){
                typeof fn == 'function' && (return fn());
            });
        }
    }),

    /*应用浏览器*/
    AndroidBrowerDevicer = BrowerDevicer.extend({

    }),

    /*应用浏览器*/
    IOSBrowerDevicer = BrowerDevicer.extend({

    }),

    /*微信浏览器*/
    WechatBrowserDevicer = BrowerDevicer.extend({
        //设备加载完成
        ready : function(fn){
            wx.ready(function(){
                typeof fn == 'function' && fn();
            });
        }
    });

    return devicer;

});