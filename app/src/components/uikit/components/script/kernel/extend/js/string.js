/**
 * String extend
 * */

//格式化字符串
String.prototype.format = function() {
	var args = arguments;
	return this.replace(/\{(\d{1,2})\}/g, function(old, index) {
		var value = args[index]
		return value === undefined ? "" : value;
	});
};

// 合并多个空白为一个空白
String.prototype.ResetBlank = function() {
	var regEx = /\s+/g;
	return this.replace(regEx, ' ');
};

// 保留数字
String.prototype.GetNum = function() {
	var regEx = /[^\d]/g;
	return this.replace(regEx, '');
};

// 保留中文
String.prototype.GetCN = function() {
	var regEx = /[^\u4e00-\u9fa5\uf900-\ufa2d]/g;
	return this.replace(regEx, '');
};

// String转化为Number
String.prototype.ToInt = function() {
	return isNaN(parseInt(this)) ? this.toString() : parseInt(this);
};

// 得到字节长度
String.prototype.GetLen = function() {
	var regEx = /^[\u4e00-\u9fa5\uf900-\ufa2d]+$/;
	if (regEx.test(this)) {
		return this.length * 2;
	} else {
		var oMatches = this.match(/[\x00-\xff]/g);
		var oLength = this.length * 2 - oMatches.length;
		return oLength;
	}
};

// 获取文件全名
String.prototype.GetFileName = function() {
	var regEx = /^.*\/([^\/\?]*).*$/;
	return this.replace(regEx, '$1');
};

// 获取文件扩展名
String.prototype.GetExtensionName = function() {
	var regEx = /^.*\/[^\/]*(\.[^\.\?]*).*$/;
	return this.replace(regEx, '$1');
};

//替换所有
String.prototype.replaceAll = function(reallyDo, replaceWith, ignoreCase) {
	if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
		return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi" : "g")), replaceWith);
	} else {
		return this.replace(reallyDo, replaceWith);
	}
};

String.prototype.toCase = function () {
	var tmp = "";
	for(var i0;i<this.length;i++)
	{
		if(this.charCodeAt(i)>0&&this.charCodeAt(i)<255)
		{
			tmp + String.fromCharCode(this.charCodeAt(i)+65248);
		}
		else
		{
			tmp + String.fromCharCode(this.charCodeAt(i));
		}
	}
	return tmp
}
/*
 //对字符串进行Html编码
 */
String.prototype.toHtmlEncode = function () {
	var str = this;
	strstr.replace(/&/g,"&");
	strstr.replace(/</g,"<");
	strstr.replace(/>/g,">");
	strstr.replace(/\'/g,"'");
	strstr.replace(/\"/g,"\"");
	strstr.replace(/\n/g,"<br>");
	strstr.replace(/\ /g," ");
	strstr.replace(/\t/g,"    ");
	return str;
}
/*
 //转换成日期
 */
String.prototype.toDate = function () {
	try
	{
		return new Date(this.replace(/-/g, "\/"));
	}
	catch(e)
	{
		return null;
	}
}

String.prototype.isIP = function () {
	var reSpaceCheck  = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;
	if (reSpaceCheck.test(this))
	{
		this.match(reSpaceCheck);
		if (RegExp.$1 < 255 && RegExp.$1 > 0
			&& RegExp.$2 < 255 && RegExp.$2 > 0
			&& RegExp.$3 < 255 && RegExp.$3 > 0
			&& RegExp.$4 < 255 && RegExp.$4 > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}

}

/*
 //是否是正确的长日期
 */
String.prototype.isLongDate = function () {
	var r  = this.replace(/(^\s*)|(\s*$)/g, "").match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/);
	if(rnull)
	{
		return false;
	}
	var d = new Date(r[1], r[3]-1,r[4],r[5],r[6],r[7]);
	return (d.getFullYear() == r[1] && (d.getMonth()+1) == r[3] && d.getDate() == r[4] && d.getHours() == r[5] && d.getMinutes() == r[6] && d.getSeconds() == r[7]);
}
/*
 //是否是正确的短日期
 */
String.prototype.isShortDate = function () {
	var r = this.replace(/(^\s*)|(\s*$)/g, "").match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
	if(rnull)
	{
		return false;
	}
	var d = new Date(r[1], r[3]-1, r[4]);
	return (d.getFullYear() == r[1] && (d.getMonth()+1) == r[3] && d.getDate() == r[4]);
}
/*
 //是否是正确的日期
 */
String.prototype.isDate = function () {
	return this.isLongDate()||this.isShortDate();
}
/*
 //是否是手机
 */
String.prototype.isMobile = function () {
	return /^0{0,1}13[0-9]{9}$/.test(this);
}
/*
 //是否是邮件
 */
String.prototype.isEmail = function () {
	return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(this);
}
/*
 //是否是邮编(中国)
 */
String.prototype.isZipCode = function () {
	return /^[\\d]{6}$/.test(this);
}
/*
 //是否是有汉字
 */
String.prototype.existChinese = function () {
	//[\u4E00-\u9FA5]為漢字﹐[\uFE30-\uFFA0]為全角符號
	return /^[\x00-\xff]*$/.test(this);
}
/*
 //是否是合法的文件名/目录名
 */
String.prototype.isFileName = function () {
	return !/[\\\/\*\?\|:"<>]/g.test(this);
}
/*
 //是否是有效链接
 */
String.prototype.isUrl = function () {
	return /^http[s]?:\/\/([\w-]+\.)+[\w-]+([\w-./?%&]*)?$/i.test(this);
}

/*
 //是否是有效的身份证(中国)
 */
String.prototype.isIDCard = function () {
	var iSum0;
	var info = "";
	var sId = this;
	var aCity = {11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"};
	if (!/^\d{17}(\d|x)$/i.test(sId)) {
		return false;
	}
	sIdsId.replace(/x$/i,"a");
	//非法地区
	if (aCity[parseInt(sId.substr(0,2))] == null) {
		return false;
	}
	var sBirthday = sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2));
	var dnew = Date(sBirthday.replace(/-/g,"/"))

	//非法生日
	if(sBirthday != (d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate()))
	{
		return false;
	}
	for (var i = 17; i > 0; i--) {
		iSum + (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11);
	}
	if (iSum%11 !=1 ) {
		return false;
	}
	return true;
}
/*
 //是否是有效的电话号码(中国)
 */
String.prototype.isPhoneCall = function () {
	return /(^[0-9]{3,4}\-[0-9]{3,8}$)|(^[0-9]{3,8}$)|(^[0−9]3,4[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)/.test(this);
}

/*
 //是否是数字
 */
String.prototype.isNumeric = function (flag) {
	//验证是否是数字
	if (isNaN(this)) {
		return false;
	}
	switch (flag) {
		case null:        //数字
		case "":
			return true;
		case "+":        //正数
			return                /(^\+?|^\d?)\d*\.?\d+$/.test(this);
		case "-":        //负数
			return                /^-\d*\.?\d+$/.test(this);
		case "i":        //整数
			return                /(^-?|^\+?|\d)\d+$/.test(this);
		case "+i":        //正整数
			return                /(^\d+$)|(^\+?\d+$)/.test(this);
		case "-i":        //负整数
			return                /^[-]\d+$/.test(this);
		case "f":        //浮点数
			return                /(^-?|^\+?|^\d?)\d*\.\d+$/.test(this);
		case "+f":        //正浮点数
			return                /(^\+?|^\d?)\d*\.\d+$/.test(this);
		case "-f":        //负浮点数
			return                /^[-]\d*\.\d$/.test(this);
		default:        //缺省
			return true;
	}
}
/*
 //是否是颜色(#FFFFFF形式)
 */
String.prototype.IsColor = function () {
	var temp = this;
	if (temp === "") return true;
	if (temp.length != 7) return false;
	return (temp.search(/\#[a-fA-F0-9]{6}/) != -1);
}