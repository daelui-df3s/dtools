/*
 * 工具
 * */
define(function(){
	var util = {
		//模板控制
		tpl : function(tpl, data){
			tpl = objecter.isString(tpl) ? tpl : '';
			data = objecter.isPlainObject(data) ? data : {};
			var reg = /\{\{?[^\}]*\}\}?/g;
			tpl = tpl.replace(reg, function(str){
				return render(str, data);
			});

			//编译器
			function render(str, data){
				str = str.replace(/{|\}/g, '').replace(/^\s+|\s+$/g, '');
				var arr = str.split(''), fb = true, n = 0, sb = false;
				for(var i = 0; i < arr.length; i++){
					if(/[\'\"]/.test(arr[i])){
						fb = fb ? false : true;
					}
					if(/[a-zA-Z_\[\]\.]/.test(arr[i])){
						n += 1;
					}else n = 0;
					if(fb && n == 1) arr[i] = '$data.' + arr[i];
				}
				str = arr.join('');//str = arr.join('').replace(/'/g, "\\'").replace(/"/g, '\\"');
				var code = 'var _tpl="";_tpl+=(' + str + ');return new String(_tpl);';
				var R = new Function('$data', code);
				str = new R(data) + '';

				return str;
			}

			return tpl;
		}
	};

	return util;
});