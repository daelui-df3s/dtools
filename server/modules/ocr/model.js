/**
 * @description 执行器-模型
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')

class Model extends ox.Model {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 表配置
    let db = this.route.scope.CONFIG.db
    // 表名
    this.table = 'actuator'
    // 指定DB类
    let DBClass = ox.dbFactory.getDBClass(db.type)
    // 数据库
    this.$db = new DBClass(Object.assign({table: this.table}, db))
  }
}

module.exports = Model