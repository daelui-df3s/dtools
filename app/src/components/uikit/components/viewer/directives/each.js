/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 可遍历数组
 * demo: <div dog-each="item,i in list">{{item.text}}</div>
 */

import objecter from '../../../../../../../components/script/objecter/2.0.0/js/objecter.js'
import {DIRECTIVE_MODE} from './directive.js'
import {resolveArgs} from './for-in.js'

export default {
    name: 'dog-each',
    scope: {
        /**
         * @property 指令编译模式
         * @description 分为编译指令与运行时指令
         * */
        mode: DIRECTIVE_MODE.COMPILE,
        /**
         * @property 指令排序
         * @description 值越小优先级越高
         * */
        order: 1.01,
        /**
         * @function 解析参数
         * @param value {String} // 指令值
         * @param code {String} // 编译码
        */
        resolve (value, code) {
            let args = resolveArgs(value)
            code = '_d("dog-each",' + args.list + ',function(' + args.attr + ',' + args.key + '){return ' + code + '})'

            return code
        },
        /**
         * @function 编译方法
         * @param list {Array/Object/String/Int} // 遍历数组
         * @param fn {Function} // 处理方法
        */
        compile (list, fn) {
            let code = []
            // 数组形式
            if (objecter.isArray(list)) {
                list.forEach(function (item, i) {
                    code.push(fn(item, i))
                })
            }

           return code
       }
   }
}