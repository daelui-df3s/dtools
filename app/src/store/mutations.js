import Vue from 'vue'
import * as types from './mutation-types'

export default {
  /*
   全局
   */
  // 语言设置
  [types.UPDATE_LANG] (state, lang) {
    Vue.set(state, 'lang', lang)
  },
  // 显示加载状态
  [types.SHOWPRELOADER] (state, b) {
    Vue.set(state, 'showPreloader', b)
  },

  /*
   模块
   */
  // 更新首页文章列表
  [types.UPDATE_INDEXARTICLElIST] (state, {data}) {
    Vue.set(state, 'indexArticleList', data)
  },

  // 更新管理员或登录用户
  [types.UPDATE_MASTER] (state, { data }) {
    Vue.set(state, 'master', data)
  },

  // 更新文章列表
  [types.UPDATE_ARTICLElIST] (state, {data}) {
    Vue.set(state, 'articleList', data)
  },
  // 更新文章
  [types.UPDATE_ARTICLE] (state, {data}) {
    Vue.set(state, 'article', data)
  }
  /*
  ,
  [types.INIT_CONTACTS] (state, { contacts }) {
    Vue.set(state, 'contacts', contacts)
  },
  [types.INIT_TIMETIME] (state, { timeline }) {
    Vue.set(state, 'timeline', timeline)
  },
  [types.UPDATE_TIMETIME] (state, { mid, type }) {
    let item = find(state.timeline, p => p.id === mid)
    let update = {}
    switch (type) {
      case 'like':
        update.like_count = item.like_count + 1
        update.liked = true
        break
      case 'unlike':
        update.like_count = item.like_count - 1
        update.liked = false
        break
    }
    // Yes, Object.assign can update state and UI component at same time.
    item = Object.assign(item, update)
  }
  */
}
