/**
 * 摄像头
 */
define(['dialoger'], function (dialoger) {
	var cameraer = {
		//拍图片
		captureImage : function (options){
			options = options || {};
			var me = this,
				cmr = plus.camera.getCamera(),
				res = cmr.supportedImageResolutions[0],
				fmt = cmr.supportedImageFormats[0];

			cmr.captureImage( function( path ){
					me.createImage({
						path : path,
						success : options.success
					});
				},
				function( error ) {
					dialoger.error( error.message );
				},
				{resolution : res, format : fmt}
			);
		},

		//创建图片
		createImage : function (options){
			options = options || {};
			plus.io.resolveLocalFileSystemURL(options.path,function(entry){
				var img = new Image();
				img.setAttribute("src", entry.toLocalURL());
				img.setAttribute("class", 'img');
				typeof options.success === 'function' && options.success(img);
				/*entry.file( function(file){
				 var reader = new plus.io.FileReader();console.log(2);
				 console.log(3);
				 reader.onload = function(e){//加载ok时触发的事件
				 var img = new Image();console.log('ok');
				 img.setAttribute("src", e.target.result);console.log(e.target.result);
				 img.setAttribute("class", 'img');
				 typeof callback === 'function' && callback(img);
				 };
				 reader.onerror = function(e){console.log(arguments);
				 console.log('error');
				 };
				 reader.readAsDataURL(file);
				 console.log(reader);
				 });*/
			});//, function(e){},{index:1,filename:"_doc/camera/"}
		}
	}

	return cameraer;
});