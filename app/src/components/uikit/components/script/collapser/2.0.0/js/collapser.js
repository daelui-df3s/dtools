/**
 * @desc 折叠器
 * */
define(['jquery', 'classer', 'ajaxer', 'eventer', 'objecter'], function($, classer, ajaxer, eventer, objecter){
	var collapser = {
			build : function(options){
				var me = this;
				$(options.el).each(function(){
					var opts = $.extend({}, options, {el : this});
					me._build(opts);
				});
			},

			_build : function(options) {
				return new Collapser(options);
			}
		},

		Collapser = classer.extend({
			init : function(options){
				this.setOptions(objecter.extend({}, Collapser.defaults, options));
				options = this.getOptions();

				var me = this,
					$this = $(options.el),
					collapseCls = options.collapseCls,
					activeCls = options.activeCls,
					$collapseList = $this.find('.' + collapseCls),
					$activeCollapse;

				if(options.isChildren){
					$collapseList = $this.children('.' + collapseCls);
				}

				$collapseList.each(function(){
					var $this = $(this);
					$this.on(eventer.events.down, function(){
						me.active($(this));
					});
					if($this.hasClass(activeCls)){
						$activeCollapse = $this;
					}
				});

				this.$collapseList = $collapseList;

				//默认显示
				if(options.autoActiveAble){
					var $activeCollapse = $activeCollapse || 0;
					this.active($activeCollapse, true);
				}
			},

			//选中
			active : function($activeCollapse, forceAble, $collapseList){
				var options = this.getOptions(),
					activeCls = options.activeCls,
					isActive = false;
				$collapseList = $collapseList || this.$collapseList;

				//允许索引
				if(typeof $activeCollapse === 'number'){
					$activeCollapse = $collapseList.eq($activeCollapse);
				}
				isActive = $activeCollapse.hasClass(activeCls);

				//全部取消选中
				if(!options.multipleAble){
					$collapseList.each(function(){
						var href = $(this).removeClass(activeCls).attr(options.href);

						//效果css
						var $panel = $(href);
						$panel.removeClass(activeCls).removeClass(options.animateInCls);
					});
				}

				//选中状态下则取消选中
				if(!isActive || forceAble){
					//选中
					$activeCollapse.addClass(activeCls);
					var $activePanel = $($activeCollapse.attr(options.href)).addClass(activeCls),
						collapseUrl = $activeCollapse.attr(options.url);

					//效果css
					setTimeout(function(){
						$activePanel.addClass(options.animateInCls);
					}, 20);

					//远程加载
					if(collapseUrl){
						ajaxer.ajax({
							url : collapseUrl,
							dataType : 'text',
							success : function(html){
								$activePanel.html(html);
							}
						});
						if(options.cacheAble){
							$activeCollapse.removeAttr(options.url);
						}
					}
				}

				if(typeof options.onActive === 'function'){
					options.onActive.call(this, $activeCollapse, $activePanel);
				}
			}
		});

	Collapser.defaults = {
		collapseCls : 'collapse',
		activeCls : 'active',
		animateInCls : 'collapse-in',
		href : 'data-collapse-href',
		url : 'data-collapse-url',
		//是否是子元素
		isChildren : false,
		//是否可以同时选中
		multipleAble : false,
		//自动选中
		autoActiveAble : true,
		//链接地址是否缓存
		cacheAble : true,
		//选中事件
		onActive : null
	};

	return collapser;

});