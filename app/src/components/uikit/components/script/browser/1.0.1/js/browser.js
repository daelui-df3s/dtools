/**
 * 浏览器
 */
define(function () {
	/*
	 受支持的PC端浏览器列表：
	 Edge
	 IE
	 Chrome
	 Firefox
	 Opera
	 Safari
	 QQ浏览器
	 360系列浏览器
	 使用IE内核的非主流浏览器
	 使用Chrome内核的非主流浏览器
	 使用混合内核的非主流浏览器
	* */
	var sys = {};
	var ua = navigator.userAgent.toLowerCase();
	var s;
	(s = ua.match(/edge\/([\d.]+)/)) ? sys.edge = s[1] :
		(s = ua.match(/rv:([\d.]+)\) like gecko/)) ? sys.ie = s[1] :
			(s = ua.match(/msie ([\d.]+)/)) ? sys.ie = s[1] :
				(s = ua.match(/firefox\/([\d.]+)/)) ? sys.firefox = s[1] :
					(s = ua.match(/chrome\/([\d.]+)/)) ? sys.chrome = s[1] :
						(s = ua.match(/opera.([\d.]+)/)) ? sys.opera = s[1] :
							(s = ua.match(/version\/([\d.]+).*safari/)) ? sys.safari = s[1] : 0;

	if (sys.edge) return { broswer : "Edge", version : sys.edge };
	if (sys.ie) return { broswer : "IE", version : sys.ie };
	if (sys.firefox) return { broswer : "Firefox", version : sys.firefox };
	if (sys.chrome) return { broswer : "Chrome", version : sys.chrome };
	if (sys.opera) return { broswer : "Opera", version : sys.opera };
	if (sys.safari) return { broswer : "Safari", version : sys.safari };

	return { broswer : "", version : "0" };
})