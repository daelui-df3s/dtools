/**
 * @description 回车回调
*/

import Vue from 'vue'

// 回车绑定指令
Vue.directive('enter', {
  inserted (el, binding) {
    el.addEventListener('keyup', function (e) {
      if (e.keyCode === 13 && typeof binding.value === 'function') {
        binding.value()
      }
    })
  }
})