/**
 * @description 一般弹出
*/

import objecter from '../../../objecter/2.0.0/js/objecter'
import {Dialoger} from './dialoger/2.0.0/js/dialoger'

class PrettyDialoger extends Dialoger {
  // 警告框
  alert (options) {
    
  }

  // 选择框
  confirm (options) {
    
  }

  // 输入框
  prompt (options) {
    
  }

  // 普通信息展示
  info (options) {
    
  }

  // 成功信息
  success (options) {
    
  }

  // 警告信息展示
  warning (options) {
    
  }

  // 错误信息展示
  error (options) {
    
  }
}

// 默认实例
const pretty = new PrettyDialoger
export default pretty
export {
  PrettyDialoger
}