/**
 * @description 数据服务
 * @author Rid King
 * @since 2018-03-10
*/

import RequesterService from '../../../requester/2.0.0/js/requester'

export default {
  /**
   * @function 获取ajax请求服务对象
   * @param {Object} options
   * @return {Service}
  */
  getAjaxRequester (options) {

  },

  /**
   * @function 获取Fetch请求服务对象
   * @param {Object} options
   * @return {Service}
  */
  getFetchRequester (options) {

  },

  /**
   * @function 获取Form服务对象
   * @param {Object} options
   * @return {Service}
  */
  getFormRequester (options) {

  }
}