/**
 * @description 执行器-控制器
 * @since 2019-11-12
 * @author Rid King
*/

const fs = require('fs')
const oxjs = require('@daelui/oxjs')
const { singleThreadOCR } = require('../../components/ocr/ocr.js')

class Controller extends oxjs.Controller {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
  }

  /**
   * @function 执行
  */
  resolveText (action, extender) {
    let config = this.route.scope.CONFIG
    // 解析数据
    return this.solveAction(action, extender).then(async action => {
      return new Promise((resolve, reject) => {
        let params = action.params || {}
        let base64 = params.base64
        let reg = /data:\w+\/(\w+);base64,/i
        let ext = (reg.exec(base64) || [])[1] || 'png'
        let fileName = config.constant.ocrImagePath + '/' + Date.now() + '.' + ext
        this.base64ToFile(base64, fileName).then(async () => {
          try {
            let data = await singleThreadOCR({
              workerPath: config.constant.tesseractWorkerPath,
              targetPhotoDir: fileName,
              langPath: config.constant.tessdata,
              languages: [
                'chi_sim',
                'chi_sim_vert',
                'chi_tra',
                'chi_tra_vert',
                'eng'
              ]
            })
            resolve({
              status: 200,
              success: 1,
              data: {
                lines: data.data.lines,
                text: data.data.text
              },
              msg: '解析成功'
            })
            fs.rmSync(fileName)
          } catch (e) {
            console.log(e)
            reject({ status: 200, success: 0, msg: '解析失败' })
            fs.rmSync(fileName)
          }
        }).catch((e) => {
          console.log(e)
          reject({
            status: 200,
            success: 0,
            msg: '解析失败'
          })
          fs.rmSync(fileName)
        })
      })
    })
  }

  // base64转图片
  base64ToFile (base64, fileName) {
    return new Promise((resolve, reject) => {
      base64 = base64.replace(/^data:\w+\/\w+;base64,/, '')
      // 将Base64字符串转换为Buffer
      const imageBuffer = Buffer.from(base64, 'base64')
      // 将Buffer写入文件系统，作为图片保存
      fs.writeFile(fileName, imageBuffer, { encoding: 'base64' }, (err) => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }
}

module.exports = Controller