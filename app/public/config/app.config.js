/**
 * @description 应用配置
*/

/* 运行模式配置 */
(function () {
  const origin = location.origin
  const domain = /file:/.test(location.protocol) ? '//127.0.0.1' : location.origin

  // 开发环境配置
  const dev = {
    mode: 'dev',
    title: '开发模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: '//127.0.0.1:3501'},
      {rule: 'server', url: '//127.0.0.1:3501'},
      {rule: 'proxy', url: '//127.0.0.1:3501/dtools/proxy'}
    ],
    // 环境域名
    domain,
    // 模块加载器
    loader: {
      path: domain + '/pool/prod/packages/@daelui/pigjs/latest/dist/system.min.js',
      registry: domain + '/pool/prod/packages.json',
      rootUrl: domain + '/pool/prod/packages'
    }
  }

  // 生产环境配置
  const prod = {
    mode: 'prod',
    title: '生产模式',
    // 服务匹配规则列表
    rules: [
      {rule: 'host', url: origin},
      {rule: 'server', url: origin},
      {rule: 'proxy', url: origin + '/dtools/proxy'}
    ],
    // 环境域名
    domain,
    // 模块加载器
    loader: {
      path: domain + '/pool/prod/packages/@daelui/pigjs/latest/dist/system.min.js',
      registry: domain + '/pool/prod/packages.json',
      rootUrl: domain + '/pool/prod/packages'
    }
  }

  /* 应用配置 */
  const application = {
    dev, prod,
    // 运行模式配置
    mode: {dev, prod}.prod
  }

  window.application = window.application || {}
  window.application.dtools = application
})();