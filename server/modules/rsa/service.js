/**
 * @description 执行器-服务
 * @since 2019-11-12
 * @author Rid King
*/

const ox = require('@daelui/oxjs')
const Model = require('./model.js')

class Service extends ox.Service {
  /**
   * @function 构造方法
  */
  constructor (args) {
    super(args)
    // 模型
    this.$model = new Model(args)
  }
}

module.exports = Service