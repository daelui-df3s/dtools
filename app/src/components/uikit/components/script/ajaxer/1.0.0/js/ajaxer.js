/*
 * @desc ajax代理
 * */
define(['jquery'], function($) {
    var ajaxer = {
        ajax : function(options){
            this.send(options, 'ajax');
        },

        "get" : function(options){
            this.send(options, 'get');
        },

        post : function(options){
            this.send(options, 'post');
        },

        send : function(opts, type){
            var options = this.requestHandle(opts);
            if(options){
                options.type = type == 'get' ? 'get' : 'post';
                $.ajax(options);
            }
        },

        //请求操作处理
        requestHandle : function(opts){
            var me = this, options = $.extend(true, {}, opts);
            options.data = $.extend(options.data, opts.params);
            options = $.extend({}, this.defaults, options);
            var showLoading = options.showLoading;
            if(options.syncs){
                if(this.loading) return false;
                this.startHandle();
                options = fit(options);
                return options;
            }
            else{
                this.loadingSwitch({status : true, showLoading : showLoading});
                this.startHandle();
                options = fit(options);
                return options;
            }

            function fit(options){
                var b = false,
                    success = options.success || function(){},
                    error = options.error || function(){},
                    complete = options.complete || function(){};
                options.complete = function(){
                    if(!b){
                        b = true;
                        me.loadingSwitch({status : false, showLoading : showLoading});
                        me.endHandle.apply(me, arguments);
                    }
                    complete.apply(this, arguments);
                };
                options.success = function(){
                    if(!b) {
                        b = true;
                        me.loadingSwitch({status: false, showLoading : showLoading});
                        me.endHandle.apply(me, arguments);
                    }
                    success.apply(me, arguments);
                };
                options.error = function(){
                    if(!b) {
                        b = true;
                        me.loadingSwitch({status: false, showLoading : options.showLoading});
                        me.endHandle.apply(me, arguments);
                    }
	                options.showError && me.errorHandle.apply(me, arguments);
                    error.apply(this, arguments);
                };
                return options;
            }
        },

        //传输状态开关
        loadingSwitch : function(options) {
            var loading = options.status == true ? true : false,
                loadingQueue = this.loadingQueue || 0;
            if (loading) loadingQueue += 1;
            else loadingQueue -= 1;

            if (options.showLoading){
                if (!this.$loadingEl) {
                    this.$loadingEl = $('<div class="indicater indicater-data">' + (options.loadingTitle || '正在加载...') + '</div>').appendTo('body');
                }
                if (loadingQueue) {
                    this.$loadingEl.show();
                } else {
                    this.$loadingEl.fadeOut(200);
                }
            }

            this.loadingQueue = loadingQueue;
        },

        //开始处理
        startHandle : function(){

        },

        //结束处理
        endHandle : function(){

        },

	    //显示加载
	    showLoading : function(id){
		    id = id || 'loading_' + String(Math.random()).replace('.', '');
		    var $el = $('<div id="' + id + '" class="indicator indicator-data">' + ('正在加载...') + '</div>').appendTo('body');
		    $el.show();
		    return id;
	    },

	    //隐藏加载
	    hideLoading : function(id){
		    if(id){
			    var $el = $('#' + id);
			    if($el.length > 0){
				    $el.fadeOut(200);
				    setTimeout(function(){
					    $el.remove();
				    }, 400);
			    }
		    }
	    },

        //错误处理
        errorHandle : function(){
            require(['dialoger'], function(dialoger){
                dialoger.error({
                    msg : '访问服务器失败'
                });
            });
        },

        //默认配置
        defaults : {
            type : 'post',
            dataType : 'json',
            showLoading : true,
            //是否同时只开启一个请求
            syncs : false
        },

        //从响应中获取data
        getPureData : function(res){
            return $.isArray(res) ? res : (res = res || {},!$.isEmptyObject(res.data) ? res.data : res);
        }

    };

    return ajaxer;
});
