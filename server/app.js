/**
 * @description 服务入口
 * @since 2019-11-12
 * @author Rid King
 */

// 系统配置
const config = require('./config/app.config')
// 启动文件
const { boot } = require('./main')

// 执行主程序
const instance = boot(config)

module.exports = instance
