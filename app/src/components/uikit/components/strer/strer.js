/**
 * @description 字符组件
 * @author Rid King
 * @since 2018-02-04
*/

export default {
  /**
   * @function 复制字符
   * @param {String} str 复制的字符串
  */
  copy (str) {
    var el = document.createElement('textarea')
    el.value = str
    el.setAttribute('readonly', '')
    el.style.position = 'absolute'
    el.style.left = '-9999px'
    document.body.appendChild(el)
    el.select()
    document.execCommand('copy')
    document.body.removeChild(el)
  }
}