/*
 * @desc 工具
 * */
define('tool', ['jquery'], function($) {
    /*基础工具JS*/
    var tool = {
        /*
         * 判断值是否在数组中
         * needle    值
         * haystack  数组
         */
        inArray : function(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        },

        //根据返回的数据获取数据行数组
        getRowsArray : function(options){
            var defaults = this.getDefaults();
            options = $.extend({}, defaults, options);
            var data = options.data || [];
            if($.isPlainObject(data) && data.hasOwnProperty(options.totalKey) && data.hasOwnProperty(options.rowsKey)){
                data = data[options.rowsKey];
            }
            return data;
        },

        //获取方法名之后的参数
        getMethodParams : function(arguments){
            var arr = [];
            for(i = 1; i < arguments.length; i++){
                arr.push(arguments[i]);
            }
            return arr;
        },

        //获取随机数
        rand : function(length){
            length = isNaN(length) || length < 2 ? 4 : length;
            var str = '123456789';
            str = str.split('');
            for(var i = 0, s = ''; i < length; i++){
                if(i === 1) str.push('0');
                s += str[Math.floor(Math.random() * str.length)];
            }
            return parseInt(s);
        },

        /*页面跳转*/
        jumpTo : function(url, win) {
            win = this.getWin(win);
            url = url || win.location.href;
            if(url == win.location.href) win.location.reload();
            else win.location.href = url;
        },

        /*页面返回*/
        back : function(win) {
            win = this.getWin(win);
            win.history.back();
        },

        /*打开新页面*/
        openTo : function(url, win) {
            win = this.getWin(win);
            win.open(url);
        },

        /*对象长度*/
        lenObj : function(obj) {
            var i = 0;
            for(var j in obj) {i++;}
            return i;
        },

        //获取window
        getWin : function(win){
            win = win || window;
            return win;
        },

        /**
         * 将form表单元素的值序列化成对象
         * @returns object
         */
        serializeObject : function($form) {
            var o = {};
            $.each($form.serializeArray(), function(index) {
                if (o[this['name']]) {
                    o[this['name']] = o[this['name']] + "," + this['value'];
                } else {
                    var $element = $("#"+this['name']);
                    /*ewbeditor编辑器取值*/
                    if($element.is('textarea')){
                        try {
                            /*尝试取所关联的编辑器的值*/
                            var html = document.getElementById("eWebEditor_"+this['name']).contentWindow.getHTML();
                            o[this['name']] = html;
                        } catch(e) {
                            /*如果没有关联的编辑器那么直接取textarea的值*/
                            o[this['name']] = $element.val();
                        }
                    } else {
                        o[this['name']] = this['value'];
                    }
                }
            });
            return o;
        },

        /* 设置表单的值 */
        setValue : function(name, value){
            var first = name.substr(0,1), input, i = 0, val;
            if(value === "") return;
            if("#" === first || "." === first){
                input = $(name);
            } else {
                input = $("[name='" + name + "']");
            }
            if(input.eq(0).is(":radio")) { //单选按钮
                input.filter("[value='" + value + "']").each(function(){this.checked = true});
            } else if(input.eq(0).is(":checkbox")) { //复选框
                if(!$.isArray(value)){
                    val = new Array();
                    val[0] = value;
                } else {
                    val = value;
                }
                for(i = 0, len = val.length; i < len; i++){
                    input.filter("[value='" + val[i] + "']").each(function(){this.checked = true});
                }
            } else {  //其他表单选项直接设置值
                input.val(value);
            }
        },

        /**
         * @desc 返回元素data-options属性对象
         * @param target [Dom,jQuery] 组件的宿主对象
         * @param properties [Array] 需要优先从宿主对象属性(不包含data-options属性)中取的opts列表
         **/
        parseOptions : function(target, properties, optionName){
            var t = $(target);
            var options = {};
            //第一步：首先从data-options属性中取opts
            var s = $.trim(t.attr(typeof optionName == 'string' ? optionName : 'data-options'));
            if (s){
                //兼容写大括号和不写大括号的写法
                if (s.substring(0, 1) != '{'){
                    s = '{' + s + '}';
                }
                //利用Function函数将字符串转为化对象
                //到这里，我们第一次取值就完成了。
                options = (new Function('return ' + s))();
            }

            //第二步：如果properties不为空的话，则将properties中定义的属性增加或者覆盖到“第一步”中取到的属性列表中
            if (properties){
                var opts = {};
                for(var i=0; i<properties.length; i++){
                    var pp = properties[i];
                    //如果是字符串型
                    if (typeof pp == 'string'){
                        //width height left top四个属性从宿主对象的style属性中取值
                        if (pp == 'width' || pp == 'height' || pp == 'left' || pp == 'top'){
                            opts[pp] = parseInt(target.style[pp]) || undefined;
                        } else {//其它情况直接从宿主对象的对应属性中取值
                            opts[pp] = t.attr(pp);
                        }
                    } else {//布尔型或者数字型
                        for(var name in pp){
                            var type = pp[name];
                            if (type == 'boolean'){//布尔型从宿主对象对应属性中取值，同时将属性值转为布尔型
                                opts[name] = t.attr(name) ? (t.attr(name) == 'true') : undefined;
                            } else if (type == 'number'){//数字型也从宿主对象对应属性中取值，同时将属性值转为浮数
                                opts[name] = t.attr(name)=='0' ? 0 : parseFloat(t.attr(name)) || undefined;
                            }
                        }
                    }
                }
                //第二步取得的结果覆盖到第一步取得的结果中
                $.extend(options, opts);
            }
            return options;
        },

        //获取表单jquery对象
        get$el : function(el){
            var $el = typeof el == 'object' && el.load ? el : el != null ? $(el) : el;
            return $el;
        },

        /*
         * 导航高亮
         * @param Object options 配置
         * {
         *  containerEl : jQuery/dom/selector,选中导航的元素
         *  el : jQuery/dom/selector,选中导航的元素
         *  defaultUrl : string,默认导航
         *  activeUrl : string,选中的导航
         *  activeCls : string,选中的类名
         *  urlCase : bool,区分大小写
         *  activeFunc : function选中执行函数
         * }
         */
        activeNav : function(options){
            options = $.extend({
                defaultUrl : 'index',
                activeCls : 'active',
                urlCase : false
            }, options);
            if(options.el){
                var $el = tool.get$el(options.containerEl);
                return active.call($el.get(0));
            }
            options.activeUrl = options.activeUrl || options.defaultUrl;
            if(!options.urlCase) options.activeUrl = options.activeUrl.toLowerCase();

            //延迟执行
            setTimeout(function(){
                var $container = tool.get$el(options.containerEl),
                    $nav = $container ? $container.find('a') : $('a');
                $nav.each(function(){
                    var $this = $(this), href = $this.attr('href') || '', reg;
                    if(!options.urlCase) href = href.toLowerCase();
                    n = href.indexOf(options.activeUrl);
                    if(n >= 0 && !/[\w]/.test(href.charAt(n - 1)) && !/[\w]/.test(href.charAt(n + options.activeUrl.length))){
                        return active.call(this);
                    }
                });
            });

            //选中
            function active(){
                if(options.activeFunc){
                    if(options.activeFunc.call(this, options) === false) return false;
                }
                if(options.activeCls) $(this).addClass(options.activeCls);
                return false;
            }

        },

        /**
         * @desc函数功能简述
         * @param {string} address 地址
         * @param {array} com 商品数组
         * @param {string} pay_status 支付方式
         * @returns void
        **/
        cookie : function(name, value, options) {
            if (typeof value != 'undefined') {
                options = options || {};
                if (value === null) {
                    value = '';
                    options.expires = -1;
                }
                var expires = '';
                if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                    var date;
                    if (typeof options.expires == 'number') {
                        date = new Date();
                        date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                    } else {
                        date = options.expires;
                    }
                    expires = '; expires=' + date.toUTCString();
                }
                var path = options.path ? '; path=' + (options.path) : '';
                var domain = options.domain ? '; domain=' + (options.domain) : '';
                var secure = options.secure ? '; secure' : '';
                document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
            } else {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
        },

        //获取默认配置
        getDefaults : function(){
            return defaults;
        }

    },

    //默认配置
    defaults = {
        //当前页key
        pageKey : 'page',
        //每页条数key
        rowsKey : 'rows',
        //每次分页数key
        sizeKey : 'size',
        //总数key
        totalKey : 'total'
    };

    return tool;
});