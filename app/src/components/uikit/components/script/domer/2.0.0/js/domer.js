/**
 * @author Rid King
 * @since 2017-07-11
 * @version 2.0.0
 * @description Dom类，处理元素查找、添加、删除、样式、类名操作
 */

function Domer (el) {
    return el

    return {
        remove,
        insertAdjacentElement: function () {

        }
    }
}

/**
 * @description 
*/
Domer.$ = function (id) {
    return document.getElementById(id)
}

/**
 * @description 查找元素
 * @param selector <String> / /选择器
 * @return <HTMLElement/null>
*/
Domer.querySelector = function (selector) {
    return document.querySelector(selector)
}

 export default Domer