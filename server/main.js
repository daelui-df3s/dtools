/**
 * @description 主文件
 * @since 2019-11-12
 * @author Rid King
 */

 const ox = require('@daelui/oxjs')
 // 路由
 let router = require('./router/index')
 // 启动入口
 function boot (config) {
   config.router = router
   return ox.bootstrap(config)
 }
 
 module.exports = {
   boot, router
 }