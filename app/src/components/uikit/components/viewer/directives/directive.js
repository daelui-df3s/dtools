/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 指令
 */

 // 指令模式
const DIRECTIVE_MODE = {
    FUNCTION: 1, // 函数编译指令
    COMPILE: 2, // 一般编译指令
    RUN: 3 // 运行指令
}

export {DIRECTIVE_MODE}