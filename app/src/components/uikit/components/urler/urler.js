/**
 * 统一资源定位
 * */

export default {
	stringify (obj, sep, eq) {
    sep = sep || '&';
    eq = eq || '=';
    let str = "";
    for (var k in obj) {
        str += k + eq + unescape(obj[k]) + sep
    }
    return str.slice(0, -1)
  },

  /**
   * @function 解析链接参数
   * @param {String} str 链接字符串
   * @return {Object}
  */
  parseURL (str) {
    var params = {}
    str = typeof str === 'string' ? str : ''
    var searches = str.split('?')
    searches.forEach(function (node) {
      if (/[^\s]+=\w*/.test(node)) {
        var items = node.split('&')
        items.forEach(function (item) {
          var index = item.indexOf('=')
          var key = item.slice(0, index)
          var value = unescape(item.slice(index + 1))
          if (index === -1) {
            key = item
            value = ''
          }
          if (key) {
            params[key] = value || ''
          }
        })
      }
    })

    return params
  }
}