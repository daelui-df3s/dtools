/**
 * Array extend
 * */
Array.prototype.add = function(item) {
	this.push(item);
}

Array.prototype.addRange = function(items) {
	var length = items.length;

	if (length != 0) {
		for (var index = 0; index < length; index++) {
			this.push(items[index]);
		}
	}
}

Array.prototype.clear = function() {
	if (this.length > 0) {
		this.splice(0, this.length);
	}
}

Array.prototype.isEmpty = function() {
	if (this.length == 0)
		return true;
	else
		return false;
}

Array.prototype.clone = function() {
	var clonedArray = [];
	var length = this.length;

	for (var index = 0; index < length; index++) {
		clonedArray[index] = this[index];
	}

	return clonedArray;
}

Array.prototype.contains = function(item) {
	var index = this.indexOf(item);
	return (index >= 0);
}

Array.prototype.queue = function(item) {
	this.push(item);
}

Array.prototype.dequeue = function() {
	return this.shift();
}

Array.prototype.insert = function(index, item) {
	this.splice(index, 0, item);
}

Array.prototype.joinstr = function(str) {
	var new_arr = new Array(this.length);
	for (var i = 0; i < this.length; i++) {
		new_arr[i] = this[i] + str
	}
	return new_arr;
}

Array.prototype.remove = function(item) {
	var index = this.indexOf(item);

	if (index >= 0) {
		this.splice(index, 1);
	}
}

Array.prototype.removeAt = function(index) {
	this.splice(index, 1);
}

// 数字数组由小到大排序
Array.prototype.Min2Max = function() {
	var oValue;
	for (var i = 0; i < this.length; i++) {
		for (var j = 0; j <= i; j++) {
			if (this[i] < this[j]) {
				oValue = this[i];
				this[i] = this[j];
				this[j] = oValue;
			}
		}
	}
	return this;
};

// 数字数组由大到小排序
Array.prototype.Max2Min = function() {
	var oValue;
	for (var i = 0; i < this.length; i++) {
		for (var j = 0; j <= i; j++) {
			if (this[i] > this[j]) {
				oValue = this[i];
				this[i] = this[j];
				this[j] = oValue;
			}
		}
	}
	return this;
};

// 获得数字数组中最大项
Array.prototype.GetMax = function() {
	var oValue = 0;
	for (var i = 0; i < this.length; i++) {
		if (this[i] > oValue) {
			oValue = this[i];
		}
	}
	return oValue;
};

// 获得数字数组中最小项
Array.prototype.GetMin = function() {
	var oValue = 0;
	for (var i = 0; i < this.length; i++) {
		if (this[i] < oValue) {
			oValue = this[i];
		}
	}
	return oValue;
};

/*
//ES 5 迭代器
function makeIterator(array){
	var nextIndex = 0;

	return {
		next: function(){
			return nextIndex < array.length ?
				{value: array[nextIndex++], done: false} :
				{done: true};
		}
	}
}

//一旦初始化, next() 方法可以用来依次访问可迭代对象中的元素：
var it = makeIterator(['yo', 'ya']);
console.log(it.next().value); // 'yo'
console.log(it.next().value); // 'ya'
console.log(it.next().done);  // true

//ES 6 迭代器
let arr = ['a', 'b', 'c'];
let it = arr[Symbol.iterator]();
console.log(it.next().value); // 'a'
console.log(it.next().value); // 'b'
console.log(it.next().value);  // 'c'
*/