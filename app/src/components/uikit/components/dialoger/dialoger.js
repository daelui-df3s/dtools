/*
 * @desc 对话框
 * */

import $ from 'jquery'
import objecter from '../../../core/extend/objecter.js'
import eventMode from '../event-mode/event-mode.js'
import template from '../template/template.js'
import layer from '../layer/layer.js'

class Dialoger {
  /**
   * @function 构造方法
  */
  constructor (options) {
    options = objecter.extend(
      {}, Dialoger.defaults, this.getDefaults(),
      options
    );
    this.setOptions(options);
  }

  // 设置配置
  setOptions (options) {
    this.options = objecter.extend({}, this.options, options);
  }

  // 获取配置
  getOptions (options) {
    return typeof key == 'string' ? this.options[key] : this.options;
  }

  /**
   * @function 获取默认配置对象
   * @return {Object}
  */
  getDefaults () {
    return {}
  }

  // 创建弹出窗
  build (options) {
    options = objecter.extend({}, this.getOptions(), options);
    var me = this,
      tpl = template.render(options.tpl || options.tpls.dialoger, options),
      $dialoger = $(tpl).appendTo('body');

    //远程加载
    if(options.url && typeof options.url == 'string'){
      var $container = $dialoger.find('.modal-body');
      //远程加载load
      if(options.load){
        $container.empty().append('<div class="modal-load" style="width:100%;height:100%;overflow:auto"></div>');
        $container.find('.modal-load').load(options.url, function(){
          if(typeof options.completeFunc == 'function'){
            options.completeFunc({dialog : $dialoger});
          }
        });
      }
      //加载iframe
      else if(options.frame){
        $container.empty().append('<iframe src="' + options.url + '" frameborder="no" scrolling="auto" style="width:100%;height:100%"></iframe>');
        this.frameLoad($container.find('iframe').get(0), function(){
          if(typeof options.completeFunc == 'function'){
            options.completeFunc({dialog : $dialoger});
          }
        });
      }
    }

    //标题与消息显示控制
    if(!options.title){
      $dialoger.find('.' + options.titleCls).remove();
    }
    if(!options.msg){
      $dialoger.find('.' + options.msgCls).remove();
    }

    //关闭按钮
    if(!options.closeAble){
      $dialoger.find('.modal-close').remove();
    }

    //确定与取消按钮显示控制
    if(!options.btnok){
      $dialoger.find('.' + options.okCls).remove();
    }
    if(!options.btncl){
      $dialoger.find('.' + options.clCls).remove();
    }

    //自定义按钮添加
    if(Array.isArray(options.btns)){
      options.btns.forEach(function(i, item){
        var $btn = $('<a class="modal-popup-btn ' + (item.cls || '') + '">' + (item.text || '按钮') + '</a>');
        $btn.on(eventMode.click, function(e){
          var b = true, value = $dialoger.find('.' + options.promptInputCls).val();
          if(typeof item.handler === 'function'){
            b = item.handler.call(me, {
              el : this,
              $dialoger : $dialoger,
              $overlay : $overlay,
              value : value
            });
          }
          if(typeof options.callback === 'function'){
            b = options.callback.call(me, {
              el : this,
              $dialoger : $dialoger,
              $overlay : $overlay,
              value : value
            });
          }

          //隐藏
          if(b !== false){
            me.hide();
          }
        });
        $dialoger.find('.modal-foot').append($btn);
      });
    }

    //是否模态背景
    if(options.backdrop){
      var $overlay = $(options.tpls.overlay).appendTo('body');
    }

    //显示模态背景
    if($overlay){
      $overlay.addClass(options.overlayActiveCls);
      if (typeof options.overlayZIndex === 'number') {
        $overlay.css('z-index', options.overlayzIndex)
        $dialoger.css('z-index', options.overlayzIndex + 1)
      }
      else if (options.overlayZIndexAuto) {
        let num = layer.increase()
        $overlay.css('z-index', num)
        $dialoger.css('z-index', num + 1)
      }
      setTimeout(function(){
        $overlay.addClass(options.overlayInCls);
      }, 50);
    }

    //显示弹出框
    $dialoger.addClass(options.dialogerActiveCls).addClass('modal-dialoger');
    setTimeout(function(){
      $dialoger.addClass(options.dialogerInCls);
    }, 50);

    this.$dialoger = $dialoger;
    this.$overlay = $overlay;

    //自动隐藏
    if(options.autoHidden){
      var timeHidden = isNaN(options.timeHidden) || options.timeHidden < 0 ? Dialoger.defaults.timeHidden : options.timeHidden;
      setTimeout(function(){
        me.hide();
      }, timeHidden);
    }

    ////事件处理////
    //确定
    $dialoger.find('.' + options.okCls).on(eventMode.click, function(){
      var b = true, value = $dialoger.find('.' + options.promptInputCls).val();
      if(typeof options.okHandler === 'function'){
        b = options.okHandler.call(me, {
          el : this,
          $dialoger : $dialoger,
          $overlay : $overlay,
          value : value
        });
      }
      if(typeof options.callback === 'function'){
        b = options.callback.call(me, {
          el : this,
          $dialoger : $dialoger,
          $overlay : $overlay,
          sure : true,
          value : value
        });
      }

      //隐藏
      if(b !== false){
        me.hide();
      }
    });

    //取消
    $dialoger.find('.' + options.clCls).on(eventMode.click, function(){
      var b = true, value = $dialoger.find('.' + options.promptInputCls).val();
      if(typeof options.clHandler === 'function'){
        b = options.clHandler.call(me, {
          el : this,
          $dialoger : $dialoger,
          $overlay : $overlay,
          value : value
        });
      }
      if(typeof options.callback === 'function'){
        b = options.callback.call(me, {
          el : this,
          $dialoger : $dialoger,
          $overlay : $overlay,
          sure : false,
          value : value
        });
      }

      //隐藏
      if(b !== false){
        me.hide();
      }
    });

    //关闭按钮退出
    $dialoger.find('.modal-close').on(eventMode.click, function(){
      var b = true;
      if(typeof options.callback === 'function'){
        b = options.callback.call(me, {
          el : this,
          $dialoger : $dialoger,
          $overlay : $overlay,
          sure : -1
        });
      }

      //隐藏
      if(b !== false){
        me.hide();
      }
    });

    //模态退出
    if(options.backdropHideAble && $overlay){
      $overlay.on(eventMode.click, function(){
        var b = true;
        if(typeof options.callback === 'function'){
          b = options.callback.call(me, {
              el : this,
              $dialoger : $dialoger,
              $overlay : $overlay,
              sure : -1
            });
        }

        //隐藏
        if(b !== false){
          me.hide();
        }
      });
    }
  }

  /**
   * @function 显示
  */
  show (options) {
    this.hide()
    return this.build(options)
  }

  // 隐藏
  hide () {
    var $dialoger = this.$dialoger,
      $overlay = this.$overlay,
      options = this.getOptions();

    // 弹出框
    if ($dialoger) {
      $dialoger.addClass(options.dialogerOutCls);
      setTimeout(function(){
        $dialoger.remove();
      }, 1000);
    }

    // 遮罩层
    if ($overlay) {
      $overlay.addClass(options.overlayOutCls);
      setTimeout(function(){
        $overlay.remove();
      }, 1000);
    }

    this.$dialoger = null;
    this.$overlay = null;
  }

  // 销毁
  remove (options) {
    
  }

  // 框架监测
  frameLoad (frame, fn) {
    if(!frame){
      fn();
      return;
    }
    if(frame.readyState == "complete" || frame.readyState == 1){
      fn();
    }else{
      if (frame.attachEvent){
        frame.attachEvent("onload", function(){
          fn();
        });
      } else {
        frame.onload = function(){
          fn();
        };
      }
    }
  }
}

// 默认配置
Dialoger.defaults = {
  //标题
  title : '标题',
  //标题类名
  titleCls : 'modal-title',
  //消息内容
  msg : '消息',
  //消息类名
  msgCls : 'modal-text',
  //关闭按钮
  closeAble : false,
  //远程地址
  url : '',
  //load形式加载远程地址
  load : false,
  //frame形式加载远程地址
  frame : false,
  //是否模态
  backdrop : true,
  //是否点击模态退出
  backdropHideAble : true,
  //弹出框类名
  dialogerCls : '',
  //显示类名
  dialogerActiveCls : 'active',
  //进入类名
  dialogerInCls : 'modal-in',
  //退出类名
  dialogerOutCls : 'modal-out',
  //显示类名
  overlayActiveCls : 'active',
  //进入类名
  overlayInCls : 'modal-visible',
  //退出类名
  overlayOutCls : 'modal-hidden',
  // 遮罩层级
  overlayZIndex: null,
  // 遮罩层级自动
  overlayZIndexAuto: false,
  //是否显示确定按钮
  btnok : true,
  //确定类名
  okCls : 'popup-btn-ok',
  //确定回调方法
  okHandler : null,
  //确定文本
  okText : '确定',
  //取消文本
  clText : '取消',
  //是否显示取消按钮
  btncl : true,
  //取消类名
  clCls : 'popup-btn-cl',
  //取消回调方法
  clHandler : null,
  //无论成功或失败回调
  callback : null,
  //加载文本
  indicatorTitle : '加载中...',
  //是否自支隐藏
  autoHide : false,
  //弹出框隐藏时间
  timeHidden : 2000,
  //填值类名
  promptInputCls : 'prompt-input',
  //填值默认值
  promptValue : '',
  //模板
  tpls : {
    //覆盖背景
    overlay : '<div class="modal-overlay"></div>',
    //弹出框模板
    dialoger :  '<div class="modal modal-popup {{dialogerCls}}">' +
          '<div class="modal-head">' +
            '<div class="modal-title">{{title}}</div>' +
            '<div class="modal-tools">' +
              '<a class="modal-close">×</a>' +
            '</div>' +
          '</div>' +
          '<div class="modal-body">' +
            '<div class="modal-text">{{msg}}</div>' +
          '</div>' +
          '<div class="modal-foot">' +
            '<a class="modal-popup-btn popup-btn-ok">{{okText}}</a>' +
            '<a class="modal-popup-btn popup-btn-cl">{{clText}}</a>' +
          '</div>' +
        '</div>',
    //填值模板
    prompt : '<div class="modal modal-popup {{dialogerCls}}">' +
          '<div class="modal-head">' +
            '<div class="modal-title">{{title}}</div>' +
            '<div class="modal-tools">' +
              '<a class="modal-close">×</a>' +
            '</div>' +
          '</div>' +
          '<div class="modal-body">' +
            '<div class="modal-text">{{msg}}</div>' +
            '<div class="modal-text"><input class="form-input prompt-input" name="modal_input" type="text" value="{{promptValue}}" /></div>' +
          '</div>' +
          '<div class="modal-foot">' +
            '<a class="modal-popup-btn popup-btn-ok">{{okText}}</a>' +
            '<a class="modal-popup-btn popup-btn-cl">{{clText}}</a>' +
          '</div>' +
        '</div>',
    //简单提示模板
    toast : '<div class="modal modal-toast {{dialogerCls}}">{{msg}}</div>'
  }
}

// 默认实例
const dialoger = new Dialoger
export default dialoger
export {
  Dialoger
}