/**
 * @description 富表单
 */
define(['jquery', 'eventer', 'classer'], function($, eventer, classer){
	var richFormer = {
		build : function(options){
			$container = $(options.containerEl);
			$container.find('.form-switch-rich').each(function(){
				new RichSwitch({
					el : this
				});
			});

			$container.find('.form-radio-group').each(function(){
				var groupEl = this;
				$(this).find('.form-radio-rich').each(function(){
					new RichRadio({
						groupEl : groupEl,
						el : this
					});
				});
			});

			$container.find('.form-checkbox-group').each(function(){
				var groupEl = this;
				$(this).find('.form-checkbox-rich').each(function(){
					new RichCheckbox({
						groupEl : groupEl,
						el : this
					});
				});
			});
		}
	};

	//富表单
	var Rich = classer.extend({
		//配置
		options : {
			activeCls : 'active'
		},

		//初始化
		init : function(options){
			this.setOptions(options);
			this.initEvents();
		},

		//初始化事件
		initEvents : function(){
			var me = this, el = this.getOptions('el');
			$(el).on(eventer.events.click, function(){
				me.onActive(this);
			});
		}
	}),

	//开关
	RichSwitch = Rich.extend({
		//选择
		onActive : function(el){
			var $el = $(el), activeCls = this.getOptions('activeCls'), b = false;
			if(!$el.hasClass(activeCls)){
				b = true;
			}

			this.active($el, b);
		},

		//选中
		active : function($el, b){
			var activeCls = this.getOptions('activeCls');
			b = b || false;
			if(b){
				$el.addClass(activeCls);
			}else{
				$el.removeClass(activeCls);
			}
			$el.find('input').prop('checked', b);
		}
	}),

	//富单选
	RichRadio = Rich.extend({
		//选择
		onActive : function(el){
			var $el = $(el), $group = $(this.getOptions('el')),
				activeCls = this.getOptions('activeCls'), b = false;
			if(!$el.hasClass(activeCls)){
				b = true;
			}

			this.active($el, b);
		},

		//选中
		active : function($el, b){
			var $group = $(this.getOptions('groupEl')),
				activeCls = this.getOptions('activeCls');
			b = b || false;
			if(b){
				$group.find('.' + activeCls).removeClass(activeCls).find('input').prop('checked', false);
				$el.addClass(activeCls).find('input').prop('checked', true);
			}
		}
	}),

	//富多选
	RichCheckbox = Rich.extend({
		//选择
		onActive : function(el){
			var $el = $(el), activeCls = this.getOptions('activeCls'), b = false;
			if(!$el.hasClass(activeCls)){
				b = true;
			}

			this.active($el, b);
		},

		//选中
		active : function($el, b){
			var activeCls = this.getOptions('activeCls');
			b = b || false;
			if(b){
				$el.addClass(activeCls);
			}else{
				$el.removeClass(activeCls);
			}
			$el.find('input').prop('checked', b);
		}
	});

	return richFormer;
});