/**
 * @description 数据请求组件
 * @since 2019-03-06
 * @author Rid King
*/

import { ds } from '@daelui/dogjs/dist/components.js'
import resolver from './resolver'

// 设置token
ds.$on('onBeforeRequest', function (options) {
  let token = localStorage.getItem('daelui-token')
  if (token) {
    options.headers.token = token
  }
})
// 设置解析器
ds.setOptions({ resolver })

// 通道设置
ds.pipe(function (data) {
  // 失败
  if (data && String(data.status) === '401') {
    return Promise.reject(data)
  }
  return data
})

export default ds
