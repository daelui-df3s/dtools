/**
 * @author Rid King
 * @since 2017-07-15
 * @version 2.0.0
 * @description 元素节点类
 */

import Node from './node.js'

class ElementNode extends Node {
    constructor (name) {
        super()
        this.tagName = name
        // 属性
        this.attributes = {}
        // 子节点
        this.childNodes = []
    }

    /**
     * @description 设置标签名
     * @param name <String> // 标签名
    */
    set tagName (name) {
        if (typeof name === 'string') {
            this.name = name
        }
    }

    /**
     * @description 获取标签名
     * @param name <String> // 标签名
    */
    get tagName () {
        return this.name
    }

    /**
     * @description 闭合
    */
    close () {
        this._end = true
    }

    /**
     * @description 是否闭合
    */
    isClosed () {
        return this._end === true ? true : false
    }
}

export default ElementNode