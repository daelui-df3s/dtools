/**
 * @author Rid King
 * @since 2017-05-19
 * @version 2.0.0
 * @description 可遍历数组、对象、字符串、数字(替换成整型后遍历)
 * demo: <div dog-for="item,i in list">{{item.text}}</div>
 */

import objecter from '../../../../../../../components/script/objecter/2.0.0/js/objecter.js'
import {DIRECTIVE_MODE} from './directive.js'

 /**
  * @description 解析参数
 */
function resolveArgs (value) {
    let args = /\(?\s*([\w_\$]+\s*\,?\s*[\w_\$]*)\s*\)?\s+(in|of)\s+(.+)/.exec(value)

    // 参数不匹配
    if (!args || args.length < 4) {
        return ''
    }

    let param = args[1].split(',')
    let key = '$index'
    let attr = param
    let list = args[3]
    // 带索引的参数
    if (param.length > 1) {
        key = param[1].trim()
        attr = param[0].trim()
    }
    // 仅有值
    else {
        attr = param[0]
    }

    return {
        key: key,
        attr: attr,
        list: list
    }
}

export default {
    name: 'dog-for',
    scope: {
        /**
         * @property 指令编译模式
         * @description 分为编译指令与运行时指令
         * */
        mode: DIRECTIVE_MODE.COMPILE,
        /**
         * @property 指令排序
         * @description 值越小优先级越高
         * */
        order: 1,
        /**
         * @function 解析参数
         * @param value {String} // 指令值
         * @param code {String} // 编译码
        */
        resolve (value, code) {
            let args = resolveArgs(value)
            // 参数不存在
            if (!args) {
                return code
            }

            // 生成编译字符
            code = '_d("dog-for",' + args.list + ',function(' + args.attr + ',' + args.key + '){return ' + code + '})'

            return code
        },
        /**
         * @function 编译方法
         * @param list {Array/Object/String/Int} // 遍历数组
         * @param fn {Function} // 处理方法
        */
        compile (list, fn) {
            let code = []
            // 字符串形式转换成数组
            if (objecter.isString(list)) {
                list = list.split('')
            }

            // 数组形式
            if (objecter.isArray(list)) {
                list.forEach(function (item, i) {
                    code.push(fn(item, i))
                })
            }
            // 对象形式
            else if (objecter.isPlainObject(list)) {
                for (let key in list) {
                    code.push(fn(list[key], key))
                }
            }
            // 数字形式，以0为原点，小于0则递减，大于0则递加
            else if (objecter.isNumber(list)) {
                list = parseInt(list)
                // 小于0
                if (list < 0) {
                    for (let i = 0; i >= list; i--) {
                        code.push(fn(i, i))
                    }
                } else {
                    for (let i = 0; i <= list; i++) {
                        code.push(fn(i, i))
                    }
                }
            }

            return code
        }
    }
}

export {
    resolveArgs
}