// 语言设置
export const UPDATE_LANG = 'UPDATE_LANG'
// 遮罩加载状态
export const SHOWPRELOADER = 'SHOWPRELOADER'

// 首页文章
export const UPDATE_INDEXARTICLElIST = 'UPDATE_INDEXARTICLElIST'

// 更新管理员或登录用户
export const UPDATE_MASTER = 'UPDATE_MASTER'

// 更新文章列表
export const UPDATE_ARTICLElIST = 'UPDATE_ARTICLElIST'
// 更新文章
export const UPDATE_ARTICLE = 'UPDATE_ARTICLE'
