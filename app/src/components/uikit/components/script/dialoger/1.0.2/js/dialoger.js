/*
 * @desc 对话框
 * */
define(['classer', 'jquery', 'eventer', 'objecter', 'template'], function(classer, $, eventer, objecter, template) {
	var dialoger = {
		//弹出框
		show : function(options){
			var d = new Dialoger(options);
			d.show();
			return d;
		},

		//警告框
		alert : function(options){
			var d = new Dialoger(options);
			d.alert();
			return d;
		},

		//选择框
		confirm : function(options){
			var d = new Dialoger(options);
			d.confirm();
			return d;
		},

		//输入框
		prompt : function(options){
			var d = new Dialoger(options);
			d.prompt();
			return d;
		},

		//普通信息展示
		info : function(options){
			var d = new Dialoger(options);
			d.info();
			return d;
		},

		//成功信息
		success : function(options){
			var d = new Dialoger(options);
			d.success();
			return d;
		},

		//警告信息展示
		warning : function(options){
			var d = new Dialoger(options);
			d.warning();
			return d;
		},

		//错误信息展示
		danger : function(options){
			var d = new Dialoger(options);
			d.danger();
			return d;
		},
		error : function(options){
			return this.danger(options);
		},

		//加载器
		loader : function(options){
			var d = new Dialoger(options);
			d.loader();
			return d;
		},

		//指示器
		indicator : function(options){
			var d = new Dialoger(options);
			d.indicator();
			return d;
		},

		//简单提示模板
		toast : function(options){
			var d = new Dialoger(options);
			d.toast();
			return d;
		},

		// 元素弹出
		popup: function (options) {
			var d = new Dialoger(options);
			d.popup();
			return d;
		},

		getClass : function () {
			return Dialoger;
		}
	};

	//弹出框类
	var Dialoger = classer.extend({
		init : function(options){
			options = objecter.extend({}, Dialoger.defaults, options);
			this.setOptions(options);
		},

		//显示
		show : function(options){
			options = objecter.extend({}, this.getOptions(), options);
			var me = this,
				tpl = template.render(options.tpl || options.tpls.dialoger, options),
				$dialoger = $(tpl).appendTo('body');

			//标题与消息显示控制
			if(!options.title){
				$dialoger.find('.' + options.titleCls).remove();
			}
			if(!options.msg){
				$dialoger.find('.' + options.msgCls).remove();
			}

			//关闭按钮
			if(!options.closeAble){
				$dialoger.find('.modal-close').remove();
			}

			//确定与取消按钮显示控制
			if(!options.btnok){
				$dialoger.find('.' + options.okCls).remove();
			}
			if(!options.btncl){
				$dialoger.find('.' + options.clCls).remove();
			}

			//拖拽
			if(options.dragAble){
				require(['drager'], function(drager){
					drager.build({
						$el : $dialoger.find('.modal-title'),
						$dragEl : $dialoger
					});
				});
			}

			this.showDialog($dialoger, options);
		},

		//显示
		showDialog : function($dialoger, options){
			options = objecter.extend({}, this.getOptions(), options);
			var me = this;
			$dialoger.css(this.getCVStyle($dialoger, options));

			//远程加载
			if(options.url && typeof options.url == 'string'){
				var $container = $dialoger.find('.modal-body');
				//远程加载load
				if(options.load){
					$container.empty().append('<div class="modal-load" style="width:100%;height:100%;overflow:auto"></div>');
					$container.find('.modal-load').load(options.url, function(){
						if(typeof options.completeFunc == 'function'){
							options.completeFunc({dialog : $dialoger});
						}
					});
				}
				//加载iframe
				else if(options.frame){
					$container.empty().append('<iframe src="' + options.url + '" frameborder="no" scrolling="auto" style="width:100%;height:100%"></iframe>');
					frameLoad($container.find('iframe').get(0), function(){
						if(typeof options.completeFunc == 'function'){
							options.completeFunc({dialog : $dialoger});
						}
					});
				}
			}

			//自定义按钮添加
			if($.isArray(options.btns)){
				$.each(options.btns, function(i, item){
					var $btn = $('<a class="modal-popup-btn ' + (item.cls || '') + '">' + (item.text || '按钮') + '</a>');
					$btn.on(eventer.events.click, function(e){
						var b = true, value = $dialoger.find('.' + options.promptInputCls).val();
						if(typeof item.handler === 'function'){
							b = item.handler.call(me, {
								el : this,
								$dialoger : $dialoger,
								$overlay : $overlay,
								value : value
							});
						}
						if(typeof options.callback === 'function'){
							b = options.callback.call(me, {
								el : this,
								$dialoger : $dialoger,
								$overlay : $overlay,
								value : value
							});
						}

						//隐藏
						if(b !== false){
							me.hide();
						}
					});
					$dialoger.find('.modal-foot').append($btn);
				});
			}

			//是否模态背景
			if(options.backdrop){
				var $overlay = $(options.tpls.overlay).appendTo('body');
			}

			//显示模态背景
			if($overlay){
				$overlay.addClass(options.overlayActiveCls);
				setTimeout(function(){
					$overlay.addClass(options.overlayInCls);
				}, 50);
			}

			//显示弹出框
			$dialoger.addClass(options.dialogerActiveCls).addClass('modal-dialoger');
			setTimeout(function(){
				$dialoger.addClass(options.dialogerInCls);
			}, 50);

			this.$dialoger = $dialoger;
			this.$overlay = $overlay;

			//自动隐藏
			if(options.autoHidden){
				var timeHidden = isNaN(options.timeHidden) || options.timeHidden < 0 ? Dialoger.defaults.timeHidden : options.timeHidden;
				setTimeout(function(){
					me.hide();
				}, timeHidden);
			}

			////事件处理////
			//确定
			$dialoger.find('.' + options.okCls).on(eventer.events.click, function(){
				var b = true, value = $dialoger.find('.' + options.promptInputCls).val();
				if(typeof options.okHandler === 'function'){
					b = options.okHandler.call(me, {
						el : this,
						$dialoger : $dialoger,
						$overlay : $overlay,
						value : value
					});
				}
				if(typeof options.callback === 'function'){
					b = options.callback.call(me, {
						el : this,
						$dialoger : $dialoger,
						$overlay : $overlay,
						sure : true,
						value : value
					});
				}

				//隐藏
				if(b !== false){
					me.hide();
				}
			});

			//取消
			$dialoger.find('.' + options.clCls).on(eventer.events.click, function(){
				var b = true, value = $dialoger.find('.' + options.promptInputCls).val();
				if(typeof options.clHandler === 'function'){
					b = options.clHandler.call(me, {
						el : this,
						$dialoger : $dialoger,
						$overlay : $overlay,
						value : value
					});
				}
				if(typeof options.callback === 'function'){
					b = options.callback.call(me, {
						el : this,
						$dialoger : $dialoger,
						$overlay : $overlay,
						sure : false,
						value : value
					});
				}

				//隐藏
				if(b !== false){
					me.hide();
				}
			});

			//关闭按钮退出
			$dialoger.find('.modal-close').on(eventer.events.click, function(){
				var b = true;
				if(typeof options.callback === 'function'){
					b = options.callback.call(me, {
						el : this,
						$dialoger : $dialoger,
						$overlay : $overlay,
						sure : -1
					});
				}

				//隐藏
				if(b !== false){
					me.hide();
				}
			});

			//模态退出
			if(options.backdropHideAble && $overlay){
				$overlay.on(eventer.events.click, function(){
					var b = true;
					if(typeof options.callback === 'function'){
						b = options.callback.call(me, {
							el : this,
							$dialoger : $dialoger,
							$overlay : $overlay,
							sure : -1
						});
					}

					//隐藏
					if(b !== false){
						me.hide();
					}
				});
			}

			//框架监测
			function frameLoad(frame, fn){
				if(!frame){
					fn();
					return;
				}
				if(frame.readyState == "complete" || frame.readyState == 1){
					fn();
				}else{
					if (frame.attachEvent){
						frame.attachEvent("onload", function(){
							fn();
						});
					} else {
						frame.onload = function(){
							fn();
						};
					}
				}
			}
		},

		//隐藏
		hide : function(id){
			var $dialoger = this.$dialoger,
				$overlay = this.$overlay,
				options = this.getOptions();
			if($dialoger){
				$dialoger.addClass(options.dialogerOutCls);
				setTimeout(function(){
					$dialoger.remove();
				}, 1000);
			}
			if($overlay){
				$overlay.addClass(options.overlayOutCls);
				setTimeout(function(){
					$overlay.remove();
				}, 1000);
			}

			this.$dialoger = null;
			this.$overlay = null;
		},

		//警告框
		alert : function(options){
			this.show(objecter.extend({
				btncl : false
			}, options));
		},

		//选择框
		confirm : function(options){
			this.show(objecter.extend({
				btnCl : false
			}, options));
		},

		//输入框
		prompt : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				tpl : opts.tpls.prompt
			}, options));
		},

		//普通信息展示
		info : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				dialogerCls : 'modal-info',
				tpl : opts.tpls.toast,
				autoHidden : true,
				backdrop : false
			}, options));
		},

		//成功信息
		success : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				dialogerCls : 'modal-success',
				tpl : opts.tpls.toast,
				autoHidden : true,
				backdrop : false
			}, options));
		},

		//警告信息展示
		warning : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				dialogerCls : 'modal-warning',
				tpl : opts.tpls.toast,
				autoHidden : true,
				backdrop : false
			}, options));
		},

		//错误信息展示
		danger : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				dialogerCls : 'modal-error',
				tpl : opts.tpls.toast,
				autoHidden : true,
				backdrop : false
			}, options));
		},
		error : function(options){
			this.danger(options);
		},

		//加载器
		loader : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				tpl : opts.tpls.loader,
				title : '加载中',
				backdropHideAble : false
			}, options));
		},

		//指示器
		indicator : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				tpl : opts.tpls.indicator,
				backdrop : false
			}, options));
		},

		//简单提示模板
		toast : function(options){
			var opts = this.getOptions();
			this.show(objecter.extend({
				tpl : opts.tpls.toast,
				autoHidden : true,
				backdrop : false
			}, options));
		},

		//弹出
		popup : function(options){
			var $el = $(options.el),
				style = this.getCVStyle($el, options);
			$el.css(style);
			if(options.backdrop !== false){
				$modal = (options.modalEl ? $(options.modalEl) : $('<div class="modal-mask"></div>')).appendTo(options.conainerEl || 'body');
				$modal.css(this.getModalStyle($modal, options));
				if(options.backhide){
					$modal.click(function(){
						close();
					});
				}
			}

			//事件
			if(options.closeEl){
				var $close = $(options.closeEl);
				$close.click(function(){
					close();
				});
			}

			//关闭
			function close(){
				$el.fadeOut(function(){
					if(options.destroyAble){
						$el.remove();
						if($modal){
							$modal.remove();
						}
					}
				});
				if($modal){
					$modal.hide();
				}
			}

			$el.fadeIn();
		},

		//获取水平垂直居中样式
		getCVStyle : function($popup, options){
			options = options || {};
			var $win = $(options.conainerEl || options.window || window),
				width = $win.width(),
				height = $win.height(),
				popWidth = options.width || $popup.width(),
				popHeight = options.height || $popup.height(),
				defaults = {
					width : width * 0.9,
					height : height * 0.9
				}, style;

			if(popWidth < 20 || popWidth > width){
				popWidth = defaults.width;
			}
			if(popHeight < 20 || popHeight > height){
				popHeight = defaults.height;
			}

			style = {
				width : popWidth,
				height : popHeight,
				left : (width - popWidth) / 2,
				top : (height - popHeight) / 2,
				position : 'fixed'/*,
				'z-index' : options.zindex || 20*/
			};

			return style;
		},

		//获取模态样式
		getModalStyle : function($modal, options){
			options = options || {};
			var $win = $(options.conainerEl || options.window || window),
				width = $win.width(),
				height = $win.height(), style;
			style = {
				position : 'fixed',
				left : 0,
				right : 0,
				top : 0,
				bottom : 0,
				background : '#000',
				opacity : 0.5/*,
				'z-index' : options.modalZindex || 10*/
			};

			return style;
		}
	});

	Dialoger.defaults = {
		//标题
		title : '标题',
		//标题类名
		titleCls : 'modal-title',
		//消息内容
		msg : '消息',
		//消息类名
		msgCls : 'modal-text',
		//是否拖拽
		dragAble : true,
		//关闭按钮
		closeAble : true,
		//远程地址
		url : '',
		//load形式加载远程地址
		load : false,
		//frame形式加载远程地址
		frame : false,
		//是否模态
		backdrop : true,
		//是否点击模态退出
		backdropHideAble : true,
		//弹出框类名
		dialogerCls : '',
		//显示类名
		dialogerActiveCls : 'active',
		//进入类名
		dialogerInCls : 'modal-in',
		//退出类名
		dialogerOutCls : 'modal-out',
		//显示类名
		overlayActiveCls : 'active',
		//进入类名
		overlayInCls : 'modal-visible',
		//退出类名
		overlayOutCls : 'modal-hidden',
		//是否显示确定按钮
		btnok : true,
		//确定类名
		okCls : 'popup-btn-ok',
		//确定回调方法
		okHandler : null,
		//确定文本
		okText : '确定',
		//取消文本
		clText : '取消',
		//是否显示取消按钮
		btncl : true,
		//取消类名
		clCls : 'popup-btn-cl',
		//取消回调方法
		clHandler : null,
		//无论成功或失败回调
		callback : null,
		//加载文本
		indicatorTitle : '加载中...',
		//是否自支隐藏
		autoHide : false,
		//弹出框隐藏时间
		timeHidden : 2000,
		//填值类名
		promptInputCls : 'prompt-input',
		//填值默认值
		promptValue : '',
		//模板
		tpls : {
			//覆盖背景
			overlay : '<div class="modal-overlay"></div>',
			//弹出框模板
			dialoger :  '<div class="modal modal-popup {{dialogerCls}}">' +
						'<div class="modal-head">' +
							'<div class="modal-title">{{title}}</div>' +
							'<div class="modal-tools">' +
								'<a class="modal-close">×</a>' +
							'</div>' +
						'</div>' +
						'<div class="modal-body">' +
							'<div class="modal-text">{{msg}}</div>' +
						'</div>' +
						'<div class="modal-foot">' +
							'<a class="modal-popup-btn popup-btn-ok">{{okText}}</a>' +
							'<a class="modal-popup-btn popup-btn-cl">{{clText}}</a>' +
						'</div>' +
					'</div>',
			//填值模板
			prompt : '<div class="modal modal-popup {{dialogerCls}}">' +
						'<div class="modal-head">' +
							'<div class="modal-title">{{title}}</div>' +
							'<div class="modal-tools">' +
								'<a class="modal-close">×</a>' +
							'</div>' +
						'</div>' +
						'<div class="modal-body">' +
							'<div class="modal-text">{{msg}}</div>' +
							'<div class="modal-text"><input class="form-input prompt-input" name="modal_input" type="text" value="{{promptValue}}" /></div>' +
						'</div>' +
						'<div class="modal-foot">' +
							'<a class="modal-popup-btn popup-btn-ok">{{okText}}</a>' +
							'<a class="modal-popup-btn popup-btn-cl">{{clText}}</a>' +
						'</div>' +
					'</div>',
			//加载模板
			loader : '<div class="modal modal-popup {{dialogerCls}}">' +
						'<div class="modal-head">' +
							'<div class="modal-title">{{title}}</div>' +
							'<div class="modal-tools">' +
								'<a class="modal-close">×</a>' +
							'</div>' +
						'</div>' +
						'<div class="modal-body">' +
							'<div class="modal-text"><span class="indicator"></span></div>' +
						'</div>' +
					'</div>',
			//指示器模板
			indicator :  '<div class="modal modal-popup-indicator {{dialogerCls}}">' +
						'<span class="indicator indicator-snow"></span>' +
					'</div>',
			//简单提示模板
			toast : '<div class="modal modal-toast {{dialogerCls}}">{{msg}}</div>'
		}
	};

	return dialoger;

});