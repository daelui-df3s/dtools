/**
 * @author Rid King
 * @since 2017-07-29
 * @version 2.0.0
 * @description 加载器
 */

class Loader {
    /**
     * @function 加载文本
     * @param url <String> // 文本地址
     * @param callback {Function} // 回调函数
     * @return {Promise}
    */
    static text (url) {

    }

    /**
     * @function 加载脚本
     * @param url <String> // 文本地址
     * @param callback {Function} // 回调函数
     * @return {Promise}
    */
    static js (url) {

    }

    /**
     * @function 加载样式
     * @param url <String> // 文本地址
     * @param callback {Function} // 回调函数
     * @return {Promise}
    */
    static css (url) {

    }
}

export default Loader