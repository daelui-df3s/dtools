import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/index/index'
import tools from '@/views/tools/index'
import Item from '@/views/tools/item'
// 格式化
import JSView from '@/views/tools/format/js'
import CSSView from '@/views/tools/format/css'
// 文本处理转换
import UpLowerView from '@/views/tools/text/up-lower'
import CountView from '@/views/tools/text/count'
import ReplaceView from '@/views/tools/text/replace'
import DateView from '@/views/tools/text/date'
import ToFileView from '@/views/tools/text/tofile'
import IconFontView from '@/views/tools/text/iconfont'
import RandomView from '@/views/tools/text/random'
// 颜色
import RGBView from '@/views/tools/color/rgb'
// 加密与解密
import MD5View from '@/views/tools/secret/md5'
import URLHexView from '@/views/tools/secret/urlhex'
import Base64View from '@/views/tools/secret/base64'
import EncodeView from '@/views/tools/secret/encode'
import RSAView from '@/views/tools/secret/rsa'
import HashView from '@/views/tools/secret/hash'
// 图片
import SpriteView from '@/views/tools/photo/sprite'
import FlowView from '@/views/tools/photo/flow'
import QRCodeView from '@/views/tools/photo/qrcode'
import OCRView from '@/views/tools/photo/ocr'
// 服务
import ProxyView from '@/views/tools/service/proxy'
import WebSocketView from '@/views/tools/service/websocket'
import StompView from '@/views/tools/service/stomp'
// 单位
import LengthView from '@/views/tools/unit/length'
import AreaView from '@/views/tools/unit/area'
import TimeView from '@/views/tools/unit/time'
import WeightView from '@/views/tools/unit/weight'
// 文档
import SymbolView from '@/views/tools/appendix/symbol'
import ContrastView from '@/views/tools/appendix/contrast'
import ColoursView from '@/views/tools/appendix/colours'
import RegExpView from '@/views/tools/appendix/regexp'
import KeyCodeView from '@/views/tools/appendix/keycode'
import HTTPCodeView from '@/views/tools/appendix/http-code'
import ContentTypeView from '@/views/tools/appendix/content-type'
import ASCIIView from '@/views/tools/appendix/ascii'
import PortView from '@/views/tools/appendix/port'
import LangView from '@/views/tools/appendix/lang'
import config from '@/micros/config'

Vue.use(Router)


// 判断环境是否是微应用打开
let microPath = ''
if (window.__POWERED_BY_QIANKUN__) {
  microPath = config.microPath
}

export default new Router({
  routes: [
    {
      path: microPath + '/',
      name: 'index',
      component: Index
    },
    {
      path: microPath + '/home',
      name: 'home',
      component: Index
    },
    {
      path: microPath + '/tools',
      name: 'tools',
      component: tools
    },
    {
      path: microPath + '/tools/item',
      name: 'toolsItem',
      component: Item,
      children: [
        // 格式化
        {
          path: 'json',
          name: 'json',
          component: () => import(/* webpackChunkName: "json" */ '@/views/tools/format/json')
        },
        {
          path: 'js',
          name: 'js',
          component: JSView
        },
        {
          path: 'css',
          name: 'css',
          component: CSSView
        },
        // 文本处理转换
        {
          path: 'uplower',
          name: 'uplower',
          component: UpLowerView
        },
        {
          path: 'count',
          name: 'count',
          component: CountView
        },
        {
          path: 'replace',
          name: 'replace',
          component: ReplaceView
        },
        {
          path: 'date',
          name: 'date',
          component: DateView
        },
        {
          path: 'compare',
          name: 'compare',
          component: () => import(/* webpackChunkName: "compare" */ '@/views/tools/text/compare')
        },
        {
          path: 'tofile',
          name: 'tofile',
          component: ToFileView
        },
        {
          path: 'iconfont',
          name: 'iconfont',
          component: IconFontView
        },
        {
          path: 'random',
          name: 'random',
          component: RandomView
        },
        // 编译
        {
          path: 'es6-es5',
          name: 'es6-es5',
          component: () => import(/* webpackChunkName: "es6-es5" */ '@/views/tools/compile/es6-es5')
        },
        {
          path: 'less',
          name: 'less',
          component: () => import(/* webpackChunkName: "less" */ '@/views/tools/compile/less')
        },
        {
          path: 'tpl',
          name: 'tpl',
          component: () => import(/* webpackChunkName: "tpl" */ '@/views/tools/compile/tpl')
        },
        {
          path: 'console',
          name: 'console',
          component: () => import(/* webpackChunkName: "console" */ '@/views/tools/compile/console')
        },
        // 颜色
        {
          path: 'rgb',
          name: 'rgb',
          component: RGBView
        },
        // 加密与解密
        {
          path: 'md5',
          name: 'md5',
          component: MD5View
        },
        {
          path: 'urlhex',
          name: 'urlhex',
          component: URLHexView
        },
        {
          path: 'base64',
          name: 'base64',
          component: Base64View
        },
        {
          path: 'encode',
          name: 'encode',
          component: EncodeView
        },
        {
          path: 'hash',
          name: 'hash',
          component: HashView
        },
        {
          path: 'rsa',
          name: 'rsa',
          component: RSAView
        },
        // 图片
        {
          path: 'sprite',
          name: 'sprite',
          component: SpriteView
        },
        {
          path: 'flow',
          name: 'flow',
          component: FlowView
        },
        {
          path: 'qrcode',
          name: 'qrcode',
          component: QRCodeView
        },
        {
          path: 'ocr',
          name: 'ocr',
          component: OCRView
        },
        {
          path: 'image-download',
          name: 'image-download',
          component: () => import(/* webpackChunkName: "download" */ '@/views/tools/photo/download')
        },
        {
          path: 'svg',
          name: 'svg',
          component: () => import(/* webpackChunkName: "download" */ '@/views/tools/photo/svg')
        },
        // Http服务
        {
          path: 'proxy',
          name: 'proxy',
          component: ProxyView
        },
        // Websocket服务
        {
          path: 'websocket',
          name: 'websocket',
          component: WebSocketView
        },
        // Stomp服务
        {
          path: 'stomp',
          name: 'stomp',
          component: StompView
        },
        // 单位
        {
          path: 'length',
          name: 'length',
          component: LengthView
        },
        {
          path: 'area',
          name: 'area',
          component: AreaView
        },
        {
          path: 'time',
          name: 'time',
          component: TimeView
        },
        {
          path: 'weight',
          name: 'weight',
          component: WeightView
        },
        // 文档
        {
          path: 'symbol',
          name: 'symbol',
          component: SymbolView
        },
        {
          path: 'contrast',
          name: 'contrast',
          component: ContrastView
        },
        {
          path: 'colours',
          name: 'colours',
          component: ColoursView
        },
        {
          path: 'regexp',
          name: 'regexp',
          component: RegExpView
        },
        {
          path: 'keycode',
          name: 'keycode',
          component: KeyCodeView
        },
        {
          path: 'httpcode',
          name: 'httpcode',
          component: HTTPCodeView
        },
        {
          path: 'contenttype',
          name: 'contenttype',
          component: ContentTypeView
        },
        {
          path: 'ascii',
          name: 'ascii',
          component: ASCIIView
        },
        {
          path: 'port',
          name: 'port',
          component: PortView
        },
        {
          path: 'base64code',
          name: 'base64code',
          component: () => import(/* webpackChunkName: "base64" */ '@/views/tools/appendix/base64')
        },
        {
          path: 'lang',
          name: 'lang',
          component: LangView
        }
      ]
    },
    {
      path: microPath + '/console',
      name: 'console-alone',
      component: () => import(/* webpackChunkName: "console-alone" */ '@/views/tools/compile/console-alone.vue')
    }
  ]
})
