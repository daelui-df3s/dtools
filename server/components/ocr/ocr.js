/**
 * 单线程执行ocr文字识别
 */

const path = require('path')
const fs = require('fs')
const Tesseract = require('tesseract.js')

/**
 * 单线程将图片文字识别到本地图片txt文件下
 * @param {*} param 参数 
 * @param {*} param.targetPhotoDir 目标图片路径
 */
const singleThreadOCR = async ({targetPhotoDir, languages, targetPath, langPath, cachePath, workerPath, isOutputFile} = {}) => {
  const worker = await Tesseract.createWorker(languages, 3, {
    // workerPath,
    options: {
      langPath: langPath,
      cachePath: cachePath || langPath
    }
  })
  // 识别
  const res = await worker.recognize(targetPhotoDir)
  const { data: { text } } = res
  // 文本输出至文件
  if (isOutputFile) {
    // 生成指定路径
    await mkdirSync(targetPath)
    // 写入文件
    const outputfileDir = `${targetPath}\\single-train.txt`
    fs.writeFileSync(outputfileDir, text)
  }
  // detect():输出ocr的基本结果
  //  结果：data2 = { data: {orientation_confidence //方向_置信度;  orientation_degrees //方位_度;  script_confidence //脚本可信度} jobId //工作id }
  // const data2 = await worker.detect(path.join(__dirname, './images/b.png'))
  // console.log(`detect执行后的${data2}`)
  // 终止
  await worker.terminate()

  return res
}

/**
 * 生成要保存文字识别的文件
 * @param {*} targetPath 
 */
const mkdirSync = async (targetPath) => {
  try {
    if (!fs.existsSync(targetPath)) {
      fs.mkdirSync(targetPath)
    }
  } catch (error) {
    console.error(error)
  }
}

// 日期格式化
function simpleFormat(time) {
  var date = new Date()
  if (time) {
    data = new Date(time)
  }
  if (String(date) === 'Invalid Date') {
    return ''
  }
  var year= date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()
  return [year, '-', month , '-', day, ' ', hour , ':', minute, ':', second ].map(item => {
    return /^\d{1}$/.test(item) ? ('0' + item).slice(-2) : item
  }).join('')
}

module.exports = {
  singleThreadOCR
}

/*
// 测试
singleThreadOCR({
  targetPhotoDir: path.join(__dirname, './images/train.png'),
  targetPath: path.join(__dirname, './output/singleThread'),
  langPath: path.join(__dirname, '../../static/tessdata'),
  isOutputFile: true,
  languages: [
    'chi_sim',
    'chi_sim_vert',
    'chi_tra',
    'chi_tra_vert',
    'eng'
  ]
})
*/