# DTools

#### 演示地址
* <a href="http://www.daelui.com/#/dtools/tools" target="_blank">http://www.daelui.com/#/dtools/tools</a>


#### 介绍
- DTools(Dog Tools) - 小工具集，无数据库，纯NodeJS环境，本地可静态运行
- 工欲善其事，必先利其器
- 日常开发、测试过程中实用小工具，快捷方便，简单好用
-  1、服务端仅使用NodeJS，数据存储采用文件形式，尽量使系统部署简化
-  2、基于HTML语言的工具包,无需安装,可适应多类型终端;基于Web页面操作,方便快捷
-  3、系统可网络部署与本地静态访问,有网与无网都可使用,满足各场景下使用
-  4、对数据可提供覆盖与非覆盖两种模式,原有数据可保持;各功能之间提供管道操作,达到功能连续
* 具体功能如下：
* JSON格式化
* JS/HTML格式化
* CSS格式化
* HTTP测试
* WebSocket测试
* STOMP测试
* 大小写转换
* 字符统计
* 字符替换
* 文本比对
* 随机数生成
* 转化文件
* ES6编译ES5
* Less编译
* 模板编译
* 在线控制台
* MD5加密与解密
* RSA加密与解密
* Hash加密:sha1/sha224/sha256/sha384/sha384/sha512/md5/hmacsha1/hmacsha224/hmacsha256/hmacsha384/hmacsha512/hmacmd5/pbkdf2
* 16进制转换
* Base64
* URL转码
* 雪碧图
* 流程图
* 二维码
* 文本识别
* SVG预览
* 色值转换
* 长度单位转换
* 面积单位转换
* 重量单位转换
* 时间单位转换
* 符号大全表
* HTML符号表
* 配色集合
* 常用正则
* 按键KeyCode表
* HTTP状态码表
* Content-Type表
* ASCII对照表
* TCP/UDP端口表
* Base64编码表
* 语言代码表


#### 软件架构
- 后端框架：Express + OXJS
- 前端框架：VUE + DogUI + DogJS
- OXJS采用JSON数据模拟数据库，实现无数据库运行环境


#### 安装步骤
1. 安装Node环境：http://nodejs.cn/


#### 使用说明
- 方式1: 命令启动
1. cd dest/server
2. node app.js
- 方式2: 脚本启动(仅限windows系统)
1. 进入dest目录
2. 双击执行start.bat
##### 页面访问
浏览器地址输入以下地址，以下任意地址访问即可：
1) http://127.0.0.1:3502/app/index.html (http页面)
2) http://127.0.0.1:3501/app/index.html 或 https://127.0.0.1:3501/app/index.html (http或https页面)
3) https://127.0.0.1:3503/app/index.html (https页面)


#### 开发说明
1.  app目录为前端项目
2.  server目录为后端项目
3.  二次开发需要前后端各自启动
4.  全局安装webpack执行命令：npm install webpack-cli -g


#### 演示说明
* 账号：admin 密码：123456