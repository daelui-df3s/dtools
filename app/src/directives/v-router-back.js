/**
 * @description 返回指令
*/

import Vue from 'vue'
import router from '../router'

Vue.directive('router-back', {
  bind () {},
  inserted (el) {
    el.addEventListener('click', () => {
      router.back()
    })
  },
  update () {},
  componentUpdated () {},
  unbind () {}
})