/*
 * Array ES6 extend
 */
Array.prototype.find = function (fn, context) {
	var temp = null;
	if (typeof fn === "function") {
		for (var i = 0; i < this.length; i++) {
			if (fn.call(context, this[i], i, this)) {
				temp = this[i];
				break;
			}
		}
	}
	return temp;
}

Array.prototype.fill = function(value) {  
   
    // Steps 1-2.  
    var O = Object(this);  
   
    // Steps 3-5.  
    var len = parseInt(O.length);  
   
    // Steps 6-7.  
    var relativeStart = parseInt(arguments[1] || 0);  
   
    // Step 8.  
    var k = relativeStart < 0  
            ? Math.max(len + relativeStart, 0)  
            : Math.min(relativeStart, len);  
   
    // Steps 9-10.  
    var relativeEnd = parseInt(arguments[2] || len);  
   
    // Step 11.  
    var final = relativeEnd < 0  
                ? Math.max(len + relativeEnd, 0)  
                : Math.min(relativeEnd, len);  
   
    // Step 12.  
    for (; k < final; k++) {  
        O[k] = value;  
    }  
   
    // Step 13.  
    return O;  
  };  