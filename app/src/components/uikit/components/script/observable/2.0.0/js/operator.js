/**
 * @author Rid King
 * @since 2017-06-12
 * @version 2.0.0
 * @description 操作符
 */
'user strict';

import _Observable from './_observable.js'
import Observer from './observer.js'

class Operator extends Observer{
    /**
     * @description 获取操作名称
     * @return <String>
    */
    static getName () {
        return 'operator'
    }

    /**
     * @description 构造函数
     * @param observer <Observer> 消费者
     * @param args <Arguments> 操作符参数
    */
    constructor (observer, args) {
        super()

        // 最终消费者
        this._observer = observer
        // 保存当前参数
        this._args = args

        // 操作符初始处理
        this.process.apply(this, args)
    }

    /**
     * @function 操作处理逻辑
    */
    process () {
        // 消费者对象
        this.destination = {
            next: (x) => {
                this._observer.next(x)
            },
            error: (err) => {
                this._observer.error(err)
            },
            complete: () => { 
                this._observer.complete()
            }
        }
    }
}

/**
 * @description 检测到流未正常执行则重复执行
 * @param num <int> 重复次数
*/
// const remedy = new Operator('remedy', function (num) {

// })

export default Operator