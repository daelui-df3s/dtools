const { defineConfig } = require('@vue/cli-service')
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}

const isProd = process.env.NODE_ENV === 'production'
const isSingle = process.env.NODE_ENV === 'single'
const packageName = 'dtools'

module.exports = defineConfig({
  pages: {
    index: {
      entry: isProd ? 'src/loader.js' : 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      inject: true
    },
    main: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'main.html',
      inject: true
    },
    'micro-app': {
      entry: 'src/micro-app.js',
      template: 'public/index.html',
      filename: 'micro-app.html',
      inject: true
    }
  },
  outputDir: isSingle ? '../dest/app' : '../dist/app',
  transpileDependencies: true,
  runtimeCompiler: true,
  // baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  publicPath: isSingle ? './' : isProd ? '/df3s/' + packageName + '/app/' : '/',
  assetsDir: 'static',
  productionSourceMap: false,
  filenameHashing: false, // 取消hash
  // css: {
  //   // 将组件内部的css提取到一个单独的css文件（只用在生产环境）
  //   // 也可以是传递给 extract-text-webpack-plugin 的选项对象
  //   extract: true, // 允许生成 CSS source maps?
  //   sourceMap: false, // pass custom options to pre-processor loaders. e.g. to pass options to // sass-loader, use { sass: { ... } }
  //   loaderOptions: {}, // Enable CSS modules for all css / pre-processor files. // This option does not affect *.vue files.
  //   modules: false
  // },
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("@assets", resolve("src/assets"))
      .set("@vendor", resolve("src/assets/vendor"))
      .set("@components", resolve("src/components"))
      .set("public", resolve("public"));
  },
  configureWebpack: (config) => {
    return {
      plugins: [
        // new CopyPlugin({
        //   patterns: [
        //     {
        //       from: path.resolve(__dirname, './src'),
        //       to: 'dist'
        //     }
        //   ]
        // })
      ],
      output: {
        libraryTarget: 'umd',
        library: packageName,
        // filename: `[name].${version}.${timestamp}.js`,
        // chunkFilename: `[name].${version}.${timestamp}.js`
      },
      externals: isProd ? [
        {
          'vue': 'vue',
          'vue-router': 'vue-router',
          'vuex': 'vuex',
          'system': 'system',
          'axios': 'axios',
          'iview': 'iview',
          'moment': 'moment',
          'echarts': 'echarts',
          'element-ui': 'element-ui',
          'less': 'less',
          '@babel/standalone': '@babel/standalone',
          'sockjs-client': 'sockjs-client',
          'stompjs': 'stompjs'
        },
        /^@daelui\/(?!pigjs|roosterui)/,
        function(context, request, callback) {
          if (/vendor\/monaco-editor$|vendor\/(vue-)?quill|vendor\/highlightjs/.test(request)) {
            return callback(null, request.replace(/.*vendor\//, ''))
          }
          callback()
        },
        function(context, request, callback) {
          if (/^@daelui\/dogui\/dist|element-ui\/lib\/theme/.test(request)) {
            return callback(null, request)
          }
          callback()
        }
      ] : {}
    }
  }
})
